////////////////////////////////////////////////////////////////////////////////
// Copyright (C) 1999-2008 Easics NV.
// This source file may be used and distributed without restriction
// provided that this copyright statement is not removed from the file
// and that any derivative work contains the original copyright notice
// and the associated disclaimer.
//
// THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
// WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
// Purpose : synthesizable CRC function
//   * polynomial: x^8 + x^2 + x^1 + 1
//   * data width: 48
//
// Info : tools@easics.be
//        http://www.easics.com
////////////////////////////////////////////////////////////////////////////////
module CRC8_D48(
    input  wire [47:0] Data,
    input  wire [7:0]  crc,
    output wire [7:0]  newcrc
);


  // polynomial: x^8 + x^2 + x^1 + 1
  // data width: 48
  // convention: the first serial bit is D[47]
  function [7:0] nextCRC8_D48;

    input [47:0] Data;
    input [7:0] crc;
    reg [47:0] d;
    reg [7:0] c;
    reg [7:0] newcrc;
  begin
    d = Data;
    c = crc;

    newcrc[0] = d[45] ^ d[43] ^ d[40] ^ d[39] ^ d[35] ^ d[34] ^ d[31] ^ d[30] ^ d[28] ^ d[23] ^ d[21] ^ d[19] ^ d[18] ^ d[16] ^ d[14] ^ d[12] ^ d[8] ^ d[7] ^ d[6] ^ d[0] ^ c[0] ^ c[3] ^ c[5];
    newcrc[1] = d[46] ^ d[45] ^ d[44] ^ d[43] ^ d[41] ^ d[39] ^ d[36] ^ d[34] ^ d[32] ^ d[30] ^ d[29] ^ d[28] ^ d[24] ^ d[23] ^ d[22] ^ d[21] ^ d[20] ^ d[18] ^ d[17] ^ d[16] ^ d[15] ^ d[14] ^ d[13] ^ d[12] ^ d[9] ^ d[6] ^ d[1] ^ d[0] ^ c[1] ^ c[3] ^ c[4] ^ c[5] ^ c[6];
    newcrc[2] = d[47] ^ d[46] ^ d[44] ^ d[43] ^ d[42] ^ d[39] ^ d[37] ^ d[34] ^ d[33] ^ d[29] ^ d[28] ^ d[25] ^ d[24] ^ d[22] ^ d[17] ^ d[15] ^ d[13] ^ d[12] ^ d[10] ^ d[8] ^ d[6] ^ d[2] ^ d[1] ^ d[0] ^ c[2] ^ c[3] ^ c[4] ^ c[6] ^ c[7];
    newcrc[3] = d[47] ^ d[45] ^ d[44] ^ d[43] ^ d[40] ^ d[38] ^ d[35] ^ d[34] ^ d[30] ^ d[29] ^ d[26] ^ d[25] ^ d[23] ^ d[18] ^ d[16] ^ d[14] ^ d[13] ^ d[11] ^ d[9] ^ d[7] ^ d[3] ^ d[2] ^ d[1] ^ c[0] ^ c[3] ^ c[4] ^ c[5] ^ c[7];
    newcrc[4] = d[46] ^ d[45] ^ d[44] ^ d[41] ^ d[39] ^ d[36] ^ d[35] ^ d[31] ^ d[30] ^ d[27] ^ d[26] ^ d[24] ^ d[19] ^ d[17] ^ d[15] ^ d[14] ^ d[12] ^ d[10] ^ d[8] ^ d[4] ^ d[3] ^ d[2] ^ c[1] ^ c[4] ^ c[5] ^ c[6];
    newcrc[5] = d[47] ^ d[46] ^ d[45] ^ d[42] ^ d[40] ^ d[37] ^ d[36] ^ d[32] ^ d[31] ^ d[28] ^ d[27] ^ d[25] ^ d[20] ^ d[18] ^ d[16] ^ d[15] ^ d[13] ^ d[11] ^ d[9] ^ d[5] ^ d[4] ^ d[3] ^ c[0] ^ c[2] ^ c[5] ^ c[6] ^ c[7];
    newcrc[6] = d[47] ^ d[46] ^ d[43] ^ d[41] ^ d[38] ^ d[37] ^ d[33] ^ d[32] ^ d[29] ^ d[28] ^ d[26] ^ d[21] ^ d[19] ^ d[17] ^ d[16] ^ d[14] ^ d[12] ^ d[10] ^ d[6] ^ d[5] ^ d[4] ^ c[1] ^ c[3] ^ c[6] ^ c[7];
    newcrc[7] = d[47] ^ d[44] ^ d[42] ^ d[39] ^ d[38] ^ d[34] ^ d[33] ^ d[30] ^ d[29] ^ d[27] ^ d[22] ^ d[20] ^ d[18] ^ d[17] ^ d[15] ^ d[13] ^ d[11] ^ d[7] ^ d[6] ^ d[5] ^ c[2] ^ c[4] ^ c[7];
    nextCRC8_D48 = newcrc;
  end
  endfunction
  
assign newcrc[7:0] = nextCRC8_D48(Data, crc);  
  
endmodule
