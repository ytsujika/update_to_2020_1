// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Sep 17 14:30:04 2020
// Host        : koichiro-PC running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/tsujikawa/SLFirmware/sandbox/update_2020_1/FPGA/L1MuEGLinkCnvFirmware.srcs/sources_1/ip/dcm40_to_4phases/dcm40_to_4phases_stub.v
// Design      : dcm40_to_4phases
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k410tffg900-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module dcm40_to_4phases(clk40_0, clk40_1, clk40_2, clk40_3, reset, locked, 
  clk_in)
/* synthesis syn_black_box black_box_pad_pin="clk40_0,clk40_1,clk40_2,clk40_3,reset,locked,clk_in" */;
  output clk40_0;
  output clk40_1;
  output clk40_2;
  output clk40_3;
  input reset;
  output locked;
  input clk_in;
endmodule
