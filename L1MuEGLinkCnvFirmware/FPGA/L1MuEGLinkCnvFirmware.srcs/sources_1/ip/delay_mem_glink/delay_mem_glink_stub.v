// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Sep 17 14:27:47 2020
// Host        : koichiro-PC running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/tsujikawa/SLFirmware/sandbox/update_2020_1/FPGA/L1MuEGLinkCnvFirmware.srcs/sources_1/ip/delay_mem_glink/delay_mem_glink_stub.v
// Design      : delay_mem_glink
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k410tffg900-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_4,Vivado 2020.1" *)
module delay_mem_glink(clka, wea, addra, dina, clkb, rstb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,wea[0:0],addra[6:0],dina[20:0],clkb,rstb,addrb[6:0],doutb[20:0]" */;
  input clka;
  input [0:0]wea;
  input [6:0]addra;
  input [20:0]dina;
  input clkb;
  input rstb;
  input [6:0]addrb;
  output [20:0]doutb;
endmodule
