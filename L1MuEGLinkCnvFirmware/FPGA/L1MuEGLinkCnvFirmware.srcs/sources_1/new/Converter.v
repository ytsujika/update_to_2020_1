module Converter(
  input clk,
  input rst,
  
  input  wire [16:0]  glink_rx0,
  input  wire [16:0]  glink_rx1,
  input  wire [16:0]  glink_rx2,
  input  wire [16:0]  glink_rx3,
  input  wire [16:0]  glink_rx4,
  input  wire [16:0]  glink_rx5,
  input  wire [16:0]  glink_rx6,
  input  wire [16:0]  glink_rx7,
  input  wire [16:0]  glink_rx8,
  input  wire [16:0]  glink_rx9,
  input  wire [16:0]  glink_rx10,
  input  wire [16:0]  glink_rx11,
  input  wire [16:0]  glink_rx12,
  input  wire [16:0]  glink_rx13,

  output reg [47:0] gtx_tx0,
  output reg [47:0] gtx_tx1,
  output reg [47:0] gtx_tx2,
  output reg [47:0] gtx_tx3,
  output reg [47:0] gtx_tx4,
  output reg [47:0] gtx_tx5,
  output reg [47:0] gtx_tx6,
  output reg [47:0] gtx_tx7,
  output reg [47:0] gtx_tx8,
  output reg [47:0] gtx_tx9,
  output reg [47:0] gtx_tx10,
  output reg [47:0] gtx_tx11
);

reg [237:0]  buffer;

reg [47:0] register0;
reg [47:0] register1;
reg [47:0] register2;
reg [47:0] register3;
reg [47:0] register4;
reg [47:0] register5;
reg [47:0] register6;
reg [47:0] register7;
reg [47:0] register8;
reg [47:0] register9;
reg [47:0] register10;
reg [47:0] register11;

always@(posedge clk)begin
  if(rst)begin
    buffer = 238'h0;
  end else begin
    buffer [16:0]    <=  glink_rx0;  // EI/FI 24  or 12
    buffer [33:17]   <=  glink_rx1;  // EI/FI 1   or 13
    buffer [50:34]   <=  glink_rx2;  // EI/FI 2   or 14
    buffer [67:51]   <=  glink_rx3;  // EI/FI 3   or 15
    buffer [84:68]   <=  glink_rx4;  // EI/FI 4   or 16
    buffer [101:85]  <=  glink_rx5;  // EI/FI 5   or 17
    buffer [118:102] <=  glink_rx6;  // EI/FI 6   or 18
    buffer [135:119] <=  glink_rx7;  // EI/FI 7   or 19
    buffer [152:136] <=  glink_rx8;  // EI/FI 8   or 20
    buffer [169:153] <=  glink_rx9;  // EI/FI 9   or 21
    buffer [186:170] <=  glink_rx10; // EI/FI 10  or 22
    buffer [203:187] <=  glink_rx11; // EI/FI 11  or 23
    buffer [220:204] <=  glink_rx12; // EI/FI 12  or 24
    buffer [237:221] <=  glink_rx13; // EI/FI 13  or 1
  end
end

always@(posedge clk)begin
//Link Num 0
  register0 [15:0]   <=  buffer [15:0];
  register0 [31:16]  <=  buffer [32:17];
  register0 [47:32]  <=  buffer [49:34];
//Link Num 1
  register1 [15:0]   <=  buffer [32:17];
  register1 [31:16]  <=  buffer [49:34];
  register1 [47:32]  <=  buffer [66:51];
//Link Num 2
  register2 [15:0]   <=  buffer [49:34];
  register2 [31:16]  <=  buffer [66:51];
  register2 [47:32]  <=  buffer [83:68];
//Link Num 3
  register3 [15:0]   <=  buffer [66:51];
  register3 [31:16]  <=  buffer [83:68];
  register3 [47:32]  <=  buffer [100:85];
//Link Num 4
  register4 [15:0]   <=  buffer [83:68];
  register4 [31:16]  <=  buffer [100:85];
  register4 [47:32]  <=  buffer [117:102];
//Link Num 5
  register5 [15:0]   <=  buffer [100:85];
  register5 [31:16]  <=  buffer [117:102];
  register5 [47:32]  <=  buffer [134:119];
//Link Num 6
  register6 [15:0]   <=  buffer [117:102];
  register6 [31:16]  <=  buffer [134:119];
  register6 [47:32]  <=  buffer [151:136];
//Link Num 7
  register7 [15:0]   <=  buffer [134:119];
  register7 [31:16]  <=  buffer [151:136];
  register7 [47:32]  <=  buffer [168:153];
//Link Num 8
  register8 [15:0]   <=  buffer [151:136];
  register8 [31:16]  <=  buffer [168:153];
  register8 [47:32]  <=  buffer [185:170];
//Link Num 9
  register9 [15:0]   <=  buffer [168:153];
  register9 [31:16]  <=  buffer [185:170];
  register9[47:32]   <=  buffer [202:187];
//Link Num 10
  register10 [15:0]   <=  buffer [185:170];
  register10 [31:16]  <=  buffer [202:187];
  register10 [47:32]  <=  buffer [219:204];
//Link Num 11
  register11 [15:0]   <=  buffer [202:187];
  register11 [31:16]  <=  buffer [219:204];
  register11 [47:32]  <=  buffer [236:221];
end


always@(posedge clk)begin
  gtx_tx0  <=  register0;
  gtx_tx1  <=  register1;
  gtx_tx2  <=  register2;
  gtx_tx3  <=  register3;
  gtx_tx4  <=  register4;
  gtx_tx5  <=  register5;
  gtx_tx6  <=  register6;
  gtx_tx7  <=  register7;
  gtx_tx8  <=  register8;
  gtx_tx9  <=  register9;
  gtx_tx10 <=  register10;
  gtx_tx11 <=  register11;
end

endmodule
