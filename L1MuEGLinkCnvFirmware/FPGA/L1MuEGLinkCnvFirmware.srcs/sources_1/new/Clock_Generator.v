`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/08/17 03:34:40
// Design Name: 
// Module Name: Clock_Generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Clock_Generator(
    input wire FPGA_CLK_in,
    input wire FPGA_CLK40_in,
    input wire reset_in,
    
    output wire CLK_40_0_out,
    output wire CLK_40_1_out,
    output wire CLK_40_2_out,
    output wire CLK_40_3_out,
    output wire CLK_160_out,
    output wire TTC_CLK_out,
    output wire CLK_40_VMEWRITE_out,
    output wire locked_out
    );
    

wire CLK_40;
wire locked40;
wire locked160;
wire clk_125_int;
    
wire TTC_CLK;
//assign TTC_CLK = TTC_CLK_out;
assign CLK_40_VMEWRITE_out = CLK_40;
//assign CLK_40_0_out = CLK_40;

BUFG FPGA_CLK_BUFG ( .I(FPGA_CLK_in), .O(TTC_CLK));
BUFG FPGA_CLK40_BUFG ( .I(FPGA_CLK40_in), .O(CLK_40));

assign TTC_CLK_out = CLK_40_0_out;

dcm40_to_4phases dcm40_to_4phases(
    .clk_in     (TTC_CLK),
    .clk40_0    (CLK_40_0_out),
    .clk40_1    (CLK_40_1_out),
    .clk40_2    (CLK_40_2_out),
    .clk40_3    (CLK_40_3_out),
    .reset      (reset_in),
    .locked     (locked40)
);
    
dcm40_to_160 dcm40_to_160(
    .clk_in     (CLK_40),
    .clk160     (CLK_160_out),
    .reset      (reset_in),
    .locked     (locked160)
);
    
    
    assign locked_out = locked40 && locked160;

endmodule
