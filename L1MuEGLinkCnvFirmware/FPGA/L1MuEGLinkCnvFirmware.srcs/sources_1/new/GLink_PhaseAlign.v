`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/11/23 18:30:07
// Design Name: 
// Module Name: GLink_Delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLink_PhaseAlign(
    input wire CLK_in,
    input wire CLK_160,
    input wire reset_in,

    input wire [20:0] glink0_data_in,
    input wire [20:0] glink1_data_in,
    input wire [20:0] glink2_data_in,
    input wire [20:0] glink3_data_in,
    input wire [20:0] glink4_data_in,
    input wire [20:0] glink5_data_in,
    input wire [20:0] glink6_data_in,
    input wire [20:0] glink7_data_in,
    input wire [20:0] glink8_data_in,
    input wire [20:0] glink9_data_in,
    input wire [20:0] glink10_data_in,
    input wire [20:0] glink11_data_in,
    input wire [20:0] glink12_data_in,
    input wire [20:0] glink13_data_in,

    input wire [3:0] Phase_glink0,
    input wire [3:0] Phase_glink1,
    input wire [3:0] Phase_glink2,
    input wire [3:0] Phase_glink3,
    input wire [3:0] Phase_glink4,
    input wire [3:0] Phase_glink5,
    input wire [3:0] Phase_glink6,
    input wire [3:0] Phase_glink7,
    input wire [3:0] Phase_glink8,
    input wire [3:0] Phase_glink9,
    input wire [3:0] Phase_glink10,
    input wire [3:0] Phase_glink11,
    input wire [3:0] Phase_glink12,
    input wire [3:0] Phase_glink13,

    output wire [20:0] GL0_ifd,
    output wire [20:0] GL1_ifd,
    output wire [20:0] GL2_ifd,
    output wire [20:0] GL3_ifd,
    output wire [20:0] GL4_ifd,
    output wire [20:0] GL5_ifd,
    output wire [20:0] GL6_ifd,
    output wire [20:0] GL7_ifd,
    output wire [20:0] GL8_ifd,
    output wire [20:0] GL9_ifd,
    output wire [20:0] GL10_ifd,
    output wire [20:0] GL11_ifd,
    output wire [20:0] GL12_ifd,
    output wire [20:0] GL13_ifd,

    output wire [83:0] GL0_ifd_all,
    output wire [83:0] GL1_ifd_all,
    output wire [83:0] GL2_ifd_all,
    output wire [83:0] GL3_ifd_all,
    output wire [83:0] GL4_ifd_all,
    output wire [83:0] GL5_ifd_all,
    output wire [83:0] GL6_ifd_all,
    output wire [83:0] GL7_ifd_all,
    output wire [83:0] GL8_ifd_all,
    output wire [83:0] GL9_ifd_all,
    output wire [83:0] GL10_ifd_all,
    output wire [83:0] GL11_ifd_all,
    output wire [83:0] GL12_ifd_all,
    output wire [83:0] GL13_ifd_all
    );


GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink0_IFD (
.data_in_from_pins (glink0_data_in[20:0]),
.data_in_to_device (GL0_ifd[20:0]),
.data_in_to_device_all (GL0_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink0),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink1_IFD (
.data_in_from_pins (glink1_data_in[20:0]),
.data_in_to_device (GL1_ifd[20:0]),
.data_in_to_device_all (GL1_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink1),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink2_IFD (
.data_in_from_pins (glink2_data_in[20:0]),
.data_in_to_device (GL2_ifd[20:0]),
.data_in_to_device_all (GL2_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink2),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink3_IFD (
.data_in_from_pins (glink3_data_in[20:0]),
.data_in_to_device (GL3_ifd[20:0]),
.data_in_to_device_all (GL3_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink3),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink4_IFD (
.data_in_from_pins (glink4_data_in[20:0]),
.data_in_to_device (GL4_ifd[20:0]),
.data_in_to_device_all (GL4_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink4),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink5_IFD (
.data_in_from_pins (glink5_data_in[20:0]),
.data_in_to_device (GL5_ifd[20:0]),
.data_in_to_device_all (GL5_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink5),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink6_IFD (
.data_in_from_pins (glink6_data_in[20:0]),
.data_in_to_device (GL6_ifd[20:0]),
.data_in_to_device_all (GL6_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink6),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink7_IFD (
.data_in_from_pins (glink7_data_in[20:0]),
.data_in_to_device (GL7_ifd[20:0]),
.data_in_to_device_all (GL7_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink7),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink8_IFD (
.data_in_from_pins (glink8_data_in[20:0]),
.data_in_to_device (GL8_ifd[20:0]),
.data_in_to_device_all (GL8_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink8),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink9_IFD (
.data_in_from_pins (glink9_data_in[20:0]),
.data_in_to_device (GL9_ifd[20:0]),
.data_in_to_device_all (GL9_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink9),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink10_IFD (
.data_in_from_pins (glink10_data_in[20:0]),
.data_in_to_device (GL10_ifd[20:0]),
.data_in_to_device_all (GL10_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink10),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink11_IFD (
.data_in_from_pins (glink11_data_in[20:0]),
.data_in_to_device (GL11_ifd[20:0]),
.data_in_to_device_all (GL11_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink11),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink12_IFD (
.data_in_from_pins (glink12_data_in[20:0]),
.data_in_to_device (GL12_ifd[20:0]),
.data_in_to_device_all (GL12_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink12),
.io_reset          (1'b0)
);

GLink_IFD #(
.SYS_W (21),
.DEV_W (21),
.IOSTANDARD ("LVCMOS33")
)
GLink13_IFD (
.data_in_from_pins (glink13_data_in[20:0]),
.data_in_to_device (GL13_ifd[20:0]),
.data_in_to_device_all (GL13_ifd_all[83:0]),
.clk_in            (CLK_160),
.clk_40_in         (CLK_in),
.phase_in          (Phase_glink13),
.io_reset          (1'b0)
);

endmodule
