`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/01/30 17:22:34
// Design Name: 
// Module Name: MaskTest_Pulse
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Mask(
    input wire          CLK_in,
    input wire          reset_in,
    input wire [111:0]  gtx0_data_in,
    input wire [111:0]  gtx1_data_in,
    input wire [111:0]  gtx2_data_in,
    input wire [111:0]  gtx3_data_in,
    input wire [111:0]  gtx4_data_in,
    input wire [111:0]  gtx5_data_in,
    input wire [111:0]  gtx6_data_in,
    input wire [111:0]  gtx7_data_in,
    input wire [111:0]  gtx8_data_in,
    input wire [111:0]  gtx9_data_in,
    input wire [111:0]  gtx10_data_in,
    input wire [111:0]  gtx11_data_in,
    input wire [16:0]   glink0_data_in,
    input wire [16:0]   glink1_data_in,
    input wire [16:0]   glink2_data_in,
    input wire [16:0]   glink3_data_in,
    input wire [16:0]   glink4_data_in,
    input wire [16:0]   glink5_data_in,
    input wire [16:0]   glink6_data_in,
    input wire [16:0]   glink7_data_in,
    input wire [16:0]   glink8_data_in,
    input wire [16:0]   glink9_data_in,
    input wire [16:0]   glink10_data_in,
    input wire [16:0]   glink11_data_in,
    input wire [16:0]   glink12_data_in,
    input wire [16:0]   glink13_data_in,

    input wire [15:0]   Test_Pulse_Length,
    input wire [15:0]   Test_Pulse_Wait_Length,
    input wire          Test_Pulse_Enable,

    input wire [1:0]    Mask_gtx0_in,
    input wire [1:0]    Mask_gtx1_in,
    input wire [1:0]    Mask_gtx2_in,
    input wire [1:0]    Mask_gtx3_in,
    input wire [1:0]    Mask_gtx4_in,
    input wire [1:0]    Mask_gtx5_in,
    input wire [1:0]    Mask_gtx6_in,
    input wire [1:0]    Mask_gtx7_in,
    input wire [1:0]    Mask_gtx8_in,
    input wire [1:0]    Mask_gtx9_in,
    input wire [1:0]    Mask_gtx10_in,
    input wire [1:0]    Mask_gtx11_in,

    input wire [1:0]    Mask_glink0_in,
    input wire [1:0]    Mask_glink1_in,
    input wire [1:0]    Mask_glink2_in,
    input wire [1:0]    Mask_glink3_in,
    input wire [1:0]    Mask_glink4_in,
    input wire [1:0]    Mask_glink5_in,
    input wire [1:0]    Mask_glink6_in,
    input wire [1:0]    Mask_glink7_in,
    input wire [1:0]    Mask_glink8_in,
    input wire [1:0]    Mask_glink9_in,
    input wire [1:0]    Mask_glink10_in,
    input wire [1:0]    Mask_glink11_in,
    input wire [1:0]    Mask_glink12_in,
    input wire [1:0]    Mask_glink13_in,

    input wire [111:0]  gtx0_test_data_in,
    input wire [111:0]  gtx1_test_data_in,
    input wire [111:0]  gtx2_test_data_in,
    input wire [111:0]  gtx3_test_data_in,
    input wire [111:0]  gtx4_test_data_in,
    input wire [111:0]  gtx5_test_data_in,
    input wire [111:0]  gtx6_test_data_in,
    input wire [111:0]  gtx7_test_data_in,
    input wire [111:0]  gtx8_test_data_in,
    input wire [111:0]  gtx9_test_data_in,
    input wire [111:0]  gtx10_test_data_in,
    input wire [111:0]  gtx11_test_data_in,
    input wire [16:0]   glink0_test_data_in,
    input wire [16:0]   glink1_test_data_in,
    input wire [16:0]   glink2_test_data_in,
    input wire [16:0]   glink3_test_data_in,
    input wire [16:0]   glink4_test_data_in,
    input wire [16:0]   glink5_test_data_in,
    input wire [16:0]   glink6_test_data_in,
    input wire [16:0]   glink7_test_data_in,
    input wire [16:0]   glink8_test_data_in,
    input wire [16:0]   glink9_test_data_in,
    input wire [16:0]   glink10_test_data_in,
    input wire [16:0]   glink11_test_data_in,
    input wire [16:0]   glink12_test_data_in,
    input wire [16:0]   glink13_test_data_in,

    output wire [111:0]  gtx0_data_out,
    output wire [111:0]  gtx1_data_out,
    output wire [111:0]  gtx2_data_out,
    output wire [111:0]  gtx3_data_out,
    output wire [111:0]  gtx4_data_out,
    output wire [111:0]  gtx5_data_out,
    output wire [111:0]  gtx6_data_out,
    output wire [111:0]  gtx7_data_out,
    output wire [111:0]  gtx8_data_out,
    output wire [111:0]  gtx9_data_out,
    output wire [111:0]  gtx10_data_out,
    output wire [111:0]  gtx11_data_out,

    output wire [16:0]   glink0_data_out,
    output wire [16:0]   glink1_data_out,
    output wire [16:0]   glink2_data_out,
    output wire [16:0]   glink3_data_out,
    output wire [16:0]   glink4_data_out,
    output wire [16:0]   glink5_data_out,
    output wire [16:0]   glink6_data_out,
    output wire [16:0]   glink7_data_out,
    output wire [16:0]   glink8_data_out,
    output wire [16:0]   glink9_data_out,
    output wire [16:0]   glink10_data_out,
    output wire [16:0]   glink11_data_out,
    output wire [16:0]   glink12_data_out,
    output wire [16:0]   glink13_data_out,

    output wire [111:0]  trig_gtx0_data_out,
    output wire [111:0]  trig_gtx1_data_out,
    output wire [111:0]  trig_gtx2_data_out,
    output wire [111:0]  trig_gtx3_data_out,
    output wire [111:0]  trig_gtx4_data_out,
    output wire [111:0]  trig_gtx5_data_out,
    output wire [111:0]  trig_gtx6_data_out,
    output wire [111:0]  trig_gtx7_data_out,
    output wire [111:0]  trig_gtx8_data_out,
    output wire [111:0]  trig_gtx9_data_out,
    output wire [111:0]  trig_gtx10_data_out,
    output wire [111:0]  trig_gtx11_data_out,

    output wire [16:0]   trig_glink0_data_out,
    output wire [16:0]   trig_glink1_data_out,
    output wire [16:0]   trig_glink2_data_out,
    output wire [16:0]   trig_glink3_data_out,
    output wire [16:0]   trig_glink4_data_out,
    output wire [16:0]   trig_glink5_data_out,
    output wire [16:0]   trig_glink6_data_out,
    output wire [16:0]   trig_glink7_data_out,
    output wire [16:0]   trig_glink8_data_out,
    output wire [16:0]   trig_glink9_data_out,
    output wire [16:0]   trig_glink10_data_out,
    output wire [16:0]   trig_glink11_data_out,
    output wire [16:0]   trig_glink12_data_out,
    output wire [16:0]   trig_glink13_data_out,

    output reg          test_pulse_out
    );

//reg [1:0] shift_test_enb = 2'b0;
//wire test_enb;
//assign test_enb = !shift_test_enb[1] & shift_test_enb[0];
reg [15:0] test_pulse_counter = 16'b0;
reg [15:0] test_pulse_wait_counter = 16'b0;


assign trig_glink13_data_out = Mask_glink13_in[1] ? (test_pulse_out ? glink13_test_data_in : 0 ) : glink13_data_in;
assign trig_glink12_data_out = Mask_glink12_in[1] ? (test_pulse_out ? glink12_test_data_in : 0 ) : glink12_data_in;
assign trig_glink11_data_out = Mask_glink11_in[1] ? (test_pulse_out ? glink11_test_data_in : 0 ) : glink11_data_in;
assign trig_glink10_data_out = Mask_glink10_in[1] ? (test_pulse_out ? glink10_test_data_in : 0 ) : glink10_data_in;
assign trig_glink9_data_out = Mask_glink9_in[1]  ? (test_pulse_out ? glink9_test_data_in : 0 ) : glink9_data_in;
assign trig_glink8_data_out = Mask_glink8_in[1]  ? (test_pulse_out ? glink8_test_data_in : 0 ) : glink8_data_in;
assign trig_glink7_data_out = Mask_glink7_in[1]  ? (test_pulse_out ? glink7_test_data_in : 0 ) : glink7_data_in;
assign trig_glink6_data_out = Mask_glink6_in[1]  ? (test_pulse_out ? glink6_test_data_in : 0 ) : glink6_data_in;
assign trig_glink5_data_out = Mask_glink5_in[1]  ? (test_pulse_out ? glink5_test_data_in : 0 ) : glink5_data_in;
assign trig_glink4_data_out = Mask_glink4_in[1]  ? (test_pulse_out ? glink4_test_data_in : 0 ) : glink4_data_in;
assign trig_glink3_data_out = Mask_glink3_in[1]  ? (test_pulse_out ? glink3_test_data_in : 0 ) : glink3_data_in;
assign trig_glink2_data_out = Mask_glink2_in[1]  ? (test_pulse_out ? glink2_test_data_in : 0 ) : glink2_data_in;
assign trig_glink1_data_out = Mask_glink1_in[1]  ? (test_pulse_out ? glink1_test_data_in : 0 ) : glink1_data_in;
assign trig_glink0_data_out = Mask_glink0_in[1]  ? (test_pulse_out ? glink0_test_data_in : 0 ) : glink0_data_in;

assign trig_gtx11_data_out = Mask_gtx11_in[1] ? (test_pulse_out ? gtx11_test_data_in : 0 ) : gtx11_data_in;
assign trig_gtx10_data_out = Mask_gtx10_in[1] ? (test_pulse_out ? gtx10_test_data_in : 0 ) : gtx10_data_in;
assign trig_gtx9_data_out = Mask_gtx9_in[1]  ? (test_pulse_out ? gtx9_test_data_in  : 0 ) : gtx9_data_in;
assign trig_gtx8_data_out = Mask_gtx8_in[1]  ? (test_pulse_out ? gtx8_test_data_in  : 0 ) : gtx8_data_in;
assign trig_gtx7_data_out = Mask_gtx7_in[1]  ? (test_pulse_out ? gtx7_test_data_in  : 0 ) : gtx7_data_in;
assign trig_gtx6_data_out = Mask_gtx6_in[1]  ? (test_pulse_out ? gtx6_test_data_in  : 0 ) : gtx6_data_in;
assign trig_gtx5_data_out = Mask_gtx5_in[1]  ? (test_pulse_out ? gtx5_test_data_in  : 0 ) : gtx5_data_in;
assign trig_gtx4_data_out = Mask_gtx4_in[1]  ? (test_pulse_out ? gtx4_test_data_in  : 0 ) : gtx4_data_in;
assign trig_gtx3_data_out = Mask_gtx3_in[1]  ? (test_pulse_out ? gtx3_test_data_in  : 0 ) : gtx3_data_in;
assign trig_gtx2_data_out = Mask_gtx2_in[1]  ? (test_pulse_out ? gtx2_test_data_in  : 0 ) : gtx2_data_in;
assign trig_gtx1_data_out = Mask_gtx1_in[1]  ? (test_pulse_out ? gtx1_test_data_in  : 0 ) : gtx1_data_in;
assign trig_gtx0_data_out = Mask_gtx0_in[1]  ? (test_pulse_out ? gtx0_test_data_in  : 0 ) : gtx0_data_in;

assign glink13_data_out = Mask_glink13_in[0] ? (test_pulse_out ? glink13_test_data_in : 0 ) : glink13_data_in;
assign glink12_data_out = Mask_glink12_in[0] ? (test_pulse_out ? glink12_test_data_in : 0 ) : glink12_data_in;
assign glink11_data_out = Mask_glink11_in[0] ? (test_pulse_out ? glink11_test_data_in : 0 ) : glink11_data_in;
assign glink10_data_out = Mask_glink10_in[0] ? (test_pulse_out ? glink10_test_data_in : 0 ) : glink10_data_in;
assign glink9_data_out = Mask_glink9_in[0]  ? (test_pulse_out ? glink9_test_data_in  : 0 ) : glink9_data_in;
assign glink8_data_out = Mask_glink8_in[0]  ? (test_pulse_out ? glink8_test_data_in  : 0 ) : glink8_data_in;
assign glink7_data_out = Mask_glink7_in[0]  ? (test_pulse_out ? glink7_test_data_in  : 0 ) : glink7_data_in;
assign glink6_data_out = Mask_glink6_in[0]  ? (test_pulse_out ? glink6_test_data_in  : 0 ) : glink6_data_in;
assign glink5_data_out = Mask_glink5_in[0]  ? (test_pulse_out ? glink5_test_data_in  : 0 ) : glink5_data_in;
assign glink4_data_out = Mask_glink4_in[0]  ? (test_pulse_out ? glink4_test_data_in  : 0 ) : glink4_data_in;
assign glink3_data_out = Mask_glink3_in[0]  ? (test_pulse_out ? glink3_test_data_in  : 0 ) : glink3_data_in;
assign glink2_data_out = Mask_glink2_in[0]  ? (test_pulse_out ? glink2_test_data_in  : 0 ) : glink2_data_in;
assign glink1_data_out = Mask_glink1_in[0]  ? (test_pulse_out ? glink1_test_data_in  : 0 ) : glink1_data_in;
assign glink0_data_out = Mask_glink0_in[0]  ? (test_pulse_out ? glink0_test_data_in  : 0 ) : glink0_data_in;

assign gtx11_data_out = Mask_gtx11_in[0] ? (test_pulse_out ? gtx11_test_data_in : 0 ) : gtx11_data_in;
assign gtx10_data_out = Mask_gtx10_in[0] ? (test_pulse_out ? gtx10_test_data_in : 0 ) : gtx10_data_in;
assign gtx9_data_out = Mask_gtx9_in[0]  ? (test_pulse_out ? gtx9_test_data_in  : 0 ) : gtx9_data_in;
assign gtx8_data_out = Mask_gtx8_in[0]  ? (test_pulse_out ? gtx8_test_data_in  : 0 ) : gtx8_data_in;
assign gtx7_data_out = Mask_gtx7_in[0]  ? (test_pulse_out ? gtx7_test_data_in  : 0 ) : gtx7_data_in;
assign gtx6_data_out = Mask_gtx6_in[0]  ? (test_pulse_out ? gtx6_test_data_in  : 0 ) : gtx6_data_in;
assign gtx5_data_out = Mask_gtx5_in[0]  ? (test_pulse_out ? gtx5_test_data_in  : 0 ) : gtx5_data_in;
assign gtx4_data_out = Mask_gtx4_in[0]  ? (test_pulse_out ? gtx4_test_data_in  : 0 ) : gtx4_data_in;
assign gtx3_data_out = Mask_gtx3_in[0]  ? (test_pulse_out ? gtx3_test_data_in  : 0 ) : gtx3_data_in;
assign gtx2_data_out = Mask_gtx2_in[0]  ? (test_pulse_out ? gtx2_test_data_in  : 0 ) : gtx2_data_in;
assign gtx1_data_out = Mask_gtx1_in[0]  ? (test_pulse_out ? gtx1_test_data_in  : 0 ) : gtx1_data_in;
assign gtx0_data_out = Mask_gtx0_in[0]  ? (test_pulse_out ? gtx0_test_data_in  : 0 ) : gtx0_data_in;

/*
always@( posedge CLK_in or posedge reset_in ) begin
    shift_test_enb[1:0] <= {shift_test_enb[0], Test_Pulse_Enable};
    if (reset_in) begin
        test_pulse_counter[15:0] <= 16'b0;
        test_pulse_out <= 1'b0;
    end
    else if (test_enb) begin
        test_pulse_counter[15:0] <= Test_Pulse_Length[15:0];
        test_pulse_out <= 1'b0;        
    end
    else if (test_pulse_counter != 16'b0) begin
        test_pulse_counter[15:0] <= test_pulse_counter[15:0] - 16'b1;
        test_pulse_out <= 1'b1;
    end
    else if (test_pulse_counter == 16'b0) begin
        test_pulse_counter[15:0] <= 16'b0;
        test_pulse_out <= 1'b0;        
    end

end
*/
always@( posedge CLK_in or posedge reset_in ) begin
    if (reset_in) begin
        test_pulse_counter[15:0] <= 16'b0;
        test_pulse_wait_counter[15:0] <= 16'b0;
        test_pulse_out <= 1'b0;
    end
    else if (test_pulse_counter != 16'b0) begin
        test_pulse_counter[15:0] <= test_pulse_counter[15:0] - 16'b1;
        test_pulse_out <= 1'b1;
    end
    else begin
        if(test_pulse_wait_counter != 16'b0) begin
            test_pulse_wait_counter[15:0] <= test_pulse_wait_counter[15:0] - 16'b1;
            test_pulse_out <= 1'b0;
        end
        else begin        
            test_pulse_counter[15:0] <= Test_Pulse_Length[15:0];
            test_pulse_wait_counter[15:0] <= Test_Pulse_Wait_Length[15:0];
            test_pulse_out <= 1'b0;
        end        
    end
end




endmodule
