`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2018/08/06 15:52:20
// Design Name: 
// Module Name: GLinkChecker
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLinkChecker(
    input wire CLK_in,
    input wire RESET_in, 
    input wire RXDATA_in,
    input wire RXCNTL_in,
    input wire RXERROR_in,
    input wire RXREADY_in,
    input wire StateRESET_in,
    input wire [1:0] RX_DIV_vme_in,
    input wire RX_DIVen_in,    
    input wire ErrorCounterReset_in,
    
    output reg [15:0] GlinkState_out,
    output reg [15:0] ErrorCounter_out,
    output reg GlinkOK_out,
    output reg [1:0] RX_DIV_out
    );
  
  
// check state BAD ONCE
reg RXDATA_BAD;
reg RXCNTL_BAD;
reg RXERROR_BAD;
reg RXREADY_BAD;
// DIV Setup
reg [19:0] WaitAfterReset;
reg [1:0] DIV;
reg [15:0] WaitCounter;
reg DivResetNow;
reg WaitGstateOK;

//assign RX_DIV_out = (RX_DIVen_in) ? RX_DIV_vme_in : DIV;

wire StateRESET;
assign StateRESET = RESET_in & StateRESET_in; // ???  

// OutPut Glink OK state
wire GStateOK;
wire GlinkOK;
assign GStateOK = ({RXDATA_in && !RXCNTL_in && !RXERROR_in && RXREADY_in} || {!RXDATA_in && !RXCNTL_in && !RXERROR_in && RXREADY_in})? 1'b1:1'b0; // DataWord or IdleMode
assign GlinkOK = ({RXDATA_in && !RXCNTL_in && !RXERROR_in && RXREADY_in} && !WaitGstateOK && !DivResetNow)? 1'b1:1'b0; // To Main Logic FPGA

always @(posedge CLK_in) begin

    RX_DIV_out = (RX_DIVen_in) ? RX_DIV_vme_in : DIV;
    
	GlinkState_out <= {	RESET_in, StateRESET_in, GlinkOK, GStateOK,
					    2'b0, RX_DIV_out,
					    RXDATA_in, RXCNTL_in, RXERROR_in, RXREADY_in, //link bat now
					    RXDATA_BAD, RXCNTL_BAD, RXERROR_BAD, RXREADY_BAD}; //link bat at least once					
end

always @(posedge CLK_in or negedge RESET_in) begin
	if (!RESET_in) begin
		RXDATA_BAD <= 0;
	end
	else if(!WaitAfterReset[19])begin
		RXDATA_BAD <= 0;
	end
	else if(!StateRESET)begin
		RXDATA_BAD <= 0;
	end
	else if (!RXDATA_in && ({RXDATA_in, RXCNTL_in, RXERROR_in, RXREADY_in }!=4'b0001)) begin
		RXDATA_BAD <= 1;
	end
end

always @(posedge CLK_in or negedge RESET_in) begin
	if (!RESET_in) begin
		RXCNTL_BAD <= 0;
	end
	else if(!WaitAfterReset[19])begin
		RXCNTL_BAD <= 0;
	end
	else if(!StateRESET)begin
		RXCNTL_BAD <= 0;
	end
	else if (RXCNTL_in) begin
		RXCNTL_BAD <= 1;
	end
end

always @(posedge CLK_in or negedge RESET_in) begin
	if (!RESET_in) begin
		RXERROR_BAD <= 0;
	end
	else if(!WaitAfterReset[19])begin
		RXERROR_BAD <= 0;
	end
	else if(!StateRESET)begin
		RXERROR_BAD <= 0;
	end
	else if (RXERROR_in) begin
		RXERROR_BAD <= 1;
	end
end

always @(posedge CLK_in or negedge RESET_in) begin
	if (!RESET_in) begin
		RXREADY_BAD <= 0;
	end
	else if(!WaitAfterReset[19])begin
		RXREADY_BAD <= 0;
	end
	else if(!StateRESET)begin
		RXREADY_BAD <= 0;
	end
	else if (!RXREADY_in) begin
		RXREADY_BAD <= 1;
	end
end

always@(posedge CLK_in or negedge RESET_in)begin
// SetUp	
	if(!RESET_in)begin
		if(!GlinkOK)begin //ver06
		DIV <= 2'b01;
		WaitCounter <= 0;
		DivResetNow <= 1'b0;
		WaitGstateOK <= 1'b0;
		ErrorCounter_out <= 16'b0;
		WaitAfterReset <= 0;
		end
	end
	else if(!ErrorCounterReset_in)begin
		DIV <= 2'b01;
		WaitCounter <= 0;
		DivResetNow <= 1'b0;
		WaitGstateOK <= 1'b0;
		ErrorCounter_out <= 16'b0;
	end
	else if(!WaitAfterReset[19])begin
		WaitAfterReset <= WaitAfterReset + 1;
		ErrorCounter_out <= 16'b0;
		WaitCounter <= 0;
	end
	else if(WaitGstateOK && WaitCounter[15] && GStateOK)begin
		WaitGstateOK <= 1'b0;
		WaitCounter <= 0;
		DivResetNow <= 1'b0;
	end
	else if(WaitGstateOK && WaitCounter[15] && !GStateOK)begin
		WaitGstateOK <= 1'b0;
	end
	else if(WaitGstateOK)begin
		WaitCounter <= WaitCounter + 1;
	end
	else if(WaitCounter[15] && DIV == 2'b10)begin
		DIV <= 2'b01;
		WaitCounter <= 0;
		WaitGstateOK <= 1'b1;
	end
	else if(DIV == 2'b10)begin
		WaitCounter <= WaitCounter + 1;
	end
	else if(WaitCounter[5])begin
		DIV <= 2'b10;
		DivResetNow <= 1'b1;
		ErrorCounter_out <= ErrorCounter_out + 16'b1;
		WaitCounter <= WaitCounter + 1;
	end
//	else if(!GlinkOK || !rxreset)begin
	else if(!GStateOK)begin
		WaitCounter <= WaitCounter + 1;
	end

end

    
endmodule
