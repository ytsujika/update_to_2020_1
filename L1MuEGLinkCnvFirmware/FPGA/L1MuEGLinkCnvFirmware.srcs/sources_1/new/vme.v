`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/06/06 14:46:46
// Design Name: 
// Module Name: vme
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define    Addr_TEST                         12'h0
`define    Addr_BITFILE_VERSION              12'hb

`define    Addr_RESET                        12'h101
`define    Addr_SCALER_RESET                 12'h102
`define    Addr_GTX_TX_RESET                 12'h103
`define    Addr_GTX_RX_RESET                 12'h104
`define    Addr_DELAY_RESET                  12'h105
`define    Addr_L1BUFFER_RESET               12'h106
`define    Addr_DERANDOMIZER_RESET           12'h107
`define    Addr_ZERO_SUPP_RESET              12'h108
`define    Addr_CLK_RESET                    12'h109
`define    Addr_FIFO_RESET                   12'h10a
`define    Addr_SITCP_RESET                  12'h10b
`define    Addr_SITCP_FIFO_RESET             12'h10c
`define    Addr_TX_LOGIC_RESET               12'h10d
`define    Addr_MONITORING_RESET             12'h10e
`define    Addr_LUT_INIT_RESET               12'h10f

`define    Addr_GT0_RX_RESET               12'h110
`define    Addr_GT1_RX_RESET               12'h111
`define    Addr_GT2_RX_RESET               12'h112
`define    Addr_GT3_RX_RESET               12'h113
`define    Addr_GT4_RX_RESET               12'h114
`define    Addr_GT5_RX_RESET               12'h115
`define    Addr_GT6_RX_RESET               12'h116
`define    Addr_GT7_RX_RESET               12'h117
`define    Addr_GT8_RX_RESET               12'h118
`define    Addr_GT9_RX_RESET               12'h119
`define    Addr_GT10_RX_RESET               12'h11a
`define    Addr_GT11_RX_RESET               12'h11b

`define    Addr_ERROR_SCALER_GT0             12'h200
`define    Addr_ERROR_SCALER_GT1             12'h201
`define    Addr_ERROR_SCALER_GT2             12'h202
`define    Addr_ERROR_SCALER_GT3             12'h203
`define    Addr_ERROR_SCALER_GT4             12'h204
`define    Addr_ERROR_SCALER_GT5             12'h205
`define    Addr_ERROR_SCALER_GT6             12'h206
`define    Addr_ERROR_SCALER_GT7             12'h207
`define    Addr_ERROR_SCALER_GT8             12'h208
`define    Addr_ERROR_SCALER_GT9             12'h209
`define    Addr_ERROR_SCALER_GT10            12'h20a
`define    Addr_ERROR_SCALER_GT11            12'h20b

`define    Addr_STATUS_GT0                   12'h210
`define    Addr_STATUS_GT1                   12'h211
`define    Addr_STATUS_GT2                   12'h212
`define    Addr_STATUS_GT3                   12'h213
`define    Addr_STATUS_GT4                   12'h214
`define    Addr_STATUS_GT5                   12'h215
`define    Addr_STATUS_GT6                   12'h216
`define    Addr_STATUS_GT7                   12'h217
`define    Addr_STATUS_GT8                   12'h218
`define    Addr_STATUS_GT9                   12'h219
`define    Addr_STATUS_GT10                  12'h21a
`define    Addr_STATUS_GT11                  12'h21b

`define    Addr_DELAY_GTX0                   12'h300
`define    Addr_DELAY_GTX1                   12'h301
`define    Addr_DELAY_GTX2                   12'h302
`define    Addr_DELAY_GTX3                   12'h303
`define    Addr_DELAY_GTX4                   12'h304
`define    Addr_DELAY_GTX5                   12'h305
`define    Addr_DELAY_GTX6                   12'h306
`define    Addr_DELAY_GTX7                   12'h307
`define    Addr_DELAY_GTX8                   12'h308
`define    Addr_DELAY_GTX9                   12'h309
`define    Addr_DELAY_GTX10                  12'h30a
`define    Addr_DELAY_GTX11                  12'h30b

`define    Addr_DELAY_GLINK0                 12'h310
`define    Addr_DELAY_GLINK1                 12'h311
`define    Addr_DELAY_GLINK2                 12'h312
`define    Addr_DELAY_GLINK3                 12'h313
`define    Addr_DELAY_GLINK4                 12'h314
`define    Addr_DELAY_GLINK5                 12'h315
`define    Addr_DELAY_GLINK6                 12'h316
`define    Addr_DELAY_GLINK7                 12'h317
`define    Addr_DELAY_GLINK8                 12'h318
`define    Addr_DELAY_GLINK9                 12'h319
`define    Addr_DELAY_GLINK10                12'h31a
`define    Addr_DELAY_GLINK11                12'h31b
`define    Addr_DELAY_GLINK12                12'h31c
`define    Addr_DELAY_GLINK13                12'h31d

`define    Addr_DELAY_L1A                    12'h320
`define    Addr_DELAY_BCR                    12'h321
`define    Addr_DELAY_ECR                    12'h322
`define    Addr_DELAY_TTC_RESET              12'h323
`define    Addr_DELAY_TEST_PULSE             12'h324
`define    Addr_DELAY_TRIG_BCR               12'h325

`define    Addr_MASK_RX0                    12'h330
`define    Addr_MASK_RX1                    12'h331
`define    Addr_MASK_RX2                    12'h332
`define    Addr_MASK_RX3                    12'h333
`define    Addr_MASK_RX4                    12'h334
`define    Addr_MASK_RX5                    12'h335
`define    Addr_MASK_RX6                    12'h336
`define    Addr_MASK_RX7                    12'h337
`define    Addr_MASK_RX8                    12'h338
`define    Addr_MASK_RX9                    12'h339
`define    Addr_MASK_RX10                   12'h33a
`define    Addr_MASK_RX11                   12'h33b

`define    Addr_MASK_L1A                    12'h340
`define    Addr_MASK_BCR                    12'h341
`define    Addr_MASK_ECR                    12'h342
`define    Addr_MASK_TTC_RESET              12'h343
`define    Addr_MASK_TEST_PULSE             12'h344
`define    Addr_MASK_TRIG_BCR               12'h345

`define    Addr_MASK_GLINK0                 12'h350
`define    Addr_MASK_GLINK1                 12'h351
`define    Addr_MASK_GLINK2                 12'h352
`define    Addr_MASK_GLINK3                 12'h353
`define    Addr_MASK_GLINK4                 12'h354
`define    Addr_MASK_GLINK5                 12'h355
`define    Addr_MASK_GLINK6                 12'h356
`define    Addr_MASK_GLINK7                 12'h357
`define    Addr_MASK_GLINK8                 12'h358
`define    Addr_MASK_GLINK9                 12'h359
`define    Addr_MASK_GLINK10                12'h35a
`define    Addr_MASK_GLINK11                12'h35b
`define    Addr_MASK_GLINK12                12'h35c
`define    Addr_MASK_GLINK13                12'h35d

`define    Addr_TEST_PULSE_LENGTH           12'h360
`define    Addr_TEST_PULSE_ENABLE           12'h361
`define    Addr_TEST_PULSE_WAIT_LENGTH      12'h362

`define    Addr_MASK_BURSTSIGNAL            12'h370

`define    Addr_L1A_COUNTER0                12'h400
`define    Addr_L1A_COUNTER1                12'h401
`define    Addr_L1ID                        12'h402
`define    Addr_BCID                        12'h403
`define    Addr_SLID                        12'h404
`define    Addr_READOUT_BC                  12'h405
`define    Addr_L1BUFFER_DEPTH              12'h406
`define    Addr_TRIGL1BUFFER_DEPTH          12'h407
`define    Addr_L1BUFFER_BW_DEPTH           12'h408
`define    Addr_CONVERTERID                 12'h409


`define    Addr_DERANDOMIZER_COUNT1_1       12'h511
`define    Addr_DERANDOMIZER_COUNT1_2       12'h512
`define    Addr_DERANDOMIZER_COUNT2         12'h513
`define    Addr_DERANDOMIZER_COUNT3         12'h514

`define    Addr_ZERO_SUPPRESS_STATUS        12'h520
`define    Addr_ZERO_SUPPRESS_COUNT         12'h521
`define    Addr_DATA_SIZE                   12'h522

`define    Addr_SITCP_COUNT                 12'h530

`define    Addr_FULLBOARD_ID                12'h680

`define    Addr_MONITORING_FIFO_OUT_0        12'h700
`define    Addr_MONITORING_FIFO_OUT_1        12'h701
`define    Addr_MONITORING_FIFO_OUT_2        12'h702
`define    Addr_MONITORING_FIFO_OUT_3        12'h703
`define    Addr_MONITORING_FIFO_OUT_4        12'h704
`define    Addr_MONITORING_FIFO_OUT_5        12'h705
`define    Addr_MONITORING_FIFO_OUT_6        12'h706
`define    Addr_MONITORING_FIFO_OUT_7        12'h707
`define    Addr_MONITORING_FIFO_OUT_8        12'h708
`define    Addr_MONITORING_FIFO_OUT_9        12'h709
`define    Addr_MONITORING_FIFO_OUT_10       12'h70a
`define    Addr_MONITORING_FIFO_OUT_11       12'h70b
`define    Addr_MONITORING_FIFO_OUT_12       12'h70c
`define    Addr_MONITORING_FIFO_OUT_13       12'h70d
`define    Addr_MONITORING_FIFO_OUT_14       12'h70e
`define    Addr_MONITORING_FIFO_OUT_15       12'h70f

`define    Addr_MONITORING_FIFO_wr_en        12'h710
`define    Addr_MONITORING_FIFO_rd_en        12'h711
`define    Addr_LANE_SELECTOR                12'h712
`define    Addr_XADC_MON_ENABLE              12'h713
`define    Addr_XADC_MONITOR_OUT             12'h714
`define    Addr_XADC_MON_ADDR                12'h715
`define    Addr_GLINK_LANE_SELECTOR          12'h716

`define    Addr_L1A_MANUAL_PARAMETER         12'h720
`define    Addr_BUSY_PROPAGATION             12'h721

`define    Addr_GTX0_TEST_DATA1             12'h801
`define    Addr_GTX0_TEST_DATA2             12'h802
`define    Addr_GTX0_TEST_DATA3             12'h803
`define    Addr_GTX0_TEST_DATA4             12'h804
`define    Addr_GTX0_TEST_DATA5             12'h805
`define    Addr_GTX0_TEST_DATA6             12'h806
`define    Addr_GTX0_TEST_DATA7             12'h807
`define    Addr_GTX1_TEST_DATA1             12'h811
`define    Addr_GTX1_TEST_DATA2             12'h812
`define    Addr_GTX1_TEST_DATA3             12'h813
`define    Addr_GTX1_TEST_DATA4             12'h814
`define    Addr_GTX1_TEST_DATA5             12'h815
`define    Addr_GTX1_TEST_DATA6             12'h816
`define    Addr_GTX1_TEST_DATA7             12'h817
`define    Addr_GTX2_TEST_DATA1             12'h821
`define    Addr_GTX2_TEST_DATA2             12'h822
`define    Addr_GTX2_TEST_DATA3             12'h823
`define    Addr_GTX2_TEST_DATA4             12'h824
`define    Addr_GTX2_TEST_DATA5             12'h825
`define    Addr_GTX2_TEST_DATA6             12'h826
`define    Addr_GTX2_TEST_DATA7             12'h827
`define    Addr_GTX3_TEST_DATA1             12'h831
`define    Addr_GTX3_TEST_DATA2             12'h832
`define    Addr_GTX3_TEST_DATA3             12'h833
`define    Addr_GTX3_TEST_DATA4             12'h834
`define    Addr_GTX3_TEST_DATA5             12'h835
`define    Addr_GTX3_TEST_DATA6             12'h836
`define    Addr_GTX3_TEST_DATA7             12'h837
`define    Addr_GTX4_TEST_DATA1             12'h841
`define    Addr_GTX4_TEST_DATA2             12'h842
`define    Addr_GTX4_TEST_DATA3             12'h843
`define    Addr_GTX4_TEST_DATA4             12'h844
`define    Addr_GTX4_TEST_DATA5             12'h845
`define    Addr_GTX4_TEST_DATA6             12'h846
`define    Addr_GTX4_TEST_DATA7             12'h847
`define    Addr_GTX5_TEST_DATA1             12'h851
`define    Addr_GTX5_TEST_DATA2             12'h852
`define    Addr_GTX5_TEST_DATA3             12'h853
`define    Addr_GTX5_TEST_DATA4             12'h854
`define    Addr_GTX5_TEST_DATA5             12'h855
`define    Addr_GTX5_TEST_DATA6             12'h856
`define    Addr_GTX5_TEST_DATA7             12'h857
`define    Addr_GTX6_TEST_DATA1             12'h861
`define    Addr_GTX6_TEST_DATA2             12'h862
`define    Addr_GTX6_TEST_DATA3             12'h863
`define    Addr_GTX6_TEST_DATA4             12'h864
`define    Addr_GTX6_TEST_DATA5             12'h865
`define    Addr_GTX6_TEST_DATA6             12'h866
`define    Addr_GTX6_TEST_DATA7             12'h867
`define    Addr_GTX7_TEST_DATA1             12'h871
`define    Addr_GTX7_TEST_DATA2             12'h872
`define    Addr_GTX7_TEST_DATA3             12'h873
`define    Addr_GTX7_TEST_DATA4             12'h874
`define    Addr_GTX7_TEST_DATA5             12'h875
`define    Addr_GTX7_TEST_DATA6             12'h876
`define    Addr_GTX7_TEST_DATA7             12'h877
`define    Addr_GTX8_TEST_DATA1             12'h881
`define    Addr_GTX8_TEST_DATA2             12'h882
`define    Addr_GTX8_TEST_DATA3             12'h883
`define    Addr_GTX8_TEST_DATA4             12'h884
`define    Addr_GTX8_TEST_DATA5             12'h885
`define    Addr_GTX8_TEST_DATA6             12'h886
`define    Addr_GTX8_TEST_DATA7             12'h887
`define    Addr_GTX9_TEST_DATA1             12'h891
`define    Addr_GTX9_TEST_DATA2             12'h892
`define    Addr_GTX9_TEST_DATA3             12'h893
`define    Addr_GTX9_TEST_DATA4             12'h894
`define    Addr_GTX9_TEST_DATA5             12'h895
`define    Addr_GTX9_TEST_DATA6             12'h896
`define    Addr_GTX9_TEST_DATA7             12'h897
`define    Addr_GTX10_TEST_DATA1            12'h8a1
`define    Addr_GTX10_TEST_DATA2            12'h8a2
`define    Addr_GTX10_TEST_DATA3            12'h8a3
`define    Addr_GTX10_TEST_DATA4            12'h8a4
`define    Addr_GTX10_TEST_DATA5            12'h8a5
`define    Addr_GTX10_TEST_DATA6            12'h8a6
`define    Addr_GTX10_TEST_DATA7            12'h8a7
`define    Addr_GTX11_TEST_DATA1            12'h8b1
`define    Addr_GTX11_TEST_DATA2            12'h8b2
`define    Addr_GTX11_TEST_DATA3            12'h8b3
`define    Addr_GTX11_TEST_DATA4            12'h8b4
`define    Addr_GTX11_TEST_DATA5            12'h8b5
`define    Addr_GTX11_TEST_DATA6            12'h8b6
`define    Addr_GTX11_TEST_DATA7            12'h8b7
`define    Addr_GTX0_PHASE_SELECT	    12'h8c0
`define    Addr_GTX1_PHASE_SELECT	    12'h8c1
`define    Addr_GTX2_PHASE_SELECT	    12'h8c2
`define    Addr_GTX3_PHASE_SELECT	    12'h8c3
`define    Addr_GTX4_PHASE_SELECT	    12'h8c4
`define    Addr_GTX5_PHASE_SELECT	    12'h8c5
`define    Addr_GTX6_PHASE_SELECT	    12'h8c6
`define    Addr_GTX7_PHASE_SELECT	    12'h8c7
`define    Addr_GTX8_PHASE_SELECT	    12'h8c8
`define    Addr_GTX9_PHASE_SELECT	    12'h8c9
`define    Addr_GTX10_PHASE_SELECT	    12'h8ca
`define    Addr_GTX11_PHASE_SELECT	    12'h8cb

`define    Addr_GLINK0_TEST_DATA1          12'h901
`define    Addr_GLINK0_TEST_DATA2          12'h902
`define    Addr_GLINK1_TEST_DATA1          12'h911
`define    Addr_GLINK1_TEST_DATA2          12'h912
`define    Addr_GLINK2_TEST_DATA1          12'h921
`define    Addr_GLINK2_TEST_DATA2          12'h922
`define    Addr_GLINK3_TEST_DATA1          12'h931
`define    Addr_GLINK3_TEST_DATA2          12'h932
`define    Addr_GLINK4_TEST_DATA1          12'h941
`define    Addr_GLINK4_TEST_DATA2          12'h942
`define    Addr_GLINK5_TEST_DATA1          12'h951
`define    Addr_GLINK5_TEST_DATA2          12'h952
`define    Addr_GLINK6_TEST_DATA1          12'h961
`define    Addr_GLINK6_TEST_DATA2          12'h962
`define    Addr_GLINK7_TEST_DATA1          12'h971
`define    Addr_GLINK7_TEST_DATA2          12'h972
`define    Addr_GLINK8_TEST_DATA1          12'h981
`define    Addr_GLINK8_TEST_DATA2          12'h982
`define    Addr_GLINK9_TEST_DATA1          12'h991
`define    Addr_GLINK9_TEST_DATA2          12'h992
`define    Addr_GLINK10_TEST_DATA1         12'h9a1
`define    Addr_GLINK10_TEST_DATA2         12'h9a2
`define    Addr_GLINK11_TEST_DATA1         12'h9b1
`define    Addr_GLINK11_TEST_DATA2         12'h9b2
`define    Addr_GLINK12_TEST_DATA1         12'h9c1
`define    Addr_GLINK12_TEST_DATA2         12'h9c2
`define    Addr_GLINK13_TEST_DATA1         12'h9d1
`define    Addr_GLINK13_TEST_DATA2         12'h9d2
`define    Addr_GLINK0_PHASE_SELECT        12'h9e0
`define    Addr_GLINK1_PHASE_SELECT        12'h9e1
`define    Addr_GLINK2_PHASE_SELECT        12'h9e2
`define    Addr_GLINK3_PHASE_SELECT        12'h9e3
`define    Addr_GLINK4_PHASE_SELECT        12'h9e4
`define    Addr_GLINK5_PHASE_SELECT        12'h9e5
`define    Addr_GLINK6_PHASE_SELECT        12'h9e6
`define    Addr_GLINK7_PHASE_SELECT        12'h9e7
`define    Addr_GLINK8_PHASE_SELECT        12'h9e8
`define    Addr_GLINK9_PHASE_SELECT        12'h9e9
`define    Addr_GLINK10_PHASE_SELECT       12'h9ea
`define    Addr_GLINK11_PHASE_SELECT       12'h9eb
`define    Addr_GLINK12_PHASE_SELECT       12'h9ec
`define    Addr_GLINK13_PHASE_SELECT       12'h9ed

// LUT initialization
`define    Addr_LUT_INIT_MODE               12'ha00
`define    Addr_LUT_INIT_DATA               12'ha01
`define    Addr_LUT_INIT_ROI                12'ha02
`define    Addr_LUT_INIT_ADDRESS            12'ha03
`define    Addr_LUT_INIT_OUTPUT             12'ha04
`define    Addr_LUT_INIT_STATE              12'ha05
`define    Addr_LUT_RD_DATA                 12'ha06
`define    Addr_LUT_RD_ADDRESS              12'ha07
`define    Addr_LUT_RD_ROI                  12'ha08

// Control Flags on InnerCoincidence
`define    Addr_NSW_DISABLE_SSC_S0          12'haa0
`define    Addr_EI_DISABLE_SSC_S0           12'haa1
`define    Addr_TILE_DISABLE_SSC_S0         12'haa2
`define    Addr_RPC_DISABLE_SSC_S0          12'haa3
`define    Addr_NSW_DISABLE_SSC_S1          12'haa4
`define    Addr_EI_DISABLE_SSC_S1           12'haa5
`define    Addr_TILE_DISABLE_SSC_S1         12'haa6
`define    Addr_RPC_DISABLE_SSC_S1          12'haa7
`define    Addr_FI_DISABLE_SSC_S0           12'haa8
`define    Addr_FI_DISABLE_SSC_S1           12'haa9
`define    Addr_NSWORFISELECTION            12'haaa

// Decoder
`define    Addr_ALIGN_ETA_NSW_0             12'hb00
`define    Addr_ALIGN_ETA_NSW_1             12'hb01
`define    Addr_ALIGN_ETA_NSW_2             12'hb02
`define    Addr_ALIGN_ETA_NSW_3             12'hb03
`define    Addr_ALIGN_ETA_NSW_4             12'hb04
`define    Addr_ALIGN_ETA_NSW_5             12'hb05
`define    Addr_ALIGN_ETA_RPC               12'hb06
`define    Addr_ALIGN_PHI_NSW_0             12'hb10
`define    Addr_ALIGN_PHI_NSW_1             12'hb11
`define    Addr_ALIGN_PHI_NSW_2             12'hb12
`define    Addr_ALIGN_PHI_NSW_3             12'hb13
`define    Addr_ALIGN_PHI_NSW_4             12'hb14
`define    Addr_ALIGN_PHI_NSW_5             12'hb15
`define    Addr_ALIGN_PHI_RPC               12'hb16

// GTX Phase monitor
`define    Addr_GTX0_PHASE_MONITOR0          12'hcd0
`define    Addr_GTX1_PHASE_MONITOR0          12'hcd1
`define    Addr_GTX2_PHASE_MONITOR0          12'hcd2
`define    Addr_GTX3_PHASE_MONITOR0          12'hcd3
`define    Addr_GTX4_PHASE_MONITOR0          12'hcd4
`define    Addr_GTX5_PHASE_MONITOR0          12'hcd5
`define    Addr_GTX6_PHASE_MONITOR0          12'hcd6
`define    Addr_GTX7_PHASE_MONITOR0          12'hcd7
`define    Addr_GTX8_PHASE_MONITOR0          12'hcd8
`define    Addr_GTX9_PHASE_MONITOR0          12'hcd9
`define    Addr_GTX10_PHASE_MONITOR0         12'hcda
`define    Addr_GTX11_PHASE_MONITOR0         12'hcdb

`define    Addr_GTX0_PHASE_MONITOR1          12'hce0
`define    Addr_GTX1_PHASE_MONITOR1          12'hce1
`define    Addr_GTX2_PHASE_MONITOR1          12'hce2
`define    Addr_GTX3_PHASE_MONITOR1          12'hce3
`define    Addr_GTX4_PHASE_MONITOR1          12'hce4
`define    Addr_GTX5_PHASE_MONITOR1          12'hce5
`define    Addr_GTX6_PHASE_MONITOR1          12'hce6
`define    Addr_GTX7_PHASE_MONITOR1          12'hce7
`define    Addr_GTX8_PHASE_MONITOR1          12'hce8
`define    Addr_GTX9_PHASE_MONITOR1          12'hce9
`define    Addr_GTX10_PHASE_MONITOR1         12'hcea
`define    Addr_GTX11_PHASE_MONITOR1         12'hceb

`define    Addr_GTX0_PHASE_MONITOR2          12'hcf0
`define    Addr_GTX1_PHASE_MONITOR2          12'hcf1
`define    Addr_GTX2_PHASE_MONITOR2          12'hcf2
`define    Addr_GTX3_PHASE_MONITOR2          12'hcf3
`define    Addr_GTX4_PHASE_MONITOR2          12'hcf4
`define    Addr_GTX5_PHASE_MONITOR2          12'hcf5
`define    Addr_GTX6_PHASE_MONITOR2          12'hcf6
`define    Addr_GTX7_PHASE_MONITOR2          12'hcf7
`define    Addr_GTX8_PHASE_MONITOR2          12'hcf8
`define    Addr_GTX9_PHASE_MONITOR2          12'hcf9
`define    Addr_GTX10_PHASE_MONITOR2         12'hcfa
`define    Addr_GTX11_PHASE_MONITOR2         12'hcfb

`define    Addr_GTX0_PHASE_MONITOR3          12'hef0
`define    Addr_GTX1_PHASE_MONITOR3          12'hef1
`define    Addr_GTX2_PHASE_MONITOR3          12'hef2
`define    Addr_GTX3_PHASE_MONITOR3          12'hef3
`define    Addr_GTX4_PHASE_MONITOR3          12'hef4
`define    Addr_GTX5_PHASE_MONITOR3          12'hef5
`define    Addr_GTX6_PHASE_MONITOR3          12'hef6
`define    Addr_GTX7_PHASE_MONITOR3          12'hef7
`define    Addr_GTX8_PHASE_MONITOR3          12'hef8
`define    Addr_GTX9_PHASE_MONITOR3          12'hef9
`define    Addr_GTX10_PHASE_MONITOR3         12'hefa
`define    Addr_GTX11_PHASE_MONITOR3         12'hefb

// TrackOutputGenerator
`define    Addr_TRACKOUTPUTGENERATOR_ENV                        12'hc00
`define    Addr_TRACKOUTPUTGENERATOR0_S0_ROI                    12'hc10
`define    Addr_TRACKOUTPUTGENERATOR1_S0_ROI                    12'hc11
`define    Addr_TRACKOUTPUTGENERATOR2_S0_ROI                    12'hc12
`define    Addr_TRACKOUTPUTGENERATOR3_S0_ROI                    12'hc13
`define    Addr_TRACKOUTPUTGENERATOR0_S0_PT                     12'hc14
`define    Addr_TRACKOUTPUTGENERATOR1_S0_PT                     12'hc15
`define    Addr_TRACKOUTPUTGENERATOR2_S0_PT                     12'hc16
`define    Addr_TRACKOUTPUTGENERATOR3_S0_PT                     12'hc17
`define    Addr_TRACKOUTPUTGENERATOR0_S0_SIGN                   12'hc18
`define    Addr_TRACKOUTPUTGENERATOR1_S0_SIGN                   12'hc19
`define    Addr_TRACKOUTPUTGENERATOR2_S0_SIGN                   12'hc1a
`define    Addr_TRACKOUTPUTGENERATOR3_S0_SIGN                   12'hc1b
`define    Addr_TRACKOUTPUTGENERATOR0_S0_INNERCOINCIDENCEFLAG   12'hc1c
`define    Addr_TRACKOUTPUTGENERATOR1_S0_INNERCOINCIDENCEFLAG   12'hc1d
`define    Addr_TRACKOUTPUTGENERATOR2_S0_INNERCOINCIDENCEFLAG   12'hc1e
`define    Addr_TRACKOUTPUTGENERATOR3_S0_INNERCOINCIDENCEFLAG   12'hc1f
`define    Addr_FLAG_TRACKGENERATOR_S0                          12'hc20
`define    Addr_TRACKOUTPUTGENERATOR0_S1_ROI                    12'hc30
`define    Addr_TRACKOUTPUTGENERATOR1_S1_ROI                    12'hc31
`define    Addr_TRACKOUTPUTGENERATOR2_S1_ROI                    12'hc32
`define    Addr_TRACKOUTPUTGENERATOR3_S1_ROI                    12'hc33
`define    Addr_TRACKOUTPUTGENERATOR0_S1_PT                     12'hc34
`define    Addr_TRACKOUTPUTGENERATOR1_S1_PT                     12'hc35
`define    Addr_TRACKOUTPUTGENERATOR2_S1_PT                     12'hc36
`define    Addr_TRACKOUTPUTGENERATOR3_S1_PT                     12'hc37
`define    Addr_TRACKOUTPUTGENERATOR0_S1_SIGN                   12'hc38
`define    Addr_TRACKOUTPUTGENERATOR1_S1_SIGN                   12'hc39
`define    Addr_TRACKOUTPUTGENERATOR2_S1_SIGN                   12'hc3a
`define    Addr_TRACKOUTPUTGENERATOR3_S1_SIGN                   12'hc3b
`define    Addr_TRACKOUTPUTGENERATOR0_S1_INNERCOINCIDENCEFLAG   12'hc3c
`define    Addr_TRACKOUTPUTGENERATOR1_S1_INNERCOINCIDENCEFLAG   12'hc3d
`define    Addr_TRACKOUTPUTGENERATOR2_S1_INNERCOINCIDENCEFLAG   12'hc3e
`define    Addr_TRACKOUTPUTGENERATOR3_S1_INNERCOINCIDENCEFLAG   12'hc3f
`define    Addr_FLAG_TRACKGENERATOR_S1                          12'hc40
// Glink Monitor
`define    Addr_RESET_GLINKMONITOR                              12'hcff
`define    Addr_GL0_STATE_RESET                                 12'hd00
`define    Addr_GL1_STATE_RESET                                 12'hd01
`define    Addr_GL2_STATE_RESET                                 12'hd02
`define    Addr_GL3_STATE_RESET                                 12'hd03
`define    Addr_GL4_STATE_RESET                                 12'hd04
`define    Addr_GL5_STATE_RESET                                 12'hd05
`define    Addr_GL6_STATE_RESET                                 12'hd06
`define    Addr_GL7_STATE_RESET                                 12'hd07
`define    Addr_GL8_STATE_RESET                                 12'hd08
`define    Addr_GL9_STATE_RESET                                 12'hd09
`define    Addr_GL10_STATE_RESET                                12'hd0a
`define    Addr_GL11_STATE_RESET                                12'hd0b
`define    Addr_GL12_STATE_RESET                                12'hd0c
`define    Addr_GL13_STATE_RESET                                12'hd0d
`define    Addr_GL0_ERRORCOUNTER_RESET                          12'hd0e
`define    Addr_GL1_ERRORCOUNTER_RESET                          12'hd0f
`define    Addr_GL2_ERRORCOUNTER_RESET                          12'hd10
`define    Addr_GL3_ERRORCOUNTER_RESET                          12'hd11
`define    Addr_GL4_ERRORCOUNTER_RESET                          12'hd12
`define    Addr_GL5_ERRORCOUNTER_RESET                          12'hd13
`define    Addr_GL6_ERRORCOUNTER_RESET                          12'hd14
`define    Addr_GL7_ERRORCOUNTER_RESET                          12'hd15
`define    Addr_GL8_ERRORCOUNTER_RESET                          12'hd16
`define    Addr_GL9_ERRORCOUNTER_RESET                          12'hd17
`define    Addr_GL10_ERRORCOUNTER_RESET                         12'hd18
`define    Addr_GL11_ERRORCOUNTER_RESET                         12'hd19
`define    Addr_GL12_ERRORCOUNTER_RESET                         12'hd1a
`define    Addr_GL13_ERRORCOUNTER_RESET                         12'hd1b
`define    Addr_RX_DIVEN                                        12'hd1c
`define    Addr_GL0_RX_DIV_VME                                  12'hd1d
`define    Addr_GL1_RX_DIV_VME                                  12'hd1e
`define    Addr_GL2_RX_DIV_VME                                  12'hd1f
`define    Addr_GL3_RX_DIV_VME                                  12'hd20
`define    Addr_GL4_RX_DIV_VME                                  12'hd21
`define    Addr_GL5_RX_DIV_VME                                  12'hd22
`define    Addr_GL6_RX_DIV_VME                                  12'hd23
`define    Addr_GL7_RX_DIV_VME                                  12'hd24
`define    Addr_GL8_RX_DIV_VME                                  12'hd25
`define    Addr_GL9_RX_DIV_VME                                  12'hd26
`define    Addr_GL10_RX_DIV_VME                                 12'hd27
`define    Addr_GL11_RX_DIV_VME                                 12'hd28
`define    Addr_GL12_RX_DIV_VME                                 12'hd29
`define    Addr_GL13_RX_DIV_VME                                 12'hd2a
`define    Addr_GL0_STATE                                       12'hd2b
`define    Addr_GL1_STATE                                       12'hd2c
`define    Addr_GL2_STATE                                       12'hd2d
`define    Addr_GL3_STATE                                       12'hd2e
`define    Addr_GL4_STATE                                       12'hd2f
`define    Addr_GL5_STATE                                       12'hd30
`define    Addr_GL6_STATE                                       12'hd31
`define    Addr_GL7_STATE                                       12'hd32
`define    Addr_GL8_STATE                                       12'hd33
`define    Addr_GL9_STATE                                       12'hd34
`define    Addr_GL10_STATE                                      12'hd35
`define    Addr_GL11_STATE                                      12'hd36
`define    Addr_GL12_STATE                                      12'hd37
`define    Addr_GL13_STATE                                      12'hd38
`define    Addr_GL0_ERRORCOUNTER                                12'hd39
`define    Addr_GL1_ERRORCOUNTER                                12'hd3a
`define    Addr_GL2_ERRORCOUNTER                                12'hd3b
`define    Addr_GL3_ERRORCOUNTER                                12'hd3c
`define    Addr_GL4_ERRORCOUNTER                                12'hd3d
`define    Addr_GL5_ERRORCOUNTER                                12'hd3e
`define    Addr_GL6_ERRORCOUNTER                                12'hd3f
`define    Addr_GL7_ERRORCOUNTER                                12'hd40
`define    Addr_GL8_ERRORCOUNTER                                12'hd41
`define    Addr_GL9_ERRORCOUNTER                                12'hd42
`define    Addr_GL10_ERRORCOUNTER                               12'hd43
`define    Addr_GL11_ERRORCOUNTER                               12'hd44
`define    Addr_GL12_ERRORCOUNTER                               12'hd45
`define    Addr_GL13_ERRORCOUNTER                               12'hd46





module vme  (
    input wire CLK_40, // clock from Quartz
    input wire CLK_160,
    input wire TXUSRCLK_in,
    input wire OE,
    input wire CE,
    input wire WE,
    input wire [11:0] VME_A,
    inout wire [15:0] VME_D,

    output reg Reset_TTC_out, // Global resets
    output reg Reset_160_out, // Global resets
    output reg Reset_TX_out,  // Global resets

    output reg Scaler_reset_out,
    output reg GTX_TX_reset_out,
    output reg GTX_RX_reset_out,
    output reg Delay_reset_out,
    output reg CLK_reset_out,
    output reg FIFO_reset_out,
    output reg TX_Logic_reset_out,
    output reg Monitoring_reset_out,
    
    output reg gt0_rx_reset_out,
    output reg gt1_rx_reset_out,
    output reg gt2_rx_reset_out,
    output reg gt3_rx_reset_out,
    output reg gt4_rx_reset_out,
    output reg gt5_rx_reset_out,
    output reg gt6_rx_reset_out,
    output reg gt7_rx_reset_out,
    output reg gt8_rx_reset_out,
    output reg gt9_rx_reset_out,
    output reg gt10_rx_reset_out,
    output reg gt11_rx_reset_out,

    input wire [15:0] Error_Scaler_gt_0,
    input wire [15:0] Error_Scaler_gt_1,
    input wire [15:0] Error_Scaler_gt_2,
    input wire [15:0] Error_Scaler_gt_3,
    input wire [15:0] Error_Scaler_gt_4,
    input wire [15:0] Error_Scaler_gt_5,
    input wire [15:0] Error_Scaler_gt_6,
    input wire [15:0] Error_Scaler_gt_7,
    input wire [15:0] Error_Scaler_gt_8,
    input wire [15:0] Error_Scaler_gt_9,
    input wire [15:0] Error_Scaler_gt_10,
    input wire [15:0] Error_Scaler_gt_11,

    input wire [15:0] Status_gt_0,
    input wire [15:0] Status_gt_1,
    input wire [15:0] Status_gt_2,
    input wire [15:0] Status_gt_3,
    input wire [15:0] Status_gt_4,
    input wire [15:0] Status_gt_5,
    input wire [15:0] Status_gt_6,
    input wire [15:0] Status_gt_7,
    input wire [15:0] Status_gt_8,
    input wire [15:0] Status_gt_9,
    input wire [15:0] Status_gt_10,
    input wire [15:0] Status_gt_11,

    
    output reg [7:0] Delay_glink0,
    output reg [7:0] Delay_glink1,
    output reg [7:0] Delay_glink2,
    output reg [7:0] Delay_glink3,
    output reg [7:0] Delay_glink4,
    output reg [7:0] Delay_glink5,
    output reg [7:0] Delay_glink6,
    output reg [7:0] Delay_glink7,
    output reg [7:0] Delay_glink8,
    output reg [7:0] Delay_glink9,
    output reg [7:0] Delay_glink10,
    output reg [7:0] Delay_glink11,
    output reg [7:0] Delay_glink12,
    output reg [7:0] Delay_glink13,

    output reg [6:0] Delay_gtx0,
    output reg [6:0] Delay_gtx1,
    output reg [6:0] Delay_gtx2,
    output reg [6:0] Delay_gtx3,
    output reg [6:0] Delay_gtx4,
    output reg [6:0] Delay_gtx5,
    output reg [6:0] Delay_gtx6,
    output reg [6:0] Delay_gtx7,
    output reg [6:0] Delay_gtx8,
    output reg [6:0] Delay_gtx9,
    output reg [6:0] Delay_gtx10,
    output reg [6:0] Delay_gtx11,

    output reg [7:0] Delay_L1A,
    output reg [7:0] Delay_BCR,
    output reg [7:0] Delay_trig_BCR,
    output reg [7:0] Delay_ECR,
    output reg [7:0] Delay_TTC_RESET,
    output reg [7:0] Delay_TEST_PULSE,
        
    output reg [1:0] Mask_gtx0,
    output reg [1:0] Mask_gtx1,
    output reg [1:0] Mask_gtx2,
    output reg [1:0] Mask_gtx3,
    output reg [1:0] Mask_gtx4,
    output reg [1:0] Mask_gtx5,
    output reg [1:0] Mask_gtx6,
    output reg [1:0] Mask_gtx7,
    output reg [1:0] Mask_gtx8,
    output reg [1:0] Mask_gtx9,
    output reg [1:0] Mask_gtx10,
    output reg [1:0] Mask_gtx11,

    output reg Mask_L1A,
    output reg Mask_BCR,
    output reg Mask_trig_BCR,
    output reg Mask_ECR,
    output reg Mask_TTC_RESET,
    output reg Mask_TEST_PULSE,
    
    output reg [1:0] Mask_glink0,
    output reg [1:0] Mask_glink1,
    output reg [1:0] Mask_glink2,
    output reg [1:0] Mask_glink3,
    output reg [1:0] Mask_glink4,
    output reg [1:0] Mask_glink5,
    output reg [1:0] Mask_glink6,
    output reg [1:0] Mask_glink7,
    output reg [1:0] Mask_glink8,
    output reg [1:0] Mask_glink9,
    output reg [1:0] Mask_glink10,
    output reg [1:0] Mask_glink11,
    output reg [1:0] Mask_glink12,
    output reg [1:0] Mask_glink13,
    
    output reg [15:0] Test_Pulse_Length,
    output reg [15:0] Test_Pulse_Wait_Length,
    output reg Test_Pulse_Enable,
    
    input wire [15:0] L1A_Counter0,
    input wire [15:0] L1A_Counter1,
    input wire [11:0] L1ID,
    input wire [11:0] BCID,
    output reg [11:0] SLID,
    output reg CONVERTERID,
    
    input wire [15:0] monitoring_FIFO_out_0,
    input wire [15:0] monitoring_FIFO_out_1,
    input wire [15:0] monitoring_FIFO_out_2,
    input wire [15:0] monitoring_FIFO_out_3,
    input wire [15:0] monitoring_FIFO_out_4,
    input wire [15:0] monitoring_FIFO_out_5,
    input wire [15:0] monitoring_FIFO_out_6,
    input wire [15:0] monitoring_FIFO_out_7,
    input wire [15:0] monitoring_FIFO_out_8,
    input wire [15:0] monitoring_FIFO_out_9,
    input wire [15:0] monitoring_FIFO_out_10,
    input wire [15:0] monitoring_FIFO_out_11,
    input wire [15:0] monitoring_FIFO_out_12,
    input wire [15:0] monitoring_FIFO_out_13,
    input wire [15:0] monitoring_FIFO_out_14,
    input wire [15:0] monitoring_FIFO_out_15,

    output reg monitoring_FIFO_wr_en,
    output reg monitoring_FIFO_rd_en,
    output reg [6:0] lane_selector_out,  
    output reg [3:0] glink_lane_selector_out,
    output reg xadc_mon_enable_out,
    output reg [6:0] xadc_mon_addr_out,
    input wire [15:0] xadc_monitor_in,

    output reg [15:0] L1A_manual_parameter,
    
    output reg [15:0] gtx0_test_data1,
    output reg [15:0] gtx0_test_data2,
    output reg [15:0] gtx0_test_data3,
    output reg [15:0] gtx0_test_data4,
    output reg [15:0] gtx0_test_data5,
    output reg [15:0] gtx0_test_data6,
    output reg [15:0] gtx0_test_data7,
    output reg [15:0] gtx1_test_data1,
    output reg [15:0] gtx1_test_data2,
    output reg [15:0] gtx1_test_data3,
    output reg [15:0] gtx1_test_data4,
    output reg [15:0] gtx1_test_data5,
    output reg [15:0] gtx1_test_data6,
    output reg [15:0] gtx1_test_data7,
    output reg [15:0] gtx2_test_data1,
    output reg [15:0] gtx2_test_data2,
    output reg [15:0] gtx2_test_data3,
    output reg [15:0] gtx2_test_data4,
    output reg [15:0] gtx2_test_data5,
    output reg [15:0] gtx2_test_data6,
    output reg [15:0] gtx2_test_data7,
    output reg [15:0] gtx3_test_data1,
    output reg [15:0] gtx3_test_data2,
    output reg [15:0] gtx3_test_data3,
    output reg [15:0] gtx3_test_data4,
    output reg [15:0] gtx3_test_data5,
    output reg [15:0] gtx3_test_data6,
    output reg [15:0] gtx3_test_data7,
    output reg [15:0] gtx4_test_data1,
    output reg [15:0] gtx4_test_data2,
    output reg [15:0] gtx4_test_data3,
    output reg [15:0] gtx4_test_data4,
    output reg [15:0] gtx4_test_data5,
    output reg [15:0] gtx4_test_data6,
    output reg [15:0] gtx4_test_data7,
    output reg [15:0] gtx5_test_data1,
    output reg [15:0] gtx5_test_data2,
    output reg [15:0] gtx5_test_data3,
    output reg [15:0] gtx5_test_data4,
    output reg [15:0] gtx5_test_data5,
    output reg [15:0] gtx5_test_data6,
    output reg [15:0] gtx5_test_data7,
    output reg [15:0] gtx6_test_data1,
    output reg [15:0] gtx6_test_data2,
    output reg [15:0] gtx6_test_data3,
    output reg [15:0] gtx6_test_data4,
    output reg [15:0] gtx6_test_data5,
    output reg [15:0] gtx6_test_data6,
    output reg [15:0] gtx6_test_data7,
    output reg [15:0] gtx7_test_data1,
    output reg [15:0] gtx7_test_data2,
    output reg [15:0] gtx7_test_data3,
    output reg [15:0] gtx7_test_data4,
    output reg [15:0] gtx7_test_data5,
    output reg [15:0] gtx7_test_data6,
    output reg [15:0] gtx7_test_data7,
    output reg [15:0] gtx8_test_data1,
    output reg [15:0] gtx8_test_data2,
    output reg [15:0] gtx8_test_data3,
    output reg [15:0] gtx8_test_data4,
    output reg [15:0] gtx8_test_data5,
    output reg [15:0] gtx8_test_data6,
    output reg [15:0] gtx8_test_data7,
    output reg [15:0] gtx9_test_data1,
    output reg [15:0] gtx9_test_data2,
    output reg [15:0] gtx9_test_data3,
    output reg [15:0] gtx9_test_data4,
    output reg [15:0] gtx9_test_data5,
    output reg [15:0] gtx9_test_data6,
    output reg [15:0] gtx9_test_data7,
    output reg [15:0] gtx10_test_data1,
    output reg [15:0] gtx10_test_data2,
    output reg [15:0] gtx10_test_data3,
    output reg [15:0] gtx10_test_data4,
    output reg [15:0] gtx10_test_data5,
    output reg [15:0] gtx10_test_data6,
    output reg [15:0] gtx10_test_data7,
    output reg [15:0] gtx11_test_data1,
    output reg [15:0] gtx11_test_data2,
    output reg [15:0] gtx11_test_data3,
    output reg [15:0] gtx11_test_data4,
    output reg [15:0] gtx11_test_data5,
    output reg [15:0] gtx11_test_data6,
    output reg [15:0] gtx11_test_data7,

    output reg [3:0]  gtx0_phase_select,
    output reg [3:0]  gtx1_phase_select,
    output reg [3:0]  gtx2_phase_select,
    output reg [3:0]  gtx3_phase_select,
    output reg [3:0]  gtx4_phase_select,
    output reg [3:0]  gtx5_phase_select,
    output reg [3:0]  gtx6_phase_select,
    output reg [3:0]  gtx7_phase_select,
    output reg [3:0]  gtx8_phase_select,
    output reg [3:0]  gtx9_phase_select,
    output reg [3:0]  gtx10_phase_select,
    output reg [3:0]  gtx11_phase_select,

    input  wire [5:0] gtx0_phase_monitor_0,
    input  wire [5:0] gtx1_phase_monitor_0,
    input  wire [5:0] gtx2_phase_monitor_0,
    input  wire [5:0] gtx3_phase_monitor_0,
    input  wire [5:0] gtx4_phase_monitor_0,
    input  wire [5:0] gtx5_phase_monitor_0,
    input  wire [5:0] gtx6_phase_monitor_0,
    input  wire [5:0] gtx7_phase_monitor_0,
    input  wire [5:0] gtx8_phase_monitor_0,
    input  wire [5:0] gtx9_phase_monitor_0,
    input  wire [5:0] gtx10_phase_monitor_0,
    input  wire [5:0] gtx11_phase_monitor_0,

    input  wire [5:0] gtx0_phase_monitor_1,
    input  wire [5:0] gtx1_phase_monitor_1,
    input  wire [5:0] gtx2_phase_monitor_1,
    input  wire [5:0] gtx3_phase_monitor_1,
    input  wire [5:0] gtx4_phase_monitor_1,
    input  wire [5:0] gtx5_phase_monitor_1,
    input  wire [5:0] gtx6_phase_monitor_1,
    input  wire [5:0] gtx7_phase_monitor_1,
    input  wire [5:0] gtx8_phase_monitor_1,
    input  wire [5:0] gtx9_phase_monitor_1,
    input  wire [5:0] gtx10_phase_monitor_1,
    input  wire [5:0] gtx11_phase_monitor_1,

    input  wire [5:0] gtx0_phase_monitor_2,
    input  wire [5:0] gtx1_phase_monitor_2,
    input  wire [5:0] gtx2_phase_monitor_2,
    input  wire [5:0] gtx3_phase_monitor_2,
    input  wire [5:0] gtx4_phase_monitor_2,
    input  wire [5:0] gtx5_phase_monitor_2,
    input  wire [5:0] gtx6_phase_monitor_2,
    input  wire [5:0] gtx7_phase_monitor_2,
    input  wire [5:0] gtx8_phase_monitor_2,
    input  wire [5:0] gtx9_phase_monitor_2,
    input  wire [5:0] gtx10_phase_monitor_2,
    input  wire [5:0] gtx11_phase_monitor_2,

    input  wire [5:0] gtx0_phase_monitor_3,
    input  wire [5:0] gtx1_phase_monitor_3,
    input  wire [5:0] gtx2_phase_monitor_3,
    input  wire [5:0] gtx3_phase_monitor_3,
    input  wire [5:0] gtx4_phase_monitor_3,
    input  wire [5:0] gtx5_phase_monitor_3,
    input  wire [5:0] gtx6_phase_monitor_3,
    input  wire [5:0] gtx7_phase_monitor_3,
    input  wire [5:0] gtx8_phase_monitor_3,
    input  wire [5:0] gtx9_phase_monitor_3,
    input  wire [5:0] gtx10_phase_monitor_3,
    input  wire [5:0] gtx11_phase_monitor_3,

    output reg [15:0] glink0_test_data1,
    output reg [3:0] glink0_test_data2,
    output reg [15:0] glink1_test_data1,
    output reg [3:0] glink1_test_data2,
    output reg [15:0] glink2_test_data1,
    output reg [3:0] glink2_test_data2,
    output reg [15:0] glink3_test_data1,
    output reg [3:0] glink3_test_data2,
    output reg [15:0] glink4_test_data1,
    output reg [3:0] glink4_test_data2,
    output reg [15:0] glink5_test_data1,
    output reg [3:0] glink5_test_data2,
    output reg [15:0] glink6_test_data1,
    output reg [3:0] glink6_test_data2,
    output reg [15:0] glink7_test_data1,
    output reg [3:0] glink7_test_data2,
    output reg [15:0] glink8_test_data1,
    output reg [3:0] glink8_test_data2,
    output reg [15:0] glink9_test_data1,
    output reg [3:0] glink9_test_data2,
    output reg [15:0] glink10_test_data1,
    output reg [3:0] glink10_test_data2,
    output reg [15:0] glink11_test_data1,
    output reg [3:0] glink11_test_data2,
    output reg [15:0] glink12_test_data1,
    output reg [3:0] glink12_test_data2,
    output reg [15:0] glink13_test_data1,
    output reg [3:0] glink13_test_data2,

    output reg [3:0] Phase_glink0,
    output reg [3:0] Phase_glink1,
    output reg [3:0] Phase_glink2,
    output reg [3:0] Phase_glink3,
    output reg [3:0] Phase_glink4,
    output reg [3:0] Phase_glink5,
    output reg [3:0] Phase_glink6,
    output reg [3:0] Phase_glink7,
    output reg [3:0] Phase_glink8,
    output reg [3:0] Phase_glink9,
    output reg [3:0] Phase_glink10,
    output reg [3:0] Phase_glink11,
    output reg [3:0] Phase_glink12,
    output reg [3:0] Phase_glink13,
        
    output reg TrackOutputGenerator_Enb,
    output reg [7:0] Trackgenerator0_S0_ROI,
    output reg [7:0] Trackgenerator1_S0_ROI,
    output reg [7:0] Trackgenerator2_S0_ROI,
    output reg [7:0] Trackgenerator3_S0_ROI,
    output reg [3:0] Trackgenerator0_S0_pT,    
    output reg [3:0] Trackgenerator1_S0_pT,    
    output reg [3:0] Trackgenerator2_S0_pT,    
    output reg [3:0] Trackgenerator3_S0_pT,    
    output reg Trackgenerator0_S0_sign,    
    output reg Trackgenerator1_S0_sign,    
    output reg Trackgenerator2_S0_sign,    
    output reg Trackgenerator3_S0_sign,    
    output reg [2:0] Trackgenerator0_S0_InnerCoincidenceFlag,    
    output reg [2:0] Trackgenerator1_S0_InnerCoincidenceFlag,    
    output reg [2:0] Trackgenerator2_S0_InnerCoincidenceFlag,    
    output reg [2:0] Trackgenerator3_S0_InnerCoincidenceFlag,    
    output reg [3:0] Flag_trackgenerator_S0,
    output reg [7:0] Trackgenerator0_S1_ROI,
    output reg [7:0] Trackgenerator1_S1_ROI,
    output reg [7:0] Trackgenerator2_S1_ROI,
    output reg [7:0] Trackgenerator3_S1_ROI,
    output reg [3:0] Trackgenerator0_S1_pT,    
    output reg [3:0] Trackgenerator1_S1_pT,    
    output reg [3:0] Trackgenerator2_S1_pT,    
    output reg [3:0] Trackgenerator3_S1_pT,    
    output reg Trackgenerator0_S1_sign,    
    output reg Trackgenerator1_S1_sign,    
    output reg Trackgenerator2_S1_sign,    
    output reg Trackgenerator3_S1_sign,    
    output reg [2:0] Trackgenerator0_S1_InnerCoincidenceFlag,    
    output reg [2:0] Trackgenerator1_S1_InnerCoincidenceFlag,    
    output reg [2:0] Trackgenerator2_S1_InnerCoincidenceFlag,    
    output reg [2:0] Trackgenerator3_S1_InnerCoincidenceFlag,    
    output reg [3:0] Flag_trackgenerator_S1,
    // Glink Monitor
    output reg Reset_GLinkMonitor,
    output reg GL0_State_Reset,
    output reg GL1_State_Reset,
    output reg GL2_State_Reset,
    output reg GL3_State_Reset,
    output reg GL4_State_Reset,
    output reg GL5_State_Reset,
    output reg GL6_State_Reset,
    output reg GL7_State_Reset,
    output reg GL8_State_Reset,
    output reg GL9_State_Reset,
    output reg GL10_State_Reset,
    output reg GL11_State_Reset,
    output reg GL12_State_Reset,
    output reg GL13_State_Reset,

    output reg GL0_ErrorCounter_Reset,       
    output reg GL1_ErrorCounter_Reset,       
    output reg GL2_ErrorCounter_Reset,       
    output reg GL3_ErrorCounter_Reset,       
    output reg GL4_ErrorCounter_Reset,       
    output reg GL5_ErrorCounter_Reset,       
    output reg GL6_ErrorCounter_Reset,       
    output reg GL7_ErrorCounter_Reset,       
    output reg GL8_ErrorCounter_Reset,       
    output reg GL9_ErrorCounter_Reset,       
    output reg GL10_ErrorCounter_Reset,       
    output reg GL11_ErrorCounter_Reset,       
    output reg GL12_ErrorCounter_Reset,       
    output reg GL13_ErrorCounter_Reset,       

    output reg RX_DIVen,
    output reg [1:0] GL0_RX_DIV_vme,  
    output reg [1:0] GL1_RX_DIV_vme,  
    output reg [1:0] GL2_RX_DIV_vme,  
    output reg [1:0] GL3_RX_DIV_vme,  
    output reg [1:0] GL4_RX_DIV_vme,  
    output reg [1:0] GL5_RX_DIV_vme,  
    output reg [1:0] GL6_RX_DIV_vme,  
    output reg [1:0] GL7_RX_DIV_vme,  
    output reg [1:0] GL8_RX_DIV_vme,  
    output reg [1:0] GL9_RX_DIV_vme,  
    output reg [1:0] GL10_RX_DIV_vme,  
    output reg [1:0] GL11_RX_DIV_vme,  
    output reg [1:0] GL12_RX_DIV_vme,  
    output reg [1:0] GL13_RX_DIV_vme,  

    input wire [15:0] GL0_State,
    input wire [15:0] GL1_State,
    input wire [15:0] GL2_State,
    input wire [15:0] GL3_State,
    input wire [15:0] GL4_State,
    input wire [15:0] GL5_State,
    input wire [15:0] GL6_State,
    input wire [15:0] GL7_State,
    input wire [15:0] GL8_State,
    input wire [15:0] GL9_State,
    input wire [15:0] GL10_State,
    input wire [15:0] GL11_State,
    input wire [15:0] GL12_State,
    input wire [15:0] GL13_State,
    
    input wire [15:0] GL0_ErrorCounter,
    input wire [15:0] GL1_ErrorCounter,
    input wire [15:0] GL2_ErrorCounter,
    input wire [15:0] GL3_ErrorCounter,
    input wire [15:0] GL4_ErrorCounter,
    input wire [15:0] GL5_ErrorCounter,
    input wire [15:0] GL6_ErrorCounter,
    input wire [15:0] GL7_ErrorCounter,
    input wire [15:0] GL8_ErrorCounter,
    input wire [15:0] GL9_ErrorCounter,
    input wire [15:0] GL10_ErrorCounter,
    input wire [15:0] GL11_ErrorCounter,
    input wire [15:0] GL12_ErrorCounter,
    input wire [15:0] GL13_ErrorCounter
       
    );
    
    
      
    //VME protocol
    wire rs,ws;
    assign rs = !(!OE & !CE);
    assign ws = !(!WE & !CE);
    wire [15:0] VME_DIN;     //VME data input
    (* ASYNC_REG = "TRUE" *) reg [15:0] VME_DOUT;    //VME data output
    (* ASYNC_REG = "TRUE" *) reg [11:0] VME_Address;
    (* ASYNC_REG = "TRUE" *) reg [15:0] VME_Data;
    assign VME_DIN = VME_D;
    assign VME_D = ((!OE & !CE)) ? VME_DOUT : 16'hzzzz;

    reg [15:0] Test_reg;    
    reg [15:0] bitfile_version;
   
initial begin
    Test_reg <= 16'h135;
    bitfile_version <= 16'h12;

    Reset_TTC_out <= 1'b0;
    Reset_TX_out <= 1'b0;
    Reset_160_out <= 1'b0;
    Scaler_reset_out <= 1'b0;
    GTX_TX_reset_out <= 1'b0;
    GTX_RX_reset_out <= 1'b0;
    Delay_reset_out <= 1'b0;
    CLK_reset_out <= 1'b0;
    FIFO_reset_out <= 1'b0;
    TX_Logic_reset_out <= 1'b0;
    Monitoring_reset_out <= 1'b0;
    
    gt0_rx_reset_out <= 1'b0;
    gt1_rx_reset_out <= 1'b0;
    gt2_rx_reset_out <= 1'b0;
    gt3_rx_reset_out <= 1'b0;
    gt4_rx_reset_out <= 1'b0;
    gt5_rx_reset_out <= 1'b0;
    gt6_rx_reset_out <= 1'b0;
    gt7_rx_reset_out <= 1'b0;
    gt8_rx_reset_out <= 1'b0;
    gt9_rx_reset_out <= 1'b0;
    gt10_rx_reset_out <= 1'b0;
    gt11_rx_reset_out <= 1'b0;
    
    Delay_gtx0 <= 7'h4;
    Delay_gtx1 <= 7'h4;
    Delay_gtx2 <= 7'h4;
    Delay_gtx3 <= 7'h4;
    Delay_gtx4 <= 7'h4;
    Delay_gtx5 <= 7'h4;
    Delay_gtx6 <= 7'h4;
    Delay_gtx7 <= 7'h4;
    Delay_gtx8 <= 7'h4;
    Delay_gtx9 <= 7'h4;
    Delay_gtx10 <= 7'h4;
    Delay_gtx11 <= 7'h4;
    
    Delay_glink0 <= 8'h4;
    Delay_glink1 <= 8'h4;
    Delay_glink2 <= 8'h4;
    Delay_glink3 <= 8'h4;
    Delay_glink4 <= 8'h4;
    Delay_glink5 <= 8'h4;
    Delay_glink6 <= 8'h4;
    Delay_glink7 <= 8'h4;
    Delay_glink8 <= 8'h4;
    Delay_glink9 <= 8'h4;
    Delay_glink10 <= 8'h4;
    Delay_glink11 <= 8'h4;
    Delay_glink12 <= 8'h4;
    Delay_glink13 <= 8'h4;

    Delay_L1A <= 8'h2;
    Delay_BCR <= 8'h2;
    Delay_trig_BCR <= 8'h2;
    Delay_ECR <= 8'h2;
    Delay_TTC_RESET <= 8'h2;
    Delay_TEST_PULSE <= 8'h2;
    
    Mask_gtx0 <= 2'b0;
    Mask_gtx1 <= 2'b0;
    Mask_gtx2 <= 2'b0;
    Mask_gtx3 <= 2'b0;
    Mask_gtx4 <= 2'b0;
    Mask_gtx5 <= 2'b0;
    Mask_gtx6 <= 2'b0;
    Mask_gtx7 <= 2'b0;
    Mask_gtx8 <= 2'b0;
    Mask_gtx9 <= 2'b0;
    Mask_gtx10 <= 2'b0;
    Mask_gtx11 <= 2'b0;
    
    Mask_L1A <= 1'b0;
    Mask_BCR <= 1'b0;
    Mask_trig_BCR <= 1'b0;
    Mask_ECR <= 1'b0;
    Mask_TTC_RESET <= 1'b0;
    Mask_TEST_PULSE <= 1'b0;
    
    Mask_glink0 <= 2'b0;
    Mask_glink1 <= 2'b0;
    Mask_glink2 <= 2'b0;
    Mask_glink3 <= 2'b0;
    Mask_glink4 <= 2'b0;
    Mask_glink5 <= 2'b0;
    Mask_glink6 <= 2'b0;
    Mask_glink7 <= 2'b0;
    Mask_glink8 <= 2'b0;
    Mask_glink9 <= 2'b0;
    Mask_glink10 <= 2'b0;
    Mask_glink11 <= 2'b0;
    Mask_glink12 <= 2'b0;
    Mask_glink13 <= 2'b0;

    Test_Pulse_Length <= 16'h7f;
    Test_Pulse_Wait_Length <= 16'h7f;
    Test_Pulse_Enable <= 1'b0;

    
    SLID <= 12'h777;
    CONVERTERID <= 1'b0;

    monitoring_FIFO_wr_en <= 1'b0;
    monitoring_FIFO_rd_en <= 1'b0;
    lane_selector_out <= 7'h0;
    glink_lane_selector_out <= 4'h0;
    xadc_mon_enable_out <= 1'b0;
    xadc_mon_addr_out <= 7'h0;

    L1A_manual_parameter <= 16'h6ff1;
        
    gtx0_test_data1 <= 16'h0;
    gtx0_test_data2 <= 16'h0;
    gtx0_test_data3 <= 16'h0;
    gtx0_test_data4 <= 16'h0;
    gtx0_test_data5 <= 16'h0;
    gtx0_test_data6 <= 16'h0;
    gtx0_test_data7 <= 16'h0;
    gtx1_test_data1 <= 16'h0;
    gtx1_test_data2 <= 16'h0;
    gtx1_test_data3 <= 16'h0;
    gtx1_test_data4 <= 16'h0;
    gtx1_test_data5 <= 16'h0;
    gtx1_test_data6 <= 16'h0;
    gtx1_test_data7 <= 16'h0;
    gtx2_test_data1 <= 16'h0;
    gtx2_test_data2 <= 16'h0;
    gtx2_test_data3 <= 16'h0;
    gtx2_test_data4 <= 16'h0;
    gtx2_test_data5 <= 16'h0;
    gtx2_test_data6 <= 16'h0;
    gtx2_test_data7 <= 16'h0;
    gtx3_test_data1 <= 16'h0;
    gtx3_test_data2 <= 16'h0;
    gtx3_test_data3 <= 16'h0;
    gtx3_test_data4 <= 16'h0;
    gtx3_test_data5 <= 16'h0;
    gtx3_test_data6 <= 16'h0;
    gtx3_test_data7 <= 16'h0;
    gtx4_test_data1 <= 16'h0;
    gtx4_test_data2 <= 16'h0;
    gtx4_test_data3 <= 16'h0;
    gtx4_test_data4 <= 16'h0;
    gtx4_test_data5 <= 16'h0;
    gtx4_test_data6 <= 16'h0;
    gtx4_test_data7 <= 16'h0;
    gtx5_test_data1 <= 16'h0;
    gtx5_test_data2 <= 16'h0;
    gtx5_test_data3 <= 16'h0;
    gtx5_test_data4 <= 16'h0;
    gtx5_test_data5 <= 16'h0;
    gtx5_test_data6 <= 16'h0;
    gtx5_test_data7 <= 16'h0;
    gtx6_test_data1 <= 16'h0;
    gtx6_test_data2 <= 16'h0;
    gtx6_test_data3 <= 16'h0;
    gtx6_test_data4 <= 16'h0;
    gtx6_test_data5 <= 16'h0;
    gtx6_test_data6 <= 16'h0;
    gtx6_test_data7 <= 16'h0;
    gtx7_test_data1 <= 16'h0;
    gtx7_test_data2 <= 16'h0;
    gtx7_test_data3 <= 16'h0;
    gtx7_test_data4 <= 16'h0;
    gtx7_test_data5 <= 16'h0;
    gtx7_test_data6 <= 16'h0;
    gtx7_test_data7 <= 16'h0;
    gtx8_test_data1 <= 16'h0;
    gtx8_test_data2 <= 16'h0;
    gtx8_test_data3 <= 16'h0;
    gtx8_test_data4 <= 16'h0;
    gtx8_test_data5 <= 16'h0;
    gtx8_test_data6 <= 16'h0;
    gtx8_test_data7 <= 16'h0;
    gtx9_test_data1 <= 16'h0;
    gtx9_test_data2 <= 16'h0;
    gtx9_test_data3 <= 16'h0;
    gtx9_test_data4 <= 16'h0;
    gtx9_test_data5 <= 16'h0;
    gtx9_test_data6 <= 16'h0;
    gtx9_test_data7 <= 16'h0;
    gtx10_test_data1 <= 16'h0;
    gtx10_test_data2 <= 16'h0;
    gtx10_test_data3 <= 16'h0;
    gtx10_test_data4 <= 16'h0;
    gtx10_test_data5 <= 16'h0;
    gtx10_test_data6 <= 16'h0;
    gtx10_test_data7 <= 16'h0;
    gtx11_test_data1 <= 16'h0;
    gtx11_test_data2 <= 16'h0;
    gtx11_test_data3 <= 16'h0;
    gtx11_test_data4 <= 16'h0;
    gtx11_test_data5 <= 16'h0;
    gtx11_test_data6 <= 16'h0;
    gtx11_test_data7 <= 16'h0;

    gtx0_phase_select <= 4'h0;
    gtx1_phase_select <= 4'h0;
    gtx2_phase_select <= 4'h0;
    gtx3_phase_select <= 4'h0;
    gtx4_phase_select <= 4'h0;
    gtx5_phase_select <= 4'h0;
    gtx6_phase_select <= 4'h0;
    gtx7_phase_select <= 4'h0;
    gtx8_phase_select <= 4'h0;
    gtx9_phase_select <= 4'h0;
    gtx10_phase_select <= 4'h0;
    gtx11_phase_select <= 4'h0;

    glink0_test_data1 <= 16'h0;
    glink0_test_data2 <= 4'h0;
    glink1_test_data1 <= 16'h0;
    glink1_test_data2 <= 4'h0;
    glink2_test_data1 <= 16'h0;
    glink2_test_data2 <= 4'h0;
    glink3_test_data1 <= 16'h0;
    glink3_test_data2 <= 4'h0;
    glink4_test_data1 <= 16'h0;
    glink4_test_data2 <= 4'h0;
    glink5_test_data1 <= 16'h0;
    glink5_test_data2 <= 4'h0;
    glink6_test_data1 <= 16'h0;
    glink6_test_data2 <= 4'h0;
    glink7_test_data1 <= 16'h0;
    glink7_test_data2 <= 4'h0;
    glink8_test_data1 <= 16'h0;
    glink8_test_data2 <= 4'h0;
    glink9_test_data1 <= 16'h0;
    glink9_test_data2 <= 4'h0;
    glink10_test_data1 <= 16'h0;
    glink10_test_data2 <= 4'h0;
    glink11_test_data1 <= 16'h0;
    glink11_test_data2 <= 4'h0;
    glink12_test_data1 <= 16'h0;
    glink12_test_data2 <= 4'h0;
    glink13_test_data1 <= 16'h0;
    glink13_test_data2 <= 4'h0;

    Phase_glink0 <= 4'h0;
    Phase_glink1 <= 4'h0;
    Phase_glink2 <= 4'h0;
    Phase_glink3 <= 4'h0;
    Phase_glink4 <= 4'h0;
    Phase_glink5 <= 4'h0;
    Phase_glink6 <= 4'h0;
    Phase_glink7 <= 4'h0;
    Phase_glink8 <= 4'h0;
    Phase_glink9 <= 4'h0;
    Phase_glink10 <= 4'h0;
    Phase_glink11 <= 4'h0;
    Phase_glink12 <= 4'h0;
    Phase_glink13 <= 4'h0;
    
    TrackOutputGenerator_Enb <= 1'b0;
    Trackgenerator0_S0_ROI <= 8'b0;
    Trackgenerator1_S0_ROI <= 8'b0;
    Trackgenerator2_S0_ROI <= 8'b0;
    Trackgenerator3_S0_ROI <= 8'b0;
    Trackgenerator0_S0_pT <= 4'b0;
    Trackgenerator1_S0_pT <= 4'b0;
    Trackgenerator2_S0_pT <= 4'b0;
    Trackgenerator3_S0_pT <= 4'b0;    
    Trackgenerator0_S0_sign <= 1'b0;
    Trackgenerator1_S0_sign <= 1'b0;
    Trackgenerator2_S0_sign <= 1'b0;
    Trackgenerator3_S0_sign <= 1'b0;    
    Trackgenerator0_S0_InnerCoincidenceFlag <= 3'b0;
    Trackgenerator1_S0_InnerCoincidenceFlag <= 3'b0;
    Trackgenerator2_S0_InnerCoincidenceFlag <= 3'b0;
    Trackgenerator3_S0_InnerCoincidenceFlag <= 3'b0;        
    Flag_trackgenerator_S0 <= 4'b0;
    Trackgenerator0_S1_ROI <= 8'b0;
    Trackgenerator1_S1_ROI <= 8'b0;
    Trackgenerator2_S1_ROI <= 8'b0;
    Trackgenerator3_S1_ROI <= 8'b0;
    Trackgenerator0_S1_pT <= 4'b0;
    Trackgenerator1_S1_pT <= 4'b0;
    Trackgenerator2_S1_pT <= 4'b0;
    Trackgenerator3_S1_pT <= 4'b0;    
    Trackgenerator0_S1_sign <= 1'b0;
    Trackgenerator1_S1_sign <= 1'b0;
    Trackgenerator2_S1_sign <= 1'b0;
    Trackgenerator3_S1_sign <= 1'b0;    
    Trackgenerator0_S1_InnerCoincidenceFlag <= 3'b0;
    Trackgenerator1_S1_InnerCoincidenceFlag <= 3'b0;
    Trackgenerator2_S1_InnerCoincidenceFlag <= 3'b0;
    Trackgenerator3_S1_InnerCoincidenceFlag <= 3'b0;        
    Flag_trackgenerator_S1 <= 4'b0; 
    
    Reset_GLinkMonitor <= 1'b1;
    GL0_State_Reset <= 1'b1;
    GL1_State_Reset <= 1'b1;
    GL2_State_Reset <= 1'b1;
    GL3_State_Reset <= 1'b1;
    GL4_State_Reset <= 1'b1;
    GL5_State_Reset <= 1'b1;
    GL6_State_Reset <= 1'b1;
    GL7_State_Reset <= 1'b1;
    GL8_State_Reset <= 1'b1;
    GL9_State_Reset <= 1'b1;
    GL10_State_Reset <= 1'b1;
    GL11_State_Reset <= 1'b1;
    GL12_State_Reset <= 1'b1;
    GL13_State_Reset <= 1'b1;
    
    GL0_ErrorCounter_Reset <= 1'b1;
    GL1_ErrorCounter_Reset <= 1'b1;
    GL2_ErrorCounter_Reset <= 1'b1;
    GL3_ErrorCounter_Reset <= 1'b1;
    GL4_ErrorCounter_Reset <= 1'b1;
    GL5_ErrorCounter_Reset <= 1'b1;
    GL6_ErrorCounter_Reset <= 1'b1;
    GL7_ErrorCounter_Reset <= 1'b1;
    GL8_ErrorCounter_Reset <= 1'b1;
    GL9_ErrorCounter_Reset <= 1'b1;
    GL10_ErrorCounter_Reset <= 1'b1;
    GL11_ErrorCounter_Reset <= 1'b1;
    GL12_ErrorCounter_Reset <= 1'b1;
    GL13_ErrorCounter_Reset <= 1'b1;
    
    RX_DIVen <= 1'b0;
    GL0_RX_DIV_vme <= 2'b0;
    GL1_RX_DIV_vme <= 2'b0;
    GL2_RX_DIV_vme <= 2'b0;
    GL3_RX_DIV_vme <= 2'b0;
    GL4_RX_DIV_vme <= 2'b0;
    GL5_RX_DIV_vme <= 2'b0;
    GL6_RX_DIV_vme <= 2'b0;
    GL7_RX_DIV_vme <= 2'b0;
    GL8_RX_DIV_vme <= 2'b0;
    GL9_RX_DIV_vme <= 2'b0;
    GL10_RX_DIV_vme <= 2'b0;
    GL11_RX_DIV_vme <= 2'b0;
    GL12_RX_DIV_vme <= 2'b0;
    GL13_RX_DIV_vme <= 2'b0;   
        
end

reg [2:0] shift_reg_40;     
reg [2:0] shift_reg_160;    
reg [2:0] shift_reg_TX;    
reg write_40; 
reg write_160; 
reg write_TX; 
reg LUT_init_wr_en_reg = 0;
        



// WRITE asynchronus
always@(posedge ws) begin
    case (VME_A)        
// Reset
        default : begin
        end    
    endcase
end


// Write 40 MHz
always@(posedge CLK_40) begin
    shift_reg_40[2:0] <= {shift_reg_40[1:0], ws};
    if(write_40) begin
        write_40 <= 1'b0;
    end
    else if (shift_reg_40[2]==1'b0 && shift_reg_40[0] == 1'b1) begin
        write_40 <= 1'b1;
        case (VME_A)
        `Addr_TEST : begin
            Test_reg <= VME_DIN[15:0];
        end
        `Addr_RESET : begin
            Reset_TTC_out <= VME_DIN[0];
        end
        `Addr_GTX_TX_RESET : begin
            GTX_TX_reset_out <= VME_DIN[0];
        end
        `Addr_CLK_RESET : begin
            CLK_reset_out <= VME_DIN[0];
        end
        `Addr_SCALER_RESET : begin
            Scaler_reset_out <= VME_DIN[0];
        end
        `Addr_GTX_RX_RESET : begin
            GTX_RX_reset_out <= VME_DIN[0];
        end
        `Addr_DELAY_RESET : begin
            Delay_reset_out <= VME_DIN[0];
        end
        `Addr_MONITORING_RESET : begin
            Monitoring_reset_out <= VME_DIN[0];
        end
        `Addr_GT0_RX_RESET : begin
            gt0_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT1_RX_RESET : begin
            gt1_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT2_RX_RESET : begin
            gt2_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT3_RX_RESET : begin
            gt3_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT4_RX_RESET : begin
            gt4_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT5_RX_RESET : begin
            gt5_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT6_RX_RESET : begin
            gt6_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT7_RX_RESET : begin
            gt7_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT8_RX_RESET : begin
            gt8_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT9_RX_RESET : begin
            gt9_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT10_RX_RESET : begin
            gt10_rx_reset_out <= VME_DIN[0];
        end
        `Addr_GT11_RX_RESET : begin
            gt11_rx_reset_out <= VME_DIN[0];
        end

// Delay
        `Addr_DELAY_GTX0 : begin
            Delay_gtx0[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX1 : begin
            Delay_gtx1[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX2 : begin
            Delay_gtx2[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX3 : begin
            Delay_gtx3[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX4 : begin
            Delay_gtx4[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX5 : begin
            Delay_gtx5[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX6 : begin
            Delay_gtx6[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX7 : begin
            Delay_gtx7[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX8 : begin
            Delay_gtx8[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX9 : begin
            Delay_gtx9[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX10 : begin
            Delay_gtx10[6:0] <= VME_DIN[6:0];
        end
        `Addr_DELAY_GTX11 : begin
            Delay_gtx11[6:0] <= VME_DIN[6:0];
        end

        `Addr_DELAY_GLINK0 : begin
            Delay_glink0[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK1 : begin
            Delay_glink1[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK2 : begin
            Delay_glink2[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK3 : begin
            Delay_glink3[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK4 : begin
            Delay_glink4[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK5 : begin
            Delay_glink5[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK6 : begin
            Delay_glink6[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK7 : begin
            Delay_glink7[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK8 : begin
            Delay_glink8[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK9 : begin
            Delay_glink9[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK10 : begin
            Delay_glink10[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK11 : begin
            Delay_glink11[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK12 : begin
            Delay_glink12[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_GLINK13 : begin
            Delay_glink13[7:0] <= VME_DIN[7:0];
        end
        
        `Addr_DELAY_L1A : begin
            Delay_L1A[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_BCR : begin
            Delay_BCR[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_TRIG_BCR : begin
            Delay_trig_BCR[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_ECR : begin
            Delay_ECR[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_TTC_RESET : begin
            Delay_TTC_RESET[7:0] <= VME_DIN[7:0];
        end
        `Addr_DELAY_TEST_PULSE : begin
            Delay_TEST_PULSE[7:0] <= VME_DIN[7:0];
        end



// Mask    
        `Addr_MASK_RX0 : begin
            Mask_gtx0[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX1 : begin
            Mask_gtx1[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX2 : begin
            Mask_gtx2[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX3 : begin
            Mask_gtx3[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX4 : begin
            Mask_gtx4[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX5 : begin
            Mask_gtx5[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX6 : begin
            Mask_gtx6[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX7 : begin
            Mask_gtx7[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX8 : begin
            Mask_gtx8[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX9 : begin
            Mask_gtx9[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX10 : begin
            Mask_gtx10[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_RX11 : begin
            Mask_gtx11[1:0] <= VME_DIN[1:0];
        end

        `Addr_MASK_L1A : begin
            Mask_L1A <= VME_DIN[0];
        end
        `Addr_MASK_BCR : begin
            Mask_BCR <= VME_DIN[0];
        end
        `Addr_MASK_TRIG_BCR : begin
            Mask_trig_BCR <= VME_DIN[0];
        end
        `Addr_MASK_ECR : begin
            Mask_ECR <= VME_DIN[0];
        end
        `Addr_MASK_TTC_RESET : begin
            Mask_TTC_RESET <= VME_DIN[0];
        end
        `Addr_MASK_TEST_PULSE : begin
            Mask_TEST_PULSE <= VME_DIN[0];
        end


        `Addr_MASK_GLINK0 : begin
            Mask_glink0[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK1 : begin
            Mask_glink1[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK2 : begin
            Mask_glink2[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK3 : begin
            Mask_glink3[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK4 : begin
            Mask_glink4[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK5 : begin
            Mask_glink5[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK6 : begin
            Mask_glink6[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK7 : begin
            Mask_glink7[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK8 : begin
            Mask_glink8[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK9 : begin
            Mask_glink9[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK10 : begin
            Mask_glink10[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK11 : begin
            Mask_glink11[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK12 : begin
            Mask_glink12[1:0] <= VME_DIN[1:0];
        end
        `Addr_MASK_GLINK13 : begin
            Mask_glink13[1:0] <= VME_DIN[1:0];
        end


        `Addr_TEST_PULSE_LENGTH: begin
               Test_Pulse_Length[15:0] <= VME_DIN[15:0];
        end
        `Addr_TEST_PULSE_WAIT_LENGTH: begin
               Test_Pulse_Wait_Length[15:0] <= VME_DIN[15:0];
        end
        `Addr_TEST_PULSE_ENABLE: begin
               Test_Pulse_Enable <= VME_DIN[0];
        end
                
        `Addr_SLID : begin
            SLID[11:0] <= VME_DIN[11:0]; 
        end
        
        `Addr_CONVERTERID : begin
            CONVERTERID <= VME_DIN[0];     
        end

        `Addr_LANE_SELECTOR : begin
            lane_selector_out[6:0] <= VME_DIN[6:0];
        end
        `Addr_GLINK_LANE_SELECTOR : begin
            glink_lane_selector_out[3:0] <= VME_DIN[3:0];
        end
        `Addr_XADC_MON_ENABLE : begin
            xadc_mon_enable_out <= VME_DIN[0];
        end     
        `Addr_XADC_MON_ADDR : begin
            xadc_mon_addr_out <= VME_DIN[6:0];
        end                  
        `Addr_L1A_MANUAL_PARAMETER : begin
            L1A_manual_parameter[15:0] <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA1 : begin
            gtx0_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA2 : begin
            gtx0_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA3 : begin
            gtx0_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA4 : begin
            gtx0_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA5 : begin
            gtx0_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA6 : begin
            gtx0_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX0_TEST_DATA7 : begin
            gtx0_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA1 : begin
            gtx1_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA2 : begin
            gtx1_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA3 : begin
            gtx1_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA4 : begin
            gtx1_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA5 : begin
            gtx1_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA6 : begin
            gtx1_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX1_TEST_DATA7 : begin
            gtx1_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA1 : begin
            gtx2_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA2 : begin
            gtx2_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA3 : begin
            gtx2_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA4 : begin
            gtx2_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA5 : begin
            gtx2_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA6 : begin
            gtx2_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX2_TEST_DATA7 : begin
            gtx2_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA1 : begin
            gtx3_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA2 : begin
            gtx3_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA3 : begin
            gtx3_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA4 : begin
            gtx3_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA5 : begin
            gtx3_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA6 : begin
            gtx3_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX3_TEST_DATA7 : begin
            gtx3_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA1 : begin
            gtx4_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA2 : begin
            gtx4_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA3 : begin
            gtx4_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA4 : begin
            gtx4_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA5 : begin
            gtx4_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA6 : begin
            gtx4_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX4_TEST_DATA7 : begin
            gtx4_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA1 : begin
            gtx5_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA2 : begin
            gtx5_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA3 : begin
            gtx5_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA4 : begin
            gtx5_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA5 : begin
            gtx5_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA6 : begin
            gtx5_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX5_TEST_DATA7 : begin
            gtx5_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA1 : begin
            gtx6_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA2 : begin
            gtx6_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA3 : begin
            gtx6_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA4 : begin
            gtx6_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA5 : begin
            gtx6_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA6 : begin
            gtx6_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX6_TEST_DATA7 : begin
            gtx6_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA1 : begin
            gtx7_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA2 : begin
            gtx7_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA3 : begin
            gtx7_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA4 : begin
            gtx7_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA5 : begin
            gtx7_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA6 : begin
            gtx7_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX7_TEST_DATA7 : begin
            gtx7_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA1 : begin
            gtx8_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA2 : begin
            gtx8_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA3 : begin
            gtx8_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA4 : begin
            gtx8_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA5 : begin
            gtx8_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA6 : begin
            gtx8_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX8_TEST_DATA7 : begin
            gtx8_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA1 : begin
            gtx9_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA2 : begin
            gtx9_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA3 : begin
            gtx9_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA4 : begin
            gtx9_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA5 : begin
            gtx9_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA6 : begin
            gtx9_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX9_TEST_DATA7 : begin
            gtx9_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA1 : begin
            gtx10_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA2 : begin
            gtx10_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA3 : begin
            gtx10_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA4 : begin
            gtx10_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA5 : begin
            gtx10_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA6 : begin
            gtx10_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX10_TEST_DATA7 : begin
            gtx10_test_data7 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA1 : begin
            gtx11_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA2 : begin
            gtx11_test_data2 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA3 : begin
            gtx11_test_data3 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA4 : begin
            gtx11_test_data4 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA5 : begin
            gtx11_test_data5 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA6 : begin
            gtx11_test_data6 <= VME_DIN[15:0];
        end
        `Addr_GTX11_TEST_DATA7 : begin
            gtx11_test_data7 <= VME_DIN[15:0];
        end
	`Addr_GTX0_PHASE_SELECT : begin
	    gtx0_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX1_PHASE_SELECT : begin
	    gtx1_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX2_PHASE_SELECT : begin
	    gtx2_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX3_PHASE_SELECT : begin
	    gtx3_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX4_PHASE_SELECT : begin
	    gtx4_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX5_PHASE_SELECT : begin
	    gtx5_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX6_PHASE_SELECT : begin
	    gtx6_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX7_PHASE_SELECT : begin
	    gtx7_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX8_PHASE_SELECT : begin
	    gtx8_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX9_PHASE_SELECT : begin
	    gtx9_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX10_PHASE_SELECT : begin
	    gtx10_phase_select <= VME_DIN[3:0];
	end
	`Addr_GTX11_PHASE_SELECT : begin
	    gtx11_phase_select <= VME_DIN[3:0];
	end
        `Addr_GLINK0_TEST_DATA1 : begin
            glink0_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK0_TEST_DATA2 : begin
            glink0_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK1_TEST_DATA1 : begin
            glink1_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK1_TEST_DATA2 : begin
            glink1_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK2_TEST_DATA1 : begin
            glink2_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK2_TEST_DATA2 : begin
            glink2_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK3_TEST_DATA1 : begin
            glink3_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK3_TEST_DATA2 : begin
            glink3_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK4_TEST_DATA1 : begin
            glink4_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK4_TEST_DATA2 : begin
            glink4_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK5_TEST_DATA1 : begin
            glink5_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK5_TEST_DATA2 : begin
            glink5_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK6_TEST_DATA1 : begin
            glink6_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK6_TEST_DATA2 : begin
            glink6_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK7_TEST_DATA1 : begin
            glink7_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK7_TEST_DATA2 : begin
            glink7_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK8_TEST_DATA1 : begin
            glink8_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK8_TEST_DATA2 : begin
            glink8_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK9_TEST_DATA1 : begin
            glink9_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK9_TEST_DATA2 : begin
            glink9_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK10_TEST_DATA1 : begin
            glink10_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK10_TEST_DATA2 : begin
            glink10_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK11_TEST_DATA1 : begin
            glink11_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK11_TEST_DATA2 : begin
            glink11_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK12_TEST_DATA1 : begin
            glink12_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK12_TEST_DATA2 : begin
            glink12_test_data2 <= VME_DIN[3:0];
        end
        `Addr_GLINK13_TEST_DATA1 : begin
            glink13_test_data1 <= VME_DIN[15:0];
        end
        `Addr_GLINK13_TEST_DATA2 : begin
            glink13_test_data2 <= VME_DIN[3:0];
        end

        `Addr_GLINK0_PHASE_SELECT : begin
            Phase_glink0[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK1_PHASE_SELECT : begin
            Phase_glink1[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK2_PHASE_SELECT : begin
            Phase_glink2[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK3_PHASE_SELECT : begin
            Phase_glink3[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK4_PHASE_SELECT : begin
            Phase_glink4[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK5_PHASE_SELECT : begin
            Phase_glink5[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK6_PHASE_SELECT : begin
            Phase_glink6[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK7_PHASE_SELECT : begin
            Phase_glink7[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK8_PHASE_SELECT : begin
            Phase_glink8[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK9_PHASE_SELECT : begin
            Phase_glink9[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK10_PHASE_SELECT : begin
            Phase_glink10[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK11_PHASE_SELECT : begin
            Phase_glink11[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK12_PHASE_SELECT : begin
            Phase_glink12[3:0] <= VME_DIN[3:0];
        end
        `Addr_GLINK13_PHASE_SELECT : begin
            Phase_glink13[3:0] <= VME_DIN[3:0];
        end

        `Addr_TRACKOUTPUTGENERATOR_ENV : begin
            TrackOutputGenerator_Enb <= VME_DIN[0];       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S0_ROI : begin
            Trackgenerator0_S0_ROI <= VME_DIN[7:0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_ROI : begin
            Trackgenerator1_S0_ROI <= VME_DIN[7:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_ROI : begin
            Trackgenerator2_S0_ROI <= VME_DIN[7:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_ROI : begin
            Trackgenerator3_S0_ROI <= VME_DIN[7:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S0_PT : begin
            Trackgenerator0_S0_pT <= VME_DIN[3:0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_PT : begin
            Trackgenerator1_S0_pT <= VME_DIN[3:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_PT : begin
            Trackgenerator2_S0_pT <= VME_DIN[3:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_PT : begin
            Trackgenerator3_S0_pT <= VME_DIN[3:0];       
        end         
        `Addr_TRACKOUTPUTGENERATOR0_S0_SIGN : begin
            Trackgenerator0_S0_sign <= VME_DIN[0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_SIGN : begin
            Trackgenerator1_S0_sign <= VME_DIN[0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_SIGN : begin
            Trackgenerator2_S0_sign <= VME_DIN[0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_SIGN : begin
            Trackgenerator3_S0_sign <= VME_DIN[0];       
        end  
        `Addr_TRACKOUTPUTGENERATOR0_S0_INNERCOINCIDENCEFLAG : begin
            Trackgenerator0_S0_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_INNERCOINCIDENCEFLAG : begin
            Trackgenerator1_S0_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_INNERCOINCIDENCEFLAG : begin
            Trackgenerator2_S0_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_INNERCOINCIDENCEFLAG : begin
            Trackgenerator3_S0_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end 
        `Addr_FLAG_TRACKGENERATOR_S0  : begin
            Flag_trackgenerator_S0 <= VME_DIN[3:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S1_ROI : begin
            Trackgenerator0_S1_ROI <= VME_DIN[7:0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_ROI : begin
            Trackgenerator1_S1_ROI <= VME_DIN[7:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_ROI : begin
            Trackgenerator2_S1_ROI <= VME_DIN[7:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_ROI : begin
            Trackgenerator3_S1_ROI <= VME_DIN[7:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S1_PT : begin
            Trackgenerator0_S1_pT <= VME_DIN[3:0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_PT : begin
            Trackgenerator1_S1_pT <= VME_DIN[3:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_PT : begin
            Trackgenerator2_S1_pT <= VME_DIN[3:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_PT : begin
            Trackgenerator3_S1_pT <= VME_DIN[3:0];       
        end         
        `Addr_TRACKOUTPUTGENERATOR0_S1_SIGN : begin
            Trackgenerator0_S1_sign <= VME_DIN[0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_SIGN : begin
            Trackgenerator1_S1_sign <= VME_DIN[0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_SIGN : begin
            Trackgenerator2_S1_sign <= VME_DIN[0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_SIGN : begin
            Trackgenerator3_S1_sign <= VME_DIN[0];       
        end  
        `Addr_TRACKOUTPUTGENERATOR0_S1_INNERCOINCIDENCEFLAG : begin
            Trackgenerator0_S1_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_INNERCOINCIDENCEFLAG : begin
            Trackgenerator1_S1_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_INNERCOINCIDENCEFLAG : begin
            Trackgenerator2_S1_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_INNERCOINCIDENCEFLAG : begin
            Trackgenerator3_S1_InnerCoincidenceFlag <= VME_DIN[2:0];       
        end 
        `Addr_FLAG_TRACKGENERATOR_S1  : begin
            Flag_trackgenerator_S1 <= VME_DIN[3:0];       
        end 
        `Addr_RESET_GLINKMONITOR  : begin
            Reset_GLinkMonitor <= VME_DIN[0];       
        end 
        `Addr_GL0_STATE_RESET  : begin
            GL0_State_Reset <= VME_DIN[0];       
        end 
        `Addr_GL1_STATE_RESET  : begin
            GL1_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL2_STATE_RESET  : begin
            GL2_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL3_STATE_RESET  : begin
            GL3_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL4_STATE_RESET  : begin
            GL4_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL5_STATE_RESET  : begin
            GL5_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL6_STATE_RESET  : begin
            GL6_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL7_STATE_RESET  : begin
            GL7_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL8_STATE_RESET  : begin
            GL8_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL9_STATE_RESET  : begin
            GL9_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL10_STATE_RESET  : begin
            GL10_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL11_STATE_RESET  : begin
            GL11_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL12_STATE_RESET  : begin
            GL12_State_Reset <= VME_DIN[0];       
        end
        `Addr_GL13_STATE_RESET  : begin
            GL13_State_Reset <= VME_DIN[0];       
        end                                                                                                               
        `Addr_GL0_ERRORCOUNTER_RESET  : begin
            GL0_ErrorCounter_Reset <= VME_DIN[0];       
        end 
        `Addr_GL1_ERRORCOUNTER_RESET  : begin
            GL1_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL2_ERRORCOUNTER_RESET  : begin
            GL2_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL3_ERRORCOUNTER_RESET  : begin
            GL3_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL4_ERRORCOUNTER_RESET  : begin
            GL4_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL5_ERRORCOUNTER_RESET  : begin
            GL5_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL6_ERRORCOUNTER_RESET  : begin
            GL6_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL7_ERRORCOUNTER_RESET  : begin
            GL7_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL8_ERRORCOUNTER_RESET  : begin
            GL8_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL9_ERRORCOUNTER_RESET  : begin
            GL9_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL10_ERRORCOUNTER_RESET  : begin
            GL10_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL11_ERRORCOUNTER_RESET  : begin
            GL11_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL12_ERRORCOUNTER_RESET  : begin
            GL12_ErrorCounter_Reset <= VME_DIN[0];       
        end
        `Addr_GL13_ERRORCOUNTER_RESET  : begin
            GL13_ErrorCounter_Reset <= VME_DIN[0];       
        end                       
        `Addr_RX_DIVEN  : begin
            RX_DIVen <= VME_DIN[0];       
        end  
        `Addr_GL0_RX_DIV_VME  : begin
            GL0_RX_DIV_vme <= VME_DIN[1:0];       
        end 
        `Addr_GL1_RX_DIV_VME  : begin
            GL1_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL2_RX_DIV_VME  : begin
            GL2_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL3_RX_DIV_VME  : begin
            GL3_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL4_RX_DIV_VME  : begin
            GL4_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL5_RX_DIV_VME  : begin
            GL5_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL6_RX_DIV_VME  : begin
            GL6_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL7_RX_DIV_VME  : begin
            GL7_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL8_RX_DIV_VME  : begin
            GL8_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL9_RX_DIV_VME  : begin
            GL9_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL10_RX_DIV_VME  : begin
            GL10_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL11_RX_DIV_VME  : begin
            GL11_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL12_RX_DIV_VME  : begin
            GL12_RX_DIV_vme <= VME_DIN[1:0];       
        end
        `Addr_GL13_RX_DIV_VME  : begin
            GL13_RX_DIV_vme <= VME_DIN[1:0];       
        end                                                                                                                                                                                                                         
        default : begin
        end
    endcase
    end
end




// WRITE 160 MHz
always@(posedge CLK_160) begin
    shift_reg_160[2:0] <= {shift_reg_160[1:0], ws};
    if(write_160) begin
        write_160 <= 1'b0;
    end
    else if (shift_reg_160[2]==1'b0 && shift_reg_160[0]==1'b1) begin
        write_160 <= 1'b1;
        case (VME_A)
        `Addr_RESET : begin
            Reset_160_out <= VME_DIN[0];
        end
        `Addr_FIFO_RESET : begin
            FIFO_reset_out <= VME_DIN[0];
        end
        `Addr_MONITORING_FIFO_wr_en : begin
            monitoring_FIFO_wr_en <= VME_DIN[0];
        end
        `Addr_MONITORING_FIFO_rd_en : begin
            monitoring_FIFO_rd_en    <= VME_DIN[0];
        end
        endcase
        end
end



// WRITE TXUSRCLK
always@(posedge TXUSRCLK_in) begin
    shift_reg_TX[2:0] <= {shift_reg_TX[1:0], ws};
    if(write_TX) begin
        write_TX <= 1'b0;
    end
    else if (shift_reg_TX[2]==1'b0 && shift_reg_TX[0]==1'b1) begin
        write_TX <= 1'b1;
        case (VME_A)
        `Addr_RESET : begin
            Reset_TX_out <= VME_DIN[0];
        end
        `Addr_TX_LOGIC_RESET : begin
            TX_Logic_reset_out <= VME_DIN[0];
        end
        endcase
        end
end

   
    
    //Read
always@(negedge rs) begin     
    case (VME_A [11:0])

// Test and bifile version
     `Addr_TEST : begin
          VME_DOUT[15:0] <= Test_reg[15:0];
        end
        `Addr_BITFILE_VERSION : begin
          VME_DOUT[15:0] <= bitfile_version[15:0];
        end


// Reset
        `Addr_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Reset_TTC_out};
        end
        `Addr_SCALER_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Scaler_reset_out};
        end
        `Addr_GTX_TX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, GTX_TX_reset_out};
        end
        `Addr_GTX_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, GTX_RX_reset_out};
        end
        `Addr_DELAY_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Delay_reset_out};
        end
        `Addr_CLK_RESET : begin
          VME_DOUT[15:0] <= {15'b0, CLK_reset_out};
        end
        `Addr_FIFO_RESET : begin
          VME_DOUT[15:0] <= {15'b0, FIFO_reset_out};
        end
        `Addr_TX_LOGIC_RESET : begin
          VME_DOUT[15:0] <= {15'b0, TX_Logic_reset_out};
        end
        `Addr_MONITORING_RESET : begin
          VME_DOUT[15:0] <= {15'b0, Monitoring_reset_out};
        end
        `Addr_GT0_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt0_rx_reset_out};
        end
        `Addr_GT1_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt1_rx_reset_out};
        end
        `Addr_GT2_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt2_rx_reset_out};
        end
        `Addr_GT3_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt3_rx_reset_out};
        end
        `Addr_GT4_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt4_rx_reset_out};
        end
        `Addr_GT5_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt5_rx_reset_out};
        end
        `Addr_GT6_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt6_rx_reset_out};
        end
        `Addr_GT7_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt7_rx_reset_out};
        end
        `Addr_GT8_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt8_rx_reset_out};
        end
        `Addr_GT9_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt9_rx_reset_out};
        end
        `Addr_GT10_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt10_rx_reset_out};
        end
        `Addr_GT11_RX_RESET : begin
          VME_DOUT[15:0] <= {15'b0, gt11_rx_reset_out};
        end


// Error Scaler
        `Addr_ERROR_SCALER_GT0 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_0[15:0];
        end
        `Addr_ERROR_SCALER_GT1 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_1[15:0];
        end
        `Addr_ERROR_SCALER_GT2 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_2[15:0];
        end
        `Addr_ERROR_SCALER_GT3 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_3[15:0];
        end
        `Addr_ERROR_SCALER_GT4 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_4[15:0];
        end
        `Addr_ERROR_SCALER_GT5 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_5[15:0];
        end
        `Addr_ERROR_SCALER_GT6 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_6[15:0];
        end
        `Addr_ERROR_SCALER_GT7 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_7[15:0];
        end
        `Addr_ERROR_SCALER_GT8 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_8[15:0];
        end
        `Addr_ERROR_SCALER_GT9 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_9[15:0];
        end
        `Addr_ERROR_SCALER_GT10 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_10[15:0];
        end
        `Addr_ERROR_SCALER_GT11 : begin
            VME_DOUT [15:0] <= Error_Scaler_gt_11[15:0];
        end


// Status
        `Addr_STATUS_GT0 : begin
            VME_DOUT [15:0] <= Status_gt_0[15:0];
        end
        `Addr_STATUS_GT1 : begin
            VME_DOUT [15:0] <= Status_gt_1[15:0];
        end
        `Addr_STATUS_GT2 : begin
            VME_DOUT [15:0] <= Status_gt_2[15:0];
        end
        `Addr_STATUS_GT3 : begin
            VME_DOUT [15:0] <= Status_gt_3[15:0];
        end
        `Addr_STATUS_GT4 : begin
            VME_DOUT [15:0] <= Status_gt_4[15:0];
        end
        `Addr_STATUS_GT5 : begin
            VME_DOUT [15:0] <= Status_gt_5[15:0];
        end
        `Addr_STATUS_GT6 : begin
            VME_DOUT [15:0] <= Status_gt_6[15:0];
        end
        `Addr_STATUS_GT7 : begin
            VME_DOUT [15:0] <= Status_gt_7[15:0];
        end
        `Addr_STATUS_GT8 : begin
            VME_DOUT [15:0] <= Status_gt_8[15:0];
        end
        `Addr_STATUS_GT9 : begin
            VME_DOUT [15:0] <= Status_gt_9[15:0];
        end
        `Addr_STATUS_GT10 : begin
            VME_DOUT [15:0] <= Status_gt_10[15:0];
        end
        `Addr_STATUS_GT11 : begin
            VME_DOUT [15:0] <= Status_gt_11[15:0];
        end
      
// Delay      
        `Addr_DELAY_GTX0 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx0[6:0]};
        end
        `Addr_DELAY_GTX1 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx1[6:0]};
        end
        `Addr_DELAY_GTX2 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx2[6:0]};
        end
        `Addr_DELAY_GTX3 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx3[6:0]};
        end
        `Addr_DELAY_GTX4 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx4[6:0]};
        end
        `Addr_DELAY_GTX5 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx5[6:0]};
        end
        `Addr_DELAY_GTX6 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx6[6:0]};
        end
        `Addr_DELAY_GTX7 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx7[6:0]};
        end
        `Addr_DELAY_GTX8 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx8[6:0]};
        end
        `Addr_DELAY_GTX9 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx9[6:0]};
        end
        `Addr_DELAY_GTX10 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx10[6:0]};
        end
        `Addr_DELAY_GTX11 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_gtx11[6:0]};
        end

        `Addr_DELAY_GLINK0 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink0[7:0]};
        end
        `Addr_DELAY_GLINK1 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink1[7:0]};
        end
        `Addr_DELAY_GLINK2 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink2[7:0]};
        end
        `Addr_DELAY_GLINK3 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink3[7:0]};
        end
        `Addr_DELAY_GLINK4 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink4[7:0]};
        end
        `Addr_DELAY_GLINK5 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink5[7:0]};
        end
        `Addr_DELAY_GLINK6 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink6[7:0]};
        end
        `Addr_DELAY_GLINK7 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink7[7:0]};
        end
        `Addr_DELAY_GLINK8 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink8[7:0]};
        end
        `Addr_DELAY_GLINK9 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink9[7:0]};
        end
        `Addr_DELAY_GLINK10 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink10[7:0]};
        end
        `Addr_DELAY_GLINK11 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink11[7:0]};
        end
        `Addr_DELAY_GLINK12 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink12[7:0]};
        end
        `Addr_DELAY_GLINK13 : begin
            VME_DOUT [15:0] <= {8'b0, Delay_glink13[7:0]};
        end
      
        `Addr_DELAY_L1A : begin
             VME_DOUT [15:0] <= {8'b0, Delay_L1A[7:0]};
        end
        `Addr_DELAY_BCR : begin
             VME_DOUT [15:0] <= {8'b0, Delay_BCR[7:0]};
        end
        `Addr_DELAY_TRIG_BCR : begin
             VME_DOUT [15:0] <= {8'b0, Delay_trig_BCR[7:0]};
        end
        `Addr_DELAY_ECR : begin
             VME_DOUT [15:0] <= {8'b0, Delay_ECR[7:0]};
        end
        `Addr_DELAY_TTC_RESET : begin
             VME_DOUT [15:0] <= {8'b0, Delay_TTC_RESET[7:0]};
        end
        `Addr_DELAY_TEST_PULSE : begin
             VME_DOUT [15:0] <= {8'b0, Delay_TEST_PULSE[7:0]};
        end




// Mask

        `Addr_MASK_RX0 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx0[1:0]};
        end
        `Addr_MASK_RX1 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx1[1:0]};
        end
        `Addr_MASK_RX2 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx2[1:0]};
        end
        `Addr_MASK_RX3 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx3[1:0]};
        end
        `Addr_MASK_RX4 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx4[1:0]};
        end
        `Addr_MASK_RX5 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx5[1:0]};
        end
        `Addr_MASK_RX6 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx6[1:0]};
        end
        `Addr_MASK_RX7 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx7[1:0]};
        end
        `Addr_MASK_RX8 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx8[1:0]};
        end
        `Addr_MASK_RX9 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx9[1:0]};
        end
        `Addr_MASK_RX10 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx10[1:0]};
        end
        `Addr_MASK_RX11 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_gtx11[1:0]};
        end

        `Addr_MASK_L1A : begin
             VME_DOUT [15:0] <= {15'b0, Mask_L1A};
        end
        `Addr_MASK_BCR : begin
             VME_DOUT [15:0] <= {15'b0, Mask_BCR};
        end
        `Addr_MASK_TRIG_BCR : begin
             VME_DOUT [15:0] <= {15'b0, Mask_trig_BCR};
        end
        `Addr_MASK_ECR : begin
             VME_DOUT [15:0] <= {15'b0, Mask_ECR};
        end
        `Addr_MASK_TTC_RESET : begin
             VME_DOUT [15:0] <= {15'b0, Mask_TTC_RESET};
        end
        `Addr_MASK_TEST_PULSE : begin
             VME_DOUT [15:0] <= {15'b0, Mask_TEST_PULSE};
        end


        `Addr_MASK_GLINK0 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink0[1:0]};
        end
        `Addr_MASK_GLINK1 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink1[1:0]};
        end
        `Addr_MASK_GLINK2 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink2[1:0]};
        end
        `Addr_MASK_GLINK3 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink3[1:0]};
        end
        `Addr_MASK_GLINK4 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink4[1:0]};
        end
        `Addr_MASK_GLINK5 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink5[1:0]};
        end
        `Addr_MASK_GLINK6 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink6[1:0]};
        end
        `Addr_MASK_GLINK7 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink7[1:0]};
        end
        `Addr_MASK_GLINK8 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink8[1:0]};
        end
        `Addr_MASK_GLINK9 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink9[1:0]};
        end
        `Addr_MASK_GLINK10 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink10[1:0]};
        end
        `Addr_MASK_GLINK11 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink11[1:0]};
        end
        `Addr_MASK_GLINK12 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink12[1:0]};
        end
        `Addr_MASK_GLINK13 : begin
            VME_DOUT [15:0] <= {14'b0, Mask_glink13[1:0]};
        end

        `Addr_TEST_PULSE_LENGTH: begin
             VME_DOUT [15:0] <=  Test_Pulse_Length[15:0];
        end
        `Addr_TEST_PULSE_WAIT_LENGTH: begin
             VME_DOUT [15:0] <=  Test_Pulse_Wait_Length[15:0];
        end
        `Addr_TEST_PULSE_ENABLE: begin
             VME_DOUT [15:0] <=  {15'b0, Test_Pulse_Enable};
        end
        
// IDs
        `Addr_L1A_COUNTER0 : begin
            VME_DOUT [15:0] <= L1A_Counter0[15:0]; 
        end
        `Addr_L1A_COUNTER1 : begin
            VME_DOUT [15:0] <= L1A_Counter1[15:0]; 
        end
        `Addr_L1ID : begin
            VME_DOUT [15:0] <= {4'b0, L1ID[11:0]}; 
        end
        `Addr_BCID : begin
            VME_DOUT [15:0] <= {4'b0, BCID[11:0]}; 
        end
        `Addr_SLID : begin
            VME_DOUT [15:0] <= {4'b0, SLID[11:0]}; 
        end
        `Addr_CONVERTERID : begin
            VME_DOUT [15:0] <= {15'b0, CONVERTERID};
        end
    
        `Addr_MONITORING_FIFO_OUT_0 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_0[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_1 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_1[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_2 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_2[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_3 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_3[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_4 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_4[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_5 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_5[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_6 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_6[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_7 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_7[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_8 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_8[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_9 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_9[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_10 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_10[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_11 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_11[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_12 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_12[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_13 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_13[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_14 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_14[15:0];       
        end
        `Addr_MONITORING_FIFO_OUT_15 : begin
            VME_DOUT [15:0] <= monitoring_FIFO_out_15[15:0];       
        end
        `Addr_MONITORING_FIFO_wr_en : begin
            VME_DOUT [15:0] <= {15'b0, monitoring_FIFO_wr_en};      
        end
        `Addr_MONITORING_FIFO_rd_en : begin
          VME_DOUT [15:0]<= {15'b0, monitoring_FIFO_rd_en};
        end
        `Addr_LANE_SELECTOR : begin
            VME_DOUT [15:0] <= {9'h0, lane_selector_out[6:0]}; 
        end
        `Addr_GLINK_LANE_SELECTOR : begin
            VME_DOUT [15:0] <= {11'h0, glink_lane_selector_out[3:0]}; 
        end
        `Addr_XADC_MON_ENABLE : begin
            VME_DOUT [15:0] <= {15'h0, xadc_mon_enable_out}; 
        end
        `Addr_XADC_MON_ADDR  : begin
            VME_DOUT [15:0] <= {9'h0, xadc_mon_addr_out[6:0]}; 
        end              
        `Addr_XADC_MONITOR_OUT : begin
            VME_DOUT [15:0] <= xadc_monitor_in[15:0]; 
        end        
        `Addr_L1A_MANUAL_PARAMETER : begin
            VME_DOUT [15:0] <= L1A_manual_parameter[15:0]; 
        end

        `Addr_GTX0_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx0_test_data1[15:0]; 
        end
        `Addr_GTX0_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx0_test_data2[15:0]; 
        end
        `Addr_GTX0_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx0_test_data3[15:0]; 
        end
        `Addr_GTX0_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx0_test_data4[15:0]; 
        end
        `Addr_GTX0_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx0_test_data5[15:0]; 
        end
        `Addr_GTX0_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx0_test_data6[15:0]; 
        end
        `Addr_GTX0_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx0_test_data7[15:0]; 
        end
        `Addr_GTX1_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx1_test_data1[15:0]; 
        end
        `Addr_GTX1_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx1_test_data2[15:0]; 
        end
        `Addr_GTX1_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx1_test_data3[15:0]; 
        end
        `Addr_GTX1_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx1_test_data4[15:0]; 
        end
        `Addr_GTX1_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx1_test_data5[15:0]; 
        end
        `Addr_GTX1_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx1_test_data6[15:0]; 
        end
        `Addr_GTX1_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx1_test_data7[15:0]; 
        end
        `Addr_GTX2_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx2_test_data1[15:0]; 
        end
        `Addr_GTX2_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx2_test_data2[15:0]; 
        end
        `Addr_GTX2_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx2_test_data3[15:0]; 
        end
        `Addr_GTX2_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx2_test_data4[15:0]; 
        end
        `Addr_GTX2_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx2_test_data5[15:0]; 
        end
        `Addr_GTX2_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx2_test_data6[15:0]; 
        end
        `Addr_GTX2_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx2_test_data7[15:0]; 
        end
        `Addr_GTX3_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx3_test_data1[15:0]; 
        end
        `Addr_GTX3_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx3_test_data2[15:0]; 
        end
        `Addr_GTX3_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx3_test_data3[15:0]; 
        end
        `Addr_GTX3_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx3_test_data4[15:0]; 
        end
        `Addr_GTX3_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx3_test_data5[15:0]; 
        end
        `Addr_GTX3_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx3_test_data6[15:0]; 
        end
        `Addr_GTX3_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx3_test_data7[15:0]; 
        end
        `Addr_GTX4_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx4_test_data1[15:0]; 
        end
        `Addr_GTX4_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx4_test_data2[15:0]; 
        end
        `Addr_GTX4_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx4_test_data3[15:0]; 
        end
        `Addr_GTX4_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx4_test_data4[15:0]; 
        end
        `Addr_GTX4_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx4_test_data5[15:0]; 
        end
        `Addr_GTX4_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx4_test_data6[15:0]; 
        end
        `Addr_GTX4_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx4_test_data7[15:0]; 
        end
        `Addr_GTX5_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx5_test_data1[15:0]; 
        end
        `Addr_GTX5_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx5_test_data2[15:0]; 
        end
        `Addr_GTX5_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx5_test_data3[15:0]; 
        end
        `Addr_GTX5_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx5_test_data4[15:0]; 
        end
        `Addr_GTX5_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx5_test_data5[15:0]; 
        end
        `Addr_GTX5_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx5_test_data6[15:0]; 
        end
        `Addr_GTX5_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx5_test_data7[15:0]; 
        end
        `Addr_GTX6_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx6_test_data1[15:0]; 
        end
        `Addr_GTX6_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx6_test_data2[15:0]; 
        end
        `Addr_GTX6_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx6_test_data3[15:0]; 
        end
        `Addr_GTX6_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx6_test_data4[15:0]; 
        end
        `Addr_GTX6_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx6_test_data5[15:0]; 
        end
        `Addr_GTX6_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx6_test_data6[15:0]; 
        end
        `Addr_GTX6_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx6_test_data7[15:0]; 
        end
        `Addr_GTX7_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx7_test_data1[15:0]; 
        end
        `Addr_GTX7_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx7_test_data2[15:0]; 
        end
        `Addr_GTX7_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx7_test_data3[15:0]; 
        end
        `Addr_GTX7_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx7_test_data4[15:0]; 
        end
        `Addr_GTX7_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx7_test_data5[15:0]; 
        end
        `Addr_GTX7_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx7_test_data6[15:0]; 
        end
        `Addr_GTX7_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx7_test_data7[15:0]; 
        end
        `Addr_GTX8_TEST_DATA1 : begin
             VME_DOUT [15:0] <= gtx8_test_data1[15:0]; 
       end
        `Addr_GTX8_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx8_test_data2[15:0]; 
        end
        `Addr_GTX8_TEST_DATA3 : begin
             VME_DOUT [15:0] <= gtx8_test_data3[15:0]; 
       end
        `Addr_GTX8_TEST_DATA4 : begin
           VME_DOUT [15:0] <= gtx8_test_data4[15:0]; 
       end
       `Addr_GTX8_TEST_DATA5 : begin
           VME_DOUT [15:0] <= gtx8_test_data5[15:0]; 
       end
       `Addr_GTX8_TEST_DATA6 : begin
           VME_DOUT [15:0] <= gtx8_test_data6[15:0]; 
       end
       `Addr_GTX8_TEST_DATA7 : begin
           VME_DOUT [15:0] <= gtx8_test_data7[15:0]; 
       end
        `Addr_GTX9_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx9_test_data1[15:0]; 
        end
        `Addr_GTX9_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx9_test_data2[15:0]; 
        end
        `Addr_GTX9_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx9_test_data3[15:0]; 
        end
        `Addr_GTX9_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx9_test_data4[15:0]; 
        end
        `Addr_GTX9_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx9_test_data5[15:0]; 
        end
        `Addr_GTX9_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx9_test_data6[15:0]; 
        end
        `Addr_GTX9_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx9_test_data7[15:0]; 
        end
        `Addr_GTX10_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx10_test_data1[15:0]; 
        end
        `Addr_GTX10_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx10_test_data2[15:0]; 
        end
        `Addr_GTX10_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx10_test_data3[15:0]; 
        end
        `Addr_GTX10_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx10_test_data4[15:0]; 
        end
        `Addr_GTX10_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx10_test_data5[15:0]; 
        end
        `Addr_GTX10_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx10_test_data6[15:0]; 
        end
        `Addr_GTX10_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx10_test_data7[15:0]; 
        end
        `Addr_GTX11_TEST_DATA1 : begin
            VME_DOUT [15:0] <= gtx11_test_data1[15:0]; 
        end
        `Addr_GTX11_TEST_DATA2 : begin
            VME_DOUT [15:0] <= gtx11_test_data2[15:0]; 
        end
        `Addr_GTX11_TEST_DATA3 : begin
            VME_DOUT [15:0] <= gtx11_test_data3[15:0]; 
        end
        `Addr_GTX11_TEST_DATA4 : begin
            VME_DOUT [15:0] <= gtx11_test_data4[15:0]; 
        end
        `Addr_GTX11_TEST_DATA5 : begin
            VME_DOUT [15:0] <= gtx11_test_data5[15:0]; 
        end
        `Addr_GTX11_TEST_DATA6 : begin
            VME_DOUT [15:0] <= gtx11_test_data6[15:0]; 
        end
        `Addr_GTX11_TEST_DATA7 : begin
            VME_DOUT [15:0] <= gtx11_test_data7[15:0]; 
        end
	`Addr_GTX0_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx0_phase_select};
	end
	`Addr_GTX1_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx1_phase_select};
	end
	`Addr_GTX2_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx2_phase_select};
	end
	`Addr_GTX3_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx3_phase_select};
	end
	`Addr_GTX4_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx4_phase_select};
	end
	`Addr_GTX5_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx5_phase_select};
	end
	`Addr_GTX6_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx6_phase_select};
	end
	`Addr_GTX7_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx7_phase_select};
	end
	`Addr_GTX8_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx8_phase_select};
	end
	`Addr_GTX9_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx9_phase_select};
	end
	`Addr_GTX10_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx10_phase_select};
	end
	`Addr_GTX11_PHASE_SELECT : begin
	    VME_DOUT [15:0] <= {11'h0, gtx11_phase_select};
	end
	`Addr_GTX0_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_0};
	end
	`Addr_GTX1_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_0};
	end
	`Addr_GTX2_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_0};
	end
	`Addr_GTX3_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_0};
	end
	`Addr_GTX4_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_0};
	end
	`Addr_GTX5_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_0};
	end
	`Addr_GTX6_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_0};
	end
	`Addr_GTX7_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_0};
	end
	`Addr_GTX8_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_0};
	end
	`Addr_GTX9_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_0};
	end
	`Addr_GTX10_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_0};
	end
	`Addr_GTX11_PHASE_MONITOR0 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_0};
	end
	`Addr_GTX0_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_1};
	end
	`Addr_GTX1_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_1};
	end
	`Addr_GTX2_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_1};
	end
	`Addr_GTX3_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_1};
	end
	`Addr_GTX4_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_1};
	end
	`Addr_GTX5_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_1};
	end
	`Addr_GTX6_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_1};
	end
	`Addr_GTX7_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_1};
	end
	`Addr_GTX8_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_1};
	end
	`Addr_GTX9_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_1};
	end
	`Addr_GTX10_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_1};
	end
	`Addr_GTX11_PHASE_MONITOR1 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_1};
	end
	`Addr_GTX0_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_2};
	end
	`Addr_GTX1_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_2};
	end
	`Addr_GTX2_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_2};
	end
	`Addr_GTX3_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_2};
	end
	`Addr_GTX4_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_2};
	end
	`Addr_GTX5_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_2};
	end
	`Addr_GTX6_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_2};
	end
	`Addr_GTX7_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_2};
	end
	`Addr_GTX8_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_2};
	end
	`Addr_GTX9_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_2};
	end
	`Addr_GTX10_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_2};
	end
	`Addr_GTX11_PHASE_MONITOR2 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_2};
	end
	`Addr_GTX0_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx0_phase_monitor_3};
	end
	`Addr_GTX1_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx1_phase_monitor_3};
	end
	`Addr_GTX2_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx2_phase_monitor_3};
	end
	`Addr_GTX3_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx3_phase_monitor_3};
	end
	`Addr_GTX4_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx4_phase_monitor_3};
	end
	`Addr_GTX5_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx5_phase_monitor_3};
	end
	`Addr_GTX6_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx6_phase_monitor_3};
	end
	`Addr_GTX7_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx7_phase_monitor_3};
	end
	`Addr_GTX8_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx8_phase_monitor_3};
	end
	`Addr_GTX9_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx9_phase_monitor_3};
	end
	`Addr_GTX10_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx10_phase_monitor_3};
	end
	`Addr_GTX11_PHASE_MONITOR3 : begin
	    VME_DOUT [15:0] <= {10'h0, gtx11_phase_monitor_3};
	end
        `Addr_GLINK0_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink0_test_data1[15:0]; 
        end
        `Addr_GLINK0_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink0_test_data2[3:0]}; 
        end
        `Addr_GLINK1_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink1_test_data1[15:0]; 
        end
        `Addr_GLINK1_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink1_test_data2[3:0]}; 
        end
        `Addr_GLINK2_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink2_test_data1[15:0]; 
        end
        `Addr_GLINK2_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink2_test_data2[3:0]}; 
        end
        `Addr_GLINK3_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink3_test_data1[15:0]; 
        end
        `Addr_GLINK3_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink3_test_data2[3:0]}; 
        end
        `Addr_GLINK4_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink4_test_data1[15:0]; 
        end
        `Addr_GLINK4_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink4_test_data2[3:0]}; 
        end
        `Addr_GLINK5_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink5_test_data1[15:0]; 
        end
        `Addr_GLINK5_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink5_test_data2[3:0]}; 
        end
        `Addr_GLINK6_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink6_test_data1[15:0]; 
        end
        `Addr_GLINK6_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink6_test_data2[3:0]}; 
        end
        `Addr_GLINK7_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink7_test_data1[15:0]; 
        end
        `Addr_GLINK7_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink7_test_data2[3:0]}; 
        end
        `Addr_GLINK8_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink8_test_data1[15:0]; 
        end
        `Addr_GLINK8_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink8_test_data2[3:0]}; 
        end
        `Addr_GLINK9_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink9_test_data1[15:0]; 
        end
        `Addr_GLINK9_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink9_test_data2[3:0]}; 
        end
        `Addr_GLINK10_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink10_test_data1[15:0]; 
        end
        `Addr_GLINK10_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink10_test_data2[3:0]}; 
        end
        `Addr_GLINK11_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink11_test_data1[15:0]; 
        end
        `Addr_GLINK11_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink11_test_data2[3:0]}; 
        end
        `Addr_GLINK12_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink12_test_data1[15:0]; 
        end
        `Addr_GLINK12_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink12_test_data2[3:0]}; 
        end
        `Addr_GLINK13_TEST_DATA1 : begin
            VME_DOUT [15:0] <= glink13_test_data1[15:0]; 
        end
        `Addr_GLINK13_TEST_DATA2 : begin
            VME_DOUT [15:0] <= {12'b0, glink13_test_data2[3:0]}; 
        end

        `Addr_GLINK0_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink0[3:0]};
        end
        `Addr_GLINK1_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink1[3:0]};
        end
        `Addr_GLINK2_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink2[3:0]};
        end
        `Addr_GLINK3_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink3[3:0]};
        end
        `Addr_GLINK4_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink4[3:0]};
        end
        `Addr_GLINK5_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink5[3:0]};
        end
        `Addr_GLINK6_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink6[3:0]};
        end
        `Addr_GLINK7_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink7[3:0]};
        end
        `Addr_GLINK8_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink8[3:0]};
        end
        `Addr_GLINK9_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink9[3:0]};
        end
        `Addr_GLINK10_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink10[3:0]};
        end
        `Addr_GLINK11_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink11[3:0]};
        end
        `Addr_GLINK12_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink12[3:0]};
        end
        `Addr_GLINK13_PHASE_SELECT : begin
            VME_DOUT [15:0] <= {12'b0, Phase_glink13[3:0]};
        end

        `Addr_TRACKOUTPUTGENERATOR_ENV : begin
            VME_DOUT[15:0] <= {15'b0, TrackOutputGenerator_Enb};       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S0_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator0_S0_ROI[7:0] };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator1_S0_ROI[7:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator2_S0_ROI[7:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator3_S0_ROI[7:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S0_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator0_S0_pT[3:0] };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator1_S0_pT[3:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator2_S0_pT[3:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator3_S0_pT[3:0] };       
        end         
        `Addr_TRACKOUTPUTGENERATOR0_S0_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator0_S0_sign };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator1_S0_sign };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator2_S0_sign };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator3_S0_sign };       
        end  
        `Addr_TRACKOUTPUTGENERATOR0_S0_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator0_S0_InnerCoincidenceFlag[2:0] };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S0_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator1_S0_InnerCoincidenceFlag[2:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S0_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator2_S0_InnerCoincidenceFlag[2:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S0_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator3_S0_InnerCoincidenceFlag[2:0] };       
        end 
        `Addr_FLAG_TRACKGENERATOR_S0  : begin
            VME_DOUT[15:0] <= {12'b0, Flag_trackgenerator_S0[3:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S1_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator0_S1_ROI[7:0] };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator1_S1_ROI[7:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator2_S1_ROI[7:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_ROI : begin
            VME_DOUT[15:0] <= {8'b0, Trackgenerator3_S1_ROI[7:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR0_S1_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator0_S1_pT[3:0] };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator1_S1_pT[3:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator2_S1_pT[3:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_PT : begin
            VME_DOUT[15:0] <= {12'b0, Trackgenerator3_S1_pT[3:0] };       
        end         
        `Addr_TRACKOUTPUTGENERATOR0_S1_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator0_S1_sign };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator1_S1_sign };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator2_S1_sign };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_SIGN : begin
            VME_DOUT[15:0] <= {15'b0, Trackgenerator3_S1_sign };       
        end  
        `Addr_TRACKOUTPUTGENERATOR0_S1_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator0_S1_InnerCoincidenceFlag[2:0] };       
        end        
        `Addr_TRACKOUTPUTGENERATOR1_S1_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator1_S1_InnerCoincidenceFlag[2:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR2_S1_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator2_S1_InnerCoincidenceFlag[2:0] };       
        end
        `Addr_TRACKOUTPUTGENERATOR3_S1_INNERCOINCIDENCEFLAG : begin
            VME_DOUT[15:0] <= {13'b0, Trackgenerator3_S1_InnerCoincidenceFlag[2:0] };       
        end 
        `Addr_FLAG_TRACKGENERATOR_S1  : begin
            VME_DOUT[15:0] <= {12'b0, Flag_trackgenerator_S1[3:0] };       
        end
        `Addr_RESET_GLINKMONITOR  : begin
            VME_DOUT[15:0] <= { 15'b0, Reset_GLinkMonitor};       
        end
        `Addr_GL0_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL0_State_Reset};       
        end 
        `Addr_GL1_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL1_State_Reset};       
        end
        `Addr_GL2_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL2_State_Reset};       
        end
        `Addr_GL3_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL3_State_Reset};       
        end
        `Addr_GL4_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL4_State_Reset};       
        end
        `Addr_GL5_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL5_State_Reset};       
        end
        `Addr_GL6_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL6_State_Reset};       
        end
        `Addr_GL7_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL7_State_Reset};       
        end
        `Addr_GL8_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL8_State_Reset};       
        end
        `Addr_GL9_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL9_State_Reset};       
        end        
        `Addr_GL10_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL10_State_Reset};       
        end
        `Addr_GL11_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL11_State_Reset};       
        end
        `Addr_GL12_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL12_State_Reset};       
        end   
        `Addr_GL13_STATE_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL13_State_Reset};       
        end
        `Addr_GL0_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL0_ErrorCounter_Reset};       
        end
        `Addr_GL1_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL1_ErrorCounter_Reset};       
        end
        `Addr_GL2_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL2_ErrorCounter_Reset};       
        end
        `Addr_GL3_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL3_ErrorCounter_Reset};       
        end
        `Addr_GL4_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL4_ErrorCounter_Reset};       
        end
        `Addr_GL5_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL5_ErrorCounter_Reset};       
        end
        `Addr_GL6_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL6_ErrorCounter_Reset};       
        end
        `Addr_GL7_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL7_ErrorCounter_Reset};       
        end
        `Addr_GL8_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL8_ErrorCounter_Reset};       
        end
        `Addr_GL9_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL9_ErrorCounter_Reset};       
        end
        `Addr_GL10_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL10_ErrorCounter_Reset};       
        end
        `Addr_GL11_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL11_ErrorCounter_Reset};       
        end
        `Addr_GL12_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL12_ErrorCounter_Reset};       
        end
        `Addr_GL13_ERRORCOUNTER_RESET  : begin
            VME_DOUT[15:0] <= { 15'b0, GL13_ErrorCounter_Reset};       
        end        
        `Addr_RX_DIVEN  : begin
            VME_DOUT[15:0] <= { 15'b0, RX_DIVen};       
        end
        `Addr_GL0_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL0_RX_DIV_vme[1:0]};       
        end
        `Addr_GL1_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL1_RX_DIV_vme[1:0]};       
        end
        `Addr_GL2_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL2_RX_DIV_vme[1:0]};       
        end
        `Addr_GL3_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL3_RX_DIV_vme[1:0]};       
        end
        `Addr_GL4_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL4_RX_DIV_vme[1:0]};       
        end
        `Addr_GL5_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL5_RX_DIV_vme[1:0]};       
        end
        `Addr_GL6_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL6_RX_DIV_vme[1:0]};       
        end
        `Addr_GL7_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL7_RX_DIV_vme[1:0]};       
        end
        `Addr_GL8_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL8_RX_DIV_vme[1:0]};       
        end
        `Addr_GL9_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL9_RX_DIV_vme[1:0]};       
        end
        `Addr_GL10_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL10_RX_DIV_vme[1:0]};       
        end
        `Addr_GL11_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL11_RX_DIV_vme[1:0]};       
        end 
        `Addr_GL12_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL12_RX_DIV_vme[1:0]};       
        end 
        `Addr_GL13_RX_DIV_VME  : begin
            VME_DOUT[15:0] <= { 14'b0, GL13_RX_DIV_vme[1:0]};       
        end                                                                                                                                                                                                                                                                                                                                              
        `Addr_GL0_STATE  : begin
            VME_DOUT[15:0] <= {GL0_State[15:0] };       
        end
        `Addr_GL1_STATE  : begin
            VME_DOUT[15:0] <= {GL1_State[15:0] };       
        end 
        `Addr_GL2_STATE  : begin
            VME_DOUT[15:0] <= {GL2_State[15:0] };       
        end 
        `Addr_GL3_STATE  : begin
            VME_DOUT[15:0] <= {GL3_State[15:0] };       
        end 
        `Addr_GL4_STATE  : begin
            VME_DOUT[15:0] <= {GL4_State[15:0] };       
        end 
        `Addr_GL5_STATE  : begin
            VME_DOUT[15:0] <= {GL5_State[15:0] };       
        end 
        `Addr_GL6_STATE  : begin
            VME_DOUT[15:0] <= {GL6_State[15:0] };       
        end 
        `Addr_GL7_STATE  : begin
            VME_DOUT[15:0] <= {GL7_State[15:0] };       
        end 
        `Addr_GL8_STATE  : begin
            VME_DOUT[15:0] <= {GL8_State[15:0] };       
        end 
        `Addr_GL9_STATE  : begin
            VME_DOUT[15:0] <= {GL9_State[15:0] };       
        end 
        `Addr_GL10_STATE  : begin
            VME_DOUT[15:0] <= {GL10_State[15:0] };       
        end 
        `Addr_GL11_STATE  : begin
            VME_DOUT[15:0] <= {GL11_State[15:0] };       
        end 
        `Addr_GL12_STATE  : begin
            VME_DOUT[15:0] <= {GL12_State[15:0] };       
        end 
        `Addr_GL13_STATE  : begin
            VME_DOUT[15:0] <= {GL13_State[15:0] };       
        end  
        `Addr_GL0_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL0_ErrorCounter[15:0] };       
        end
        `Addr_GL1_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL1_ErrorCounter[15:0] };       
        end 
        `Addr_GL2_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL2_ErrorCounter[15:0] };       
        end 
        `Addr_GL3_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL3_ErrorCounter[15:0] };       
        end 
        `Addr_GL4_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL4_ErrorCounter[15:0] };       
        end 
        `Addr_GL5_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL5_ErrorCounter[15:0] };       
        end 
        `Addr_GL6_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL6_ErrorCounter[15:0] };       
        end 
        `Addr_GL7_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL7_ErrorCounter[15:0] };       
        end 
        `Addr_GL8_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL8_ErrorCounter[15:0] };       
        end 
        `Addr_GL9_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL9_ErrorCounter[15:0] };       
        end 
        `Addr_GL10_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL10_ErrorCounter[15:0] };       
        end 
        `Addr_GL11_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL11_ErrorCounter[15:0] };       
        end 
        `Addr_GL12_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL12_ErrorCounter[15:0] };       
        end 
        `Addr_GL13_ERRORCOUNTER  : begin
            VME_DOUT[15:0] <= {GL13_ErrorCounter[15:0] };       
        end                                                                                                                                                       
        default : begin
            VME_DOUT [15:0] <= 16'h1992;
        end
    endcase
    end
    
endmodule
