`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/11 00:23:43
// Design Name: 
// Module Name: GTX_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GTX_TX(
    input wire          CLK_in,    
    input wire          reset_in,
    input wire          TX_reset_in,
    input wire          TX_Logic_reset_in,

    input wire          gt_txusrclk_in,
    input wire [11:0]   BCID_in,
    input wire          ConverterID_in,
    
    input wire [47:0]  gtx0_in,
    input wire [47:0]  gtx1_in,
    input wire [47:0]  gtx2_in,
    input wire [47:0]  gtx3_in,
    input wire [47:0]  gtx4_in,
    input wire [47:0]  gtx5_in,
    input wire [47:0]  gtx6_in,
    input wire [47:0]  gtx7_in,
    input wire [47:0]  gtx8_in,
    input wire [47:0]  gtx9_in,
    input wire [47:0]  gtx10_in,
    input wire [47:0]  gtx11_in,


    output reg [31:0]   gt0_txdata_out,
    output reg [31:0]   gt1_txdata_out,
    output reg [31:0]   gt2_txdata_out,
    output reg [31:0]   gt3_txdata_out,
    output reg [31:0]   gt4_txdata_out,
    output reg [31:0]   gt5_txdata_out,
    output reg [31:0]   gt6_txdata_out,
    output reg [31:0]   gt7_txdata_out,
    output reg [31:0]   gt8_txdata_out,
    output reg [31:0]   gt9_txdata_out,
    output reg [31:0]   gt10_txdata_out,
    output reg [31:0]   gt11_txdata_out,

    output reg [3:0]    gt0_txcharisk_out,
    output reg [3:0]    gt1_txcharisk_out,
    output reg [3:0]    gt2_txcharisk_out,
    output reg [3:0]    gt3_txcharisk_out,
    output reg [3:0]    gt4_txcharisk_out,
    output reg [3:0]    gt5_txcharisk_out,
    output reg [3:0]    gt6_txcharisk_out,
    output reg [3:0]    gt7_txcharisk_out,
    output reg [3:0]    gt8_txcharisk_out,
    output reg [3:0]    gt9_txcharisk_out,
    output reg [3:0]    gt10_txcharisk_out,
    output reg [3:0]    gt11_txcharisk_out
    );
    
wire RST_in = reset_in||TX_reset_in||TX_Logic_reset_in;
    
parameter PAUSE = 5'b1;
parameter HEADER = 5'b10;
parameter TRACK1 = 5'b100;
parameter TRACK2 = 5'b1000;
parameter FOOTER = 5'b10000;
    
// tmp
wire [7:0] CRC0;
wire [7:0] CRC1;
wire [7:0] CRC2;
wire [7:0] CRC3;
wire [7:0] CRC4;
wire [7:0] CRC5;
wire [7:0] CRC6;
wire [7:0] CRC7;
wire [7:0] CRC8;
wire [7:0] CRC9;
wire [7:0] CRC10;
wire [7:0] CRC11;




    
///// TX LOGIC ////////
       
reg [4:0] TX0_state = 5'b1;
reg [4:0] TX10_state = 5'b1;
reg TX0_CLK_is_high = 1'b1;
reg TX10_CLK_is_high = 1'b1;


reg [15:0] hoge = 16'h0102;
reg [15:0] hogehoge = 16'h0a0b;



//reg [15:0] TXcounterall = 16'h0;
//wire [15:0] TXcounterall_w;
//reg [15:0] TXcounter = 16'h0;
//reg [15:0] TXcounter80 = 16'h0;

//assign TXcounterall_w = TXcounterall;
    
CRC8_D48    CRC_0(
        .Data   (gtx0_in),
        .crc    (8'b0),
        .newcrc(CRC0)
    );

CRC8_D48    CRC_1(
        .Data   (gtx1_in),
        .crc    (8'b0),
        .newcrc(CRC1)
    );    

CRC8_D48    CRC_2(
        .Data   (gtx2_in),
        .crc    (8'b0),
        .newcrc(CRC2)
    );

CRC8_D48    CRC_3(
        .Data   (gtx3_in),
        .crc    (8'b0),
        .newcrc(CRC3)
    );

CRC8_D48    CRC_4(
        .Data   (gtx4_in),
        .crc    (8'b0),
        .newcrc(CRC4)
    );

CRC8_D48    CRC_5(
        .Data   (gtx5_in),
        .crc    (8'b0),
        .newcrc(CRC5)
    );
 
CRC8_D48    CRC_6(
        .Data   (gtx6_in),
        .crc    (8'b0),
        .newcrc(CRC6)
    );
    
CRC8_D48    CRC_7(
        .Data   (gtx7_in),
        .crc    (8'b0),
        .newcrc(CRC7)
    );
       
CRC8_D48    CRC_8(
        .Data   (gtx8_in),
        .crc    (8'b0),
        .newcrc(CRC8)
    );       
    
CRC8_D48    CRC_9(
        .Data   (gtx9_in),
        .crc    (8'b0),
        .newcrc(CRC9)
    );    
    
CRC8_D48    CRC_10(
        .Data   (gtx10_in),
        .crc    (8'b0),
        .newcrc(CRC10)
    );
    
CRC8_D48    CRC_11(
        .Data   (gtx11_in),
        .crc    (8'b0),
        .newcrc(CRC11)
    );    
    
   
    
    always @(posedge gt_txusrclk_in or posedge RST_in) begin
       //TXcounterall <= TXcounterall + 16'b1; //for loopback test
       //TXcounter <= TXcounterall_w; // for loopback test
        if (RST_in) begin
            TX0_state <= PAUSE;
            gt0_txdata_out <= 32'hbcbc;
            gt0_txcharisk_out <= 4'h1;
            gt1_txdata_out <= 32'hbcbc;
            gt1_txcharisk_out <= 4'h1;
            gt2_txdata_out <= 32'hbcbc;
            gt2_txcharisk_out <= 4'h1;
            gt3_txdata_out <= 32'hbcbc;
            gt3_txcharisk_out <= 4'h1;
            gt4_txdata_out <= 32'hbcbc;
            gt4_txcharisk_out <= 4'h1;
            gt5_txdata_out <= 32'hbcbc;
            gt5_txcharisk_out <= 4'h1;
            gt6_txdata_out <= 32'hbcbc;
            gt6_txcharisk_out <= 4'h1;
            gt7_txdata_out <= 32'hbcbc;
            gt7_txcharisk_out <= 4'h1;
            gt8_txdata_out <= 32'hbcbc;
            gt8_txcharisk_out <= 4'h1;
            gt9_txdata_out <= 32'hbcbc;
            gt9_txcharisk_out <= 4'h1;
            gt10_txdata_out <= 32'hbcbc;
            gt10_txcharisk_out <= 4'h1;
            gt11_txdata_out <= 32'hbcbc;
            gt11_txcharisk_out <= 4'h1;
            TX0_CLK_is_high <= 1'b1;
        end
        else begin
            case (TX0_state)
                PAUSE : begin
                    if(!CLK_in) begin
                        TX0_CLK_is_high <= 1'b0;
                        TX0_state <= PAUSE;
                    end
                    else if(CLK_in && !TX0_CLK_is_high) begin
                        TX0_CLK_is_high <= 1'b1;                        
                        TX0_state <= HEADER;
                    end
                    else begin
                        TX0_state <= PAUSE;                        
                    end
                end                
                HEADER : begin
                    gt0_txdata_out <= gtx0_in[31:0];
                    gt0_txcharisk_out <= 4'b0;
                    gt1_txdata_out <= gtx1_in[31:0];
                    gt1_txcharisk_out <= 4'b0;
                    gt2_txdata_out <= gtx2_in[31:0];
                    gt2_txcharisk_out <= 4'b0;
                    gt3_txdata_out <= gtx3_in[31:0];
                    gt3_txcharisk_out <= 4'b0;
                    gt4_txdata_out <= gtx4_in[31:0];
                    gt4_txcharisk_out <= 4'b0;
                    gt5_txdata_out <= gtx5_in[31:0];
                    gt5_txcharisk_out <= 4'b0;
                    gt6_txdata_out <= gtx6_in[31:0];
                    gt6_txcharisk_out <= 4'b0;
                    gt7_txdata_out <= gtx7_in[31:0];
                    gt7_txcharisk_out <= 4'b0;
                    gt8_txdata_out <= gtx8_in[31:0];
                    gt8_txcharisk_out <= 4'b0;
                    gt9_txdata_out <= gtx9_in[31:0];
                    gt9_txcharisk_out <= 4'b0;
                    gt10_txdata_out <= gtx10_in[31:0];
                    gt10_txcharisk_out <= 4'b0;
                    gt11_txdata_out <= gtx11_in[31:0];
                    gt11_txcharisk_out <= 4'b0;
                    TX0_state <= TRACK1;
                end                
                TRACK1 : begin
                    gt0_txdata_out <= {4'h0,BCID_in[11:0], gtx0_in[47:32]};
                    gt0_txcharisk_out <= 4'h0;
                    gt1_txdata_out <= {4'h0,BCID_in[11:0], gtx1_in[47:32]};
                    gt1_txcharisk_out <= 4'h0;
                    gt2_txdata_out <= {4'h0,BCID_in[11:0], gtx2_in[47:32]};
                    gt2_txcharisk_out <= 4'h0;
                    gt3_txdata_out <= {4'h0,BCID_in[11:0], gtx3_in[47:32]};
                    gt3_txcharisk_out <= 4'h0;
                    gt4_txdata_out <= {4'h0,BCID_in[11:0], gtx4_in[47:32]};
                    gt4_txcharisk_out <= 4'h0;
                    gt5_txdata_out <= {4'h0,BCID_in[11:0], gtx5_in[47:32]};
                    gt5_txcharisk_out <= 4'h0;
                    gt6_txdata_out <= {4'h0,BCID_in[11:0], gtx6_in[47:32]};
                    gt6_txcharisk_out <= 4'h0;
                    gt7_txdata_out <= {4'h0,BCID_in[11:0], gtx7_in[47:32]};
                    gt7_txcharisk_out <= 4'h0;
                    gt8_txdata_out <= {4'h0,BCID_in[11:0], gtx8_in[47:32]};
                    gt8_txcharisk_out <= 4'h0;
                    gt9_txdata_out <= {4'h0,BCID_in[11:0], gtx9_in[47:32]};
                    gt9_txcharisk_out <= 4'h0;
                    gt10_txdata_out <= {4'h0,BCID_in[11:0], gtx10_in[47:32]};
                    gt10_txcharisk_out <= 4'h0;
                    gt11_txdata_out <= {4'h0,BCID_in[11:0], gtx11_in[47:32]};
                    gt11_txcharisk_out <= 4'h0;
                    TX0_state <= TRACK2;
                end
                TRACK2 : begin
                    gt0_txdata_out <= {8'hbc, 8'hbc, CRC0, 3'b000, ConverterID_in, 4'h0};
                    gt0_txcharisk_out <= 4'b1100;                
                    gt1_txdata_out <= {8'hbc, 8'hbc, CRC1, 3'b000, ConverterID_in, 4'h1};
                    gt1_txcharisk_out <= 4'b1100;                
                    gt2_txdata_out <= {8'hbc, 8'hbc, CRC2, 3'b000, ConverterID_in, 4'h2};
                    gt2_txcharisk_out <= 4'b1100;                
                    gt3_txdata_out <= {8'hbc, 8'hbc, CRC3, 3'b000, ConverterID_in, 4'h3};
                    gt3_txcharisk_out <= 4'b1100;                
                    gt4_txdata_out <= {8'hbc, 8'hbc, CRC4, 3'b000, ConverterID_in, 4'h4};
                    gt4_txcharisk_out <= 4'b1100;                
                    gt5_txdata_out <= {8'hbc, 8'hbc, CRC5, 3'b000, ConverterID_in, 4'h5};
                    gt5_txcharisk_out <= 4'b1100;                
                    gt6_txdata_out <= {8'hbc, 8'hbc, CRC6, 3'b000, ConverterID_in, 4'h6};
                    gt6_txcharisk_out <= 4'b1100;                
                    gt7_txdata_out <= {8'hbc, 8'hbc, CRC7, 3'b000, ConverterID_in, 4'h7};
                    gt7_txcharisk_out <= 4'b1100;                
                    gt8_txdata_out <= {8'hbc, 8'hbc, CRC8, 3'b000, ConverterID_in, 4'h8};
                    gt8_txcharisk_out <= 4'b1100;                
                    gt9_txdata_out <= {8'hbc, 8'hbc, CRC9, 3'b000, ConverterID_in, 4'h9};
                    gt9_txcharisk_out <= 4'b1100;                
                    gt10_txdata_out <= {8'hbc, 8'hbc, CRC10, 3'b000, ConverterID_in, 4'ha};
                    gt10_txcharisk_out <= 4'b1100;                
                    gt11_txdata_out <= {8'hbc, 8'hbc, CRC11, 3'b000, ConverterID_in, 4'hb};
                    gt11_txcharisk_out <= 4'b1100;                
                    TX0_state <= FOOTER;
                end
                FOOTER : begin
                    gt0_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt0_txcharisk_out <= 4'h0;                
                    gt1_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt1_txcharisk_out <= 4'h0;                
                    gt2_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt2_txcharisk_out <= 4'h0;                
                    gt3_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt3_txcharisk_out <= 4'h0;                
                    gt4_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt4_txcharisk_out <= 4'h0;                
                    gt5_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt5_txcharisk_out <= 4'h0;                
                    gt6_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt6_txcharisk_out <= 4'h0;                
                    gt7_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt7_txcharisk_out <= 4'h0;                
                    gt8_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt8_txcharisk_out <= 4'h0;                
                    gt9_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt9_txcharisk_out <= 4'h0;                
                    gt10_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt10_txcharisk_out <= 4'h0;                
                    gt11_txdata_out <= {hogehoge[15:0], hoge[15:0]};
                    gt11_txcharisk_out <= 4'h0;                
                    TX0_state <= HEADER;
                end
                default : begin
                    gt0_txdata_out <= 32'heeee;
                    gt0_txcharisk_out <= 4'h0;
                    gt1_txdata_out <= 32'heeee;
                    gt1_txcharisk_out <= 4'h0;
                    gt2_txdata_out <= 32'heeee;
                    gt2_txcharisk_out <= 4'h0;
                    gt3_txdata_out <= 32'heeee;
                    gt3_txcharisk_out <= 4'h0;
                    gt4_txdata_out <= 32'heeee;
                    gt4_txcharisk_out <= 4'h0;
                    gt5_txdata_out <= 32'heeee;
                    gt5_txcharisk_out <= 4'h0;
                    gt6_txdata_out <= 32'heeee;
                    gt6_txcharisk_out <= 4'h0;
                    gt7_txdata_out <= 32'heeee;
                    gt7_txcharisk_out <= 4'h0;
                    gt8_txdata_out <= 32'heeee;
                    gt8_txcharisk_out <= 4'h0;
                    gt9_txdata_out <= 32'heeee;
                    gt9_txcharisk_out <= 4'h0;
                    gt10_txdata_out <= 32'heeee;
                    gt10_txcharisk_out <= 2'b00;
                    gt11_txdata_out <= 32'heeee;
                    gt11_txcharisk_out <= 2'b00;
                    TX0_state <= PAUSE;
                end                        
            endcase
        end
    end
    
/*    
    always @(posedge gt_txusrclk_in_80 or posedge RST_in) begin
        //TXcounter80 <= TXcounterall_w; //for loopback test
        if (RST_in) begin
            TX10_state <= PAUSE;
            gt10_txdata_out <= 16'hbcbc;
            gt10_txcharisk_out <= 2'h1;
            gt11_txdata_out <= 16'hbcbc;
            gt11_txcharisk_out <= 2'h1;
            TX10_CLK_is_high <= 1'b1;
        end
        else begin
            case (TX10_state)
                PAUSE : begin
                    if(!CLK_in) begin
                        TX10_CLK_is_high <= 1'b0;
                        TX10_state <= PAUSE;
                    end
                    else if(CLK_in && !TX10_CLK_is_high) begin
                        TX10_CLK_is_high <= 1'b1;                        
                        TX10_state <= HEADER;
                    end
                    else begin
                        TX10_state <= PAUSE;                        
                    end
                end                
                HEADER : begin
                    gt10_txdata_out <= {8'hbc, 8'hbc};
                    gt10_txcharisk_out <= 2'h1;
                    gt11_txdata_out <= {8'hbc, 8'hbc};
                    gt11_txcharisk_out <= 2'h1;
                    TX10_state <= FOOTER;
                end                
                FOOTER : begin
                    gt10_txdata_out <= {flag_S0_in[3:0], BCID_in[11:0]};
                    gt10_txcharisk_out <= 2'b00;
                    gt11_txdata_out <= {flag_S1_in[3:0], BCID_in[11:0]};
                    gt11_txcharisk_out <= 2'b00;                 
                    TX10_state <= HEADER;
                end
                default : begin
                    gt10_txdata_out <= 16'heeee;
                    gt10_txcharisk_out <= 2'b00;
                    gt11_txdata_out <= 16'heeee;
                    gt11_txcharisk_out <= 2'b00;
                    TX10_state <= PAUSE;
                end                        
            endcase
        end
    end
*/
endmodule
