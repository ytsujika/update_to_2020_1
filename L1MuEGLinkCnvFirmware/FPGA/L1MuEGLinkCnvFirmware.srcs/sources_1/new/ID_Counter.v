`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/08/02 04:11:50
// Design Name: 
// Module Name: ID_Counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ID_Counter(
    input wire CLK_in,
    input wire reset_in,

    input wire L1A_in,
    input wire BCR_in,
    input wire trig_BCR_in,
    input wire ECR_in,
    
    output reg [11:0] BCID_out,
    output reg [11:0] trig_BCID_out,
    output reg [11:0] L1ID_out,
    output reg [31:0] L1A_Counter_out
    );
    
    always @(posedge CLK_in) begin
        if(reset_in||BCR_in) begin
            BCID_out <= 12'b0;
        end
        else begin
            BCID_out <= BCID_out + 12'b1;            
        end
    end

    always @(posedge CLK_in) begin
        if(reset_in||trig_BCR_in) begin
            trig_BCID_out <= 12'b0;
        end
        else begin
            trig_BCID_out <= trig_BCID_out + 12'b1;            
        end
    end

    always @(posedge CLK_in) begin
        if(reset_in||ECR_in) begin
            L1ID_out <= 12'b0;
        end
        else if (L1A_in) begin
            L1ID_out <= L1ID_out + 12'b1;
        end
        else begin
            L1ID_out <= L1ID_out;
        end
    end


    always @(posedge CLK_in) begin
        if(reset_in) begin
            L1A_Counter_out <= 32'b0;
        end
        else if (L1A_in) begin
            L1A_Counter_out <= L1A_Counter_out + 32'h1;
        end
        else begin
            L1A_Counter_out <= L1A_Counter_out;
        end
    end

endmodule
