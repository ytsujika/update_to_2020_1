`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/07/16 17:29:21
// Design Name: 
// Module Name: GTX_Delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GTX_Delay(
    input wire CLK_in,
    input wire CLK_in_0,
    input wire CLK_in_1,
    input wire CLK_in_2,
    input wire CLK_in_3,
    input wire reset_in,
    input wire Delay_reset_in,

    input wire [111:0] gtx0_data_in,
    input wire [111:0] gtx1_data_in,
    input wire [111:0] gtx2_data_in,
    input wire [111:0] gtx3_data_in,
    input wire [111:0] gtx4_data_in,
    input wire [111:0] gtx5_data_in,
    input wire [111:0] gtx6_data_in,
    input wire [111:0] gtx7_data_in,
    input wire [111:0] gtx8_data_in,
    input wire [111:0] gtx9_data_in,
    input wire [111:0] gtx10_data_in,
    input wire [111:0] gtx11_data_in,
    
    input wire [6:0] gtx0_delay_in,
    input wire [6:0] gtx1_delay_in,
    input wire [6:0] gtx2_delay_in,
    input wire [6:0] gtx3_delay_in,
    input wire [6:0] gtx4_delay_in,
    input wire [6:0] gtx5_delay_in,
    input wire [6:0] gtx6_delay_in,
    input wire [6:0] gtx7_delay_in,
    input wire [6:0] gtx8_delay_in,
    input wire [6:0] gtx9_delay_in,
    input wire [6:0] gtx10_delay_in,
    input wire [6:0] gtx11_delay_in,    

    input wire [3:0] gtx0_phase_in,
    input wire [3:0] gtx1_phase_in,
    input wire [3:0] gtx2_phase_in,
    input wire [3:0] gtx3_phase_in,
    input wire [3:0] gtx4_phase_in,
    input wire [3:0] gtx5_phase_in,
    input wire [3:0] gtx6_phase_in,
    input wire [3:0] gtx7_phase_in,
    input wire [3:0] gtx8_phase_in,
    input wire [3:0] gtx9_phase_in,
    input wire [3:0] gtx10_phase_in,
    input wire [3:0] gtx11_phase_in,

    output wire [5:0] gtx0_phase_0_out,
    output wire [5:0] gtx0_phase_1_out,
    output wire [5:0] gtx0_phase_2_out,
    output wire [5:0] gtx0_phase_3_out,

    output wire [5:0] gtx1_phase_0_out,
    output wire [5:0] gtx1_phase_1_out,
    output wire [5:0] gtx1_phase_2_out,
    output wire [5:0] gtx1_phase_3_out,

    output wire [5:0] gtx2_phase_0_out,
    output wire [5:0] gtx2_phase_1_out,
    output wire [5:0] gtx2_phase_2_out,
    output wire [5:0] gtx2_phase_3_out,

    output wire [5:0] gtx3_phase_0_out,
    output wire [5:0] gtx3_phase_1_out,
    output wire [5:0] gtx3_phase_2_out,
    output wire [5:0] gtx3_phase_3_out,

    output wire [5:0] gtx4_phase_0_out,
    output wire [5:0] gtx4_phase_1_out,
    output wire [5:0] gtx4_phase_2_out,
    output wire [5:0] gtx4_phase_3_out,

    output wire [5:0] gtx5_phase_0_out,
    output wire [5:0] gtx5_phase_1_out,
    output wire [5:0] gtx5_phase_2_out,
    output wire [5:0] gtx5_phase_3_out,

    output wire [5:0] gtx6_phase_0_out,
    output wire [5:0] gtx6_phase_1_out,
    output wire [5:0] gtx6_phase_2_out,
    output wire [5:0] gtx6_phase_3_out,

    output wire [5:0] gtx7_phase_0_out,
    output wire [5:0] gtx7_phase_1_out,
    output wire [5:0] gtx7_phase_2_out,
    output wire [5:0] gtx7_phase_3_out,

    output wire [5:0] gtx8_phase_0_out,
    output wire [5:0] gtx8_phase_1_out,
    output wire [5:0] gtx8_phase_2_out,
    output wire [5:0] gtx8_phase_3_out,

    output wire [5:0] gtx9_phase_0_out,
    output wire [5:0] gtx9_phase_1_out,
    output wire [5:0] gtx9_phase_2_out,
    output wire [5:0] gtx9_phase_3_out,

    output wire [5:0] gtx10_phase_0_out,
    output wire [5:0] gtx10_phase_1_out,
    output wire [5:0] gtx10_phase_2_out,
    output wire [5:0] gtx10_phase_3_out,

    output wire [5:0] gtx11_phase_0_out,
    output wire [5:0] gtx11_phase_1_out,
    output wire [5:0] gtx11_phase_2_out,
    output wire [5:0] gtx11_phase_3_out,

    output wire [111:0] gtx0_delayed_out,
    output wire [111:0] gtx1_delayed_out,
    output wire [111:0] gtx2_delayed_out,
    output wire [111:0] gtx3_delayed_out,
    output wire [111:0] gtx4_delayed_out,
    output wire [111:0] gtx5_delayed_out,
    output wire [111:0] gtx6_delayed_out,
    output wire [111:0] gtx7_delayed_out,
    output wire [111:0] gtx8_delayed_out,
    output wire [111:0] gtx9_delayed_out,
    output wire [111:0] gtx10_delayed_out,
    output wire [111:0] gtx11_delayed_out    

    );
wire RST_in = reset_in||Delay_reset_in;
    
Delay Delay_gtx0 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx0_data_in), .delaynum_in(gtx0_delay_in), .phase_in(gtx0_phase_in), .reset_in(RST_in), .data_out(gtx0_delayed_out),
		   .phase_out_0(gtx0_phase_0_out),
		   .phase_out_1(gtx0_phase_1_out),
		   .phase_out_2(gtx0_phase_2_out),
		   .phase_out_3(gtx0_phase_3_out)
);

Delay Delay_gtx1 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx1_data_in), .delaynum_in(gtx1_delay_in), .phase_in(gtx1_phase_in), .reset_in(RST_in), .data_out(gtx1_delayed_out),
		   .phase_out_0(gtx1_phase_0_out),
		   .phase_out_1(gtx1_phase_1_out),
		   .phase_out_2(gtx1_phase_2_out),
		   .phase_out_3(gtx1_phase_3_out)
);

Delay Delay_gtx2 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx2_data_in), .delaynum_in(gtx2_delay_in), .phase_in(gtx2_phase_in), .reset_in(RST_in), .data_out(gtx2_delayed_out),
		   .phase_out_0(gtx2_phase_0_out),
		   .phase_out_1(gtx2_phase_1_out),
		   .phase_out_2(gtx2_phase_2_out),
		   .phase_out_3(gtx2_phase_3_out)
);

Delay Delay_gtx3 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx3_data_in), .delaynum_in(gtx3_delay_in), .phase_in(gtx3_phase_in), .reset_in(RST_in), .data_out(gtx3_delayed_out),
		   .phase_out_0(gtx3_phase_0_out),
		   .phase_out_1(gtx3_phase_1_out),
		   .phase_out_2(gtx3_phase_2_out),
		   .phase_out_3(gtx3_phase_3_out)
);

Delay Delay_gtx4 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx4_data_in), .delaynum_in(gtx4_delay_in), .phase_in(gtx4_phase_in), .reset_in(RST_in), .data_out(gtx4_delayed_out),
		   .phase_out_0(gtx4_phase_0_out),
		   .phase_out_1(gtx4_phase_1_out),
		   .phase_out_2(gtx4_phase_2_out),
		   .phase_out_3(gtx4_phase_3_out)
);

Delay Delay_gtx5 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx5_data_in), .delaynum_in(gtx5_delay_in), .phase_in(gtx5_phase_in), .reset_in(RST_in), .data_out(gtx5_delayed_out),
		   .phase_out_0(gtx5_phase_0_out),
		   .phase_out_1(gtx5_phase_1_out),
		   .phase_out_2(gtx5_phase_2_out),
		   .phase_out_3(gtx5_phase_3_out)
);

Delay Delay_gtx6 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx6_data_in), .delaynum_in(gtx6_delay_in), .phase_in(gtx6_phase_in), .reset_in(RST_in), .data_out(gtx6_delayed_out),
		   .phase_out_0(gtx6_phase_0_out),
		   .phase_out_1(gtx6_phase_1_out),
		   .phase_out_2(gtx6_phase_2_out),
		   .phase_out_3(gtx6_phase_3_out)
);

Delay Delay_gtx7 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx7_data_in), .delaynum_in(gtx7_delay_in), .phase_in(gtx7_phase_in), .reset_in(RST_in), .data_out(gtx7_delayed_out),
		   .phase_out_0(gtx7_phase_0_out),
		   .phase_out_1(gtx7_phase_1_out),
		   .phase_out_2(gtx7_phase_2_out),
		   .phase_out_3(gtx7_phase_3_out)
);

Delay Delay_gtx8 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx8_data_in), .delaynum_in(gtx8_delay_in), .phase_in(gtx8_phase_in), .reset_in(RST_in), .data_out(gtx8_delayed_out),
		   .phase_out_0(gtx8_phase_0_out),
		   .phase_out_1(gtx8_phase_1_out),
		   .phase_out_2(gtx8_phase_2_out),
		   .phase_out_3(gtx8_phase_3_out)
);

Delay Delay_gtx9 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx9_data_in), .delaynum_in(gtx9_delay_in), .phase_in(gtx9_phase_in), .reset_in(RST_in), .data_out(gtx9_delayed_out),
		   .phase_out_0(gtx9_phase_0_out),
		   .phase_out_1(gtx9_phase_1_out),
		   .phase_out_2(gtx9_phase_2_out),
		   .phase_out_3(gtx9_phase_3_out)
);

Delay Delay_gtx10 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx10_data_in), .delaynum_in(gtx10_delay_in), .phase_in(gtx10_phase_in), .reset_in(RST_in), .data_out(gtx10_delayed_out),
		   .phase_out_0(gtx10_phase_0_out),
		   .phase_out_1(gtx10_phase_1_out),
		   .phase_out_2(gtx10_phase_2_out),
		   .phase_out_3(gtx10_phase_3_out)
);

Delay Delay_gtx11 ( .CLK_in(CLK_in), .CLK_in_0(CLK_in_0), .CLK_in_1(CLK_in_1), .CLK_in_2(CLK_in_2), .CLK_in_3(CLK_in_3), 
		   .data_in(gtx11_data_in), .delaynum_in(gtx11_delay_in), .phase_in(gtx11_phase_in), .reset_in(RST_in), .data_out(gtx11_delayed_out),
		   .phase_out_0(gtx11_phase_0_out),
		   .phase_out_1(gtx11_phase_1_out),
		   .phase_out_2(gtx11_phase_2_out),
		   .phase_out_3(gtx11_phase_3_out)
);

endmodule
