`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2016/11/23 18:30:07
// Design Name: 
// Module Name: GLink_Delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GLink_Delay(
    input wire CLK_in,
    input wire reset_in,
    input wire Delay_reset_in,

    input wire [20:0] glink0_data_in,
    input wire [20:0] glink1_data_in,
    input wire [20:0] glink2_data_in,
    input wire [20:0] glink3_data_in,
    input wire [20:0] glink4_data_in,
    input wire [20:0] glink5_data_in,
    input wire [20:0] glink6_data_in,
    input wire [20:0] glink7_data_in,
    input wire [20:0] glink8_data_in,
    input wire [20:0] glink9_data_in,
    input wire [20:0] glink10_data_in,
    input wire [20:0] glink11_data_in,
    input wire [20:0] glink12_data_in,
    input wire [20:0] glink13_data_in,

    input wire [7:0] glink0_delay_in,
    input wire [7:0] glink1_delay_in,
    input wire [7:0] glink2_delay_in,
    input wire [7:0] glink3_delay_in,
    input wire [7:0] glink4_delay_in,
    input wire [7:0] glink5_delay_in,
    input wire [7:0] glink6_delay_in,
    input wire [7:0] glink7_delay_in,
    input wire [7:0] glink8_delay_in,
    input wire [7:0] glink9_delay_in,
    input wire [7:0] glink10_delay_in,
    input wire [7:0] glink11_delay_in,    
    input wire [7:0] glink12_delay_in,    
    input wire [7:0] glink13_delay_in,    

    output reg [16:0] glink0_delayed_out,
    output reg [16:0] glink1_delayed_out,
    output reg [16:0] glink2_delayed_out,
    output reg [16:0] glink3_delayed_out,
    output reg [16:0] glink4_delayed_out,
    output reg [16:0] glink5_delayed_out,
    output reg [16:0] glink6_delayed_out,
    output reg [16:0] glink7_delayed_out,
    output reg [16:0] glink8_delayed_out,
    output reg [16:0] glink9_delayed_out,
    output reg [16:0] glink10_delayed_out,
    output reg [16:0] glink11_delayed_out,
    output reg [16:0] glink12_delayed_out,
    output reg [16:0] glink13_delayed_out
    );

wire RST_in = reset_in||Delay_reset_in;

wire [20:0] glink0_delayed;
wire [20:0] glink1_delayed;
wire [20:0] glink2_delayed;
wire [20:0] glink3_delayed;
wire [20:0] glink4_delayed;
wire [20:0] glink5_delayed;
wire [20:0] glink6_delayed;
wire [20:0] glink7_delayed;
wire [20:0] glink8_delayed;
wire [20:0] glink9_delayed;
wire [20:0] glink10_delayed;
wire [20:0] glink11_delayed;
wire [20:0] glink12_delayed;
wire [20:0] glink13_delayed;

Delay_glink Delay_glink0 ( .CLK_in(CLK_in), .data_in(glink0_data_in), .delaynum_in(glink0_delay_in), .reset_in(RST_in), .data_out(glink0_delayed));
Delay_glink Delay_glink1 ( .CLK_in(CLK_in), .data_in(glink1_data_in), .delaynum_in(glink1_delay_in), .reset_in(RST_in), .data_out(glink1_delayed));
Delay_glink Delay_glink2 ( .CLK_in(CLK_in), .data_in(glink2_data_in), .delaynum_in(glink2_delay_in), .reset_in(RST_in), .data_out(glink2_delayed));
Delay_glink Delay_glink3 ( .CLK_in(CLK_in), .data_in(glink3_data_in), .delaynum_in(glink3_delay_in), .reset_in(RST_in), .data_out(glink3_delayed));
Delay_glink Delay_glink4 ( .CLK_in(CLK_in), .data_in(glink4_data_in), .delaynum_in(glink4_delay_in), .reset_in(RST_in), .data_out(glink4_delayed));
Delay_glink Delay_glink5 ( .CLK_in(CLK_in), .data_in(glink5_data_in), .delaynum_in(glink5_delay_in), .reset_in(RST_in), .data_out(glink5_delayed));
Delay_glink Delay_glink6 ( .CLK_in(CLK_in), .data_in(glink6_data_in), .delaynum_in(glink6_delay_in), .reset_in(RST_in), .data_out(glink6_delayed));
Delay_glink Delay_glink7 ( .CLK_in(CLK_in), .data_in(glink7_data_in), .delaynum_in(glink7_delay_in), .reset_in(RST_in), .data_out(glink7_delayed));
Delay_glink Delay_glink8 ( .CLK_in(CLK_in), .data_in(glink8_data_in), .delaynum_in(glink8_delay_in), .reset_in(RST_in), .data_out(glink8_delayed));
Delay_glink Delay_glink9 ( .CLK_in(CLK_in), .data_in(glink9_data_in), .delaynum_in(glink9_delay_in), .reset_in(RST_in), .data_out(glink9_delayed));
Delay_glink Delay_glink10 ( .CLK_in(CLK_in), .data_in(glink10_data_in), .delaynum_in(glink10_delay_in), .reset_in(RST_in), .data_out(glink10_delayed));
Delay_glink Delay_glink11 ( .CLK_in(CLK_in), .data_in(glink11_data_in), .delaynum_in(glink11_delay_in), .reset_in(RST_in), .data_out(glink11_delayed));
Delay_glink Delay_glink12 ( .CLK_in(CLK_in), .data_in(glink12_data_in), .delaynum_in(glink12_delay_in), .reset_in(RST_in), .data_out(glink12_delayed));
Delay_glink Delay_glink13 ( .CLK_in(CLK_in), .data_in(glink13_data_in), .delaynum_in(glink13_delay_in), .reset_in(RST_in), .data_out(glink13_delayed));


always @(posedge CLK_in) begin
    if (reset_in) begin
        glink0_delayed_out <= 17'h0;
        glink1_delayed_out <= 17'h0;
        glink2_delayed_out <= 17'h0;
        glink3_delayed_out <= 17'h0;
        glink4_delayed_out <= 17'h0;
        glink5_delayed_out <= 17'h0;
        glink6_delayed_out <= 17'h0;
        glink7_delayed_out <= 17'h0;
        glink8_delayed_out <= 17'h0;
        glink9_delayed_out <= 17'h0;
        glink10_delayed_out <= 17'h0;
        glink11_delayed_out <= 17'h0;
        glink12_delayed_out <= 17'h0;
        glink13_delayed_out <= 17'h0;
    end
    else begin 
    // G-Link 0
        if(!glink0_delayed[19] || glink0_delayed[20]) begin
            glink0_delayed_out <= 17'h0;
        end
        else if (glink0_delayed[17] || glink0_delayed[18]) begin
            glink0_delayed_out <= glink0_delayed[16:0];
        end
        else begin
            glink0_delayed_out <= 17'h0;
        end

    // G-Link 1
        if(!glink1_delayed[19] || glink1_delayed[20]) begin
            glink1_delayed_out <= 17'h0;
        end
        else if (glink1_delayed[17] || glink1_delayed[18]) begin
            glink1_delayed_out <= glink1_delayed[16:0];
        end
        else begin
            glink1_delayed_out <= 17'h0;
        end

    // G-Link 2
        if(!glink2_delayed[19] || glink2_delayed[20]) begin
            glink2_delayed_out <= 17'h0;
        end
        else if (glink2_delayed[17] || glink2_delayed[18]) begin
            glink2_delayed_out <= glink2_delayed[16:0];
        end
        else begin
            glink2_delayed_out <= 17'h0;
        end

    // G-Link 3
        if(!glink3_delayed[19] || glink3_delayed[20]) begin
            glink3_delayed_out <= 17'h0;
        end
        else if (glink3_delayed[17] || glink3_delayed[18]) begin
            glink3_delayed_out <= glink3_delayed[16:0];
        end
        else begin
            glink3_delayed_out <= 17'h0;
        end

    // G-Link 4
        if(!glink4_delayed[19] || glink4_delayed[20]) begin
            glink4_delayed_out <= 17'h0;
        end
        else if (glink4_delayed[17] || glink4_delayed[18]) begin
            glink4_delayed_out <= glink4_delayed[16:0];
        end
        else begin
            glink4_delayed_out <= 17'h0;
        end

    // G-Link 5
        if(!glink5_delayed[19] || glink5_delayed[20]) begin
            glink5_delayed_out <= 17'h0;
        end
        else if (glink5_delayed[17] || glink5_delayed[18]) begin
            glink5_delayed_out <= glink5_delayed[16:0];
        end
        else begin
            glink5_delayed_out <= 17'h0;
        end

    // G-Link 6
        if(!glink6_delayed[19] || glink6_delayed[20]) begin
            glink6_delayed_out <= 17'h0;
        end
        else if (glink6_delayed[17] || glink6_delayed[18]) begin
            glink6_delayed_out <= glink6_delayed[16:0];
        end
        else begin
            glink6_delayed_out <= 17'h0;
        end

    // G-Link 7
        if(!glink7_delayed[19] || glink7_delayed[20]) begin
            glink7_delayed_out <= 17'h0;
        end
        else if (glink7_delayed[17] || glink7_delayed[18]) begin
            glink7_delayed_out <= glink7_delayed[16:0];
        end
        else begin
            glink7_delayed_out <= 17'h0;
        end

    // G-Link 8
        if(!glink8_delayed[19] || glink8_delayed[20]) begin
            glink8_delayed_out <= 17'h0;
        end
        else if (glink8_delayed[17] || glink8_delayed[18]) begin
            glink8_delayed_out <= glink8_delayed[16:0];
        end
        else begin
            glink8_delayed_out <= 17'h0;
        end

    // G-Link 9
        if(!glink9_delayed[19] || glink9_delayed[20]) begin
            glink9_delayed_out <= 17'h0;
        end
        else if (glink9_delayed[17] || glink9_delayed[18]) begin
            glink9_delayed_out <= glink9_delayed[16:0];
        end
        else begin
            glink9_delayed_out <= 17'h0;
        end

    // G-Link 10
        if(!glink10_delayed[19] || glink10_delayed[20]) begin
            glink10_delayed_out <= 17'h0;
        end
        else if (glink10_delayed[17] || glink10_delayed[18]) begin
            glink10_delayed_out <= glink10_delayed[16:0];
        end
        else begin
            glink10_delayed_out <= 17'h0;
        end

    // G-Link 11
        if(!glink11_delayed[19] || glink11_delayed[20]) begin
            glink11_delayed_out <= 17'h0;
        end
        else if (glink11_delayed[17] || glink11_delayed[18]) begin
            glink11_delayed_out <= glink11_delayed[16:0];
        end
        else begin
            glink11_delayed_out <= 17'h0;
        end

    // G-Link 12
        if(!glink12_delayed[19] || glink12_delayed[20]) begin
            glink12_delayed_out <= 17'h0;
        end
        else if (glink12_delayed[17] || glink12_delayed[18]) begin
            glink12_delayed_out <= glink12_delayed[16:0];
        end
        else begin
            glink12_delayed_out <= 17'h0;
        end

    // G-Link 13
        if(!glink13_delayed[19] || glink13_delayed[20]) begin
            glink13_delayed_out <= 17'h0;
        end
        else if (glink13_delayed[17] || glink13_delayed[18]) begin
            glink13_delayed_out <= glink13_delayed[16:0];
        end
        else begin
            glink13_delayed_out <= 17'h0;
        end

    end
end



endmodule
